#!/bin/bash
set -x

# language fix
export LC_ALL=C

echo "127.0.0.1 localhost $(hostname)" > /etc/hosts
IP=$$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)
RACK=aws_$$(curl http://169.254.169.254/latest/meta-data/placement/availability-zone)
ENV="${ENV}"
NET="${NET}"
ROLE="${ROLE}"
TYPE="${TYPE}"
mesos_work_dir=/var/lib/mesos
RESOURCES="ports:${PORTS}"
ATTRIBUTES="ip:$${IP};rack:$${RACK};net:$${NET};env:$${ENV};type:$${TYPE}"


# Install 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key adv --keyserver keyserver.ubuntu.com --recv E56151BF
add-apt-repository -y "deb https://download.docker.com/linux/ubuntu $$(lsb_release -cs) stable"
add-apt-repository -y "deb http://repos.mesosphere.io/ubuntu $$(lsb_release -cs) main"
add-apt-repository -y ppa:webupd8team/java
apt-get -y update
apt-get install -y docker-ce mesos unzip

# Install Java
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
apt-get install -y oracle-java8-installer

# Install awscli
apt-get install -y awscli


# get ignite-reciever jks files
aws s3 cp s3://ignite-receiver-prod/harman.externalcas.truststore.jks /etc/kafka_truststore.jks
#aws s3 cp s3://ignite-receiver-prod/tantalum.kafka.client.keystore.jks /etc/tantalum.keystore.jks




# Configure
echo '[Unit]
Description=Mesos Slave Service
After=network.target
Wants=network.target

[Service]
Restart=always
RestartSec=20

Environment=LIBPROCESS_IP='"$${IP}"'

Environment=MESOS_ROLE='"$${ROLE}"'
Environment=MESOS_attributes='"$${ATTRIBUTES}"'
Environment=MESOS_resources='"$${RESOURCES}"'
Environment=MESOS_default_role='"$${ROLE}"'


Environment=MESOS_work_dir='"$${mesos_work_dir}"'
#Environment=MESOS_docker_store_dir='"$${mesos_work_dir}"'/docker
#Environment=MESOS_appc_store_dir='"$${mesos_work_dir}"'/appc
#Environment=MESOS_sandbox_directory='"$${mesos_work_dir}"'/sandbox
Environment=MESOS_log_dir='"$${mesos_work_dir}"'/logs
Environment=MESOS_docker_volume_checkpoint_dir='"$${mesos_work_dir}"'/docker_checkpoint

Environment=MESOS_containerizers=mesos,docker
Environment=MESOS_image_providers=appc,docker
Environment=MESOS_isolation=filesystem/linux,docker/runtime,cgroups/cpu,cgroups/mem


Environment=MESOS_modules=/etc/mesos/modules.json
Environment=MESOS_container_logger="org_apache_mesos_LogrotateContainerLogger"

Environment=MESOS_hostname_lookup=false
Environment=MESOS_gc_delay=2days
Environment=MESOS_image_provisioner_backend=overlay
Environment=MESOS_strict=false
Environment=MESOS_systemd_enable_support=true
Environment=MESOS_revocable_cpu_low_priority=true



ExecStart=/usr/sbin/mesos-slave --master='zk://zookeeper-0.node.dc1.consul:2181,zookeeper-1.node.dc1.consul:2181,zookeeper-2.node.dc1.consul:2181/mesos' --ip='"$${IP}"' --hostname='"$${IP}"'

[Install]
WantedBy=multi-user.target' | tee /lib/systemd/system/mesos-slave.service


echo '
{
  "libraries": [
    {
      "file": "/usr/lib/mesos/modules/liblogrotate_container_logger.so",
      "modules": [
        {
          "name": "org_apache_mesos_LogrotateContainerLogger",
          "parameters": [
            {
              "key": "logrotate_stdout_options",
              "value": "rotate 7\ncompress\nmissingok\ncopytruncate"
            },
            {
              "key": "max_stdout_size",
              "value": "20mb"
            },
            {
              "key": "logrotate_stderr_options",
              "value": "rotate 7\ncompress\nmissingok\ncopytruncate"
            },
            {
              "key": "max_stderr_size",
              "value": "20mb"
            }
          ]
        }
      ]
    }
  ]
}' | tee /etc/mesos/modules.json




# Install Consul
cd /tmp
curl -sLo consul.zip https://releases.hashicorp.com/consul/1.1.0/consul_1.1.0_linux_amd64.zip
unzip consul.zip > /dev/null
chmod +x consul
mv consul /usr/local/bin/consul

# Setup Consul
mkdir -p /mnt/consul
mkdir -p /etc/consul.d
tee /etc/consul.d/config.json > /dev/null <<EOF
{
  "bind_addr": "$${IP}",
  "advertise_addr": "$${IP}",
  "client_addr": "$${IP}",
  "addresses": {
    "http": "$${IP}",
    "https": "$${IP}"
  },
  "ports": {
    "http": 8500,
    "https": -1
  },
  "data_dir": "/mnt/consul",
  "disable_remote_exec": true,
  "disable_update_check": true,
  "leave_on_terminate": true,
  "retry_join": [
    "ip-10-40-2-77.node.dc1.consul",
    "ip-10-40-4-59.node.dc1.consul",
    "ip-10-40-8-247.node.dc1.consul"
  ]
}
EOF

# Configure
echo '[Unit]
Description=Consul Agent
After=network.target
Wants=network.target

[Service]
Restart=always
RestartSec=20
ExecStart=/usr/local/bin/consul agent -config-dir="/etc/consul.d"

[Install]
WantedBy=multi-user.target' | tee /lib/systemd/system/consul.service

systemctl daemon-reload
systemctl enable mesos-slave
systemctl enable consul
systemctl start mesos-slave
systemctl start consul
