variable env {}

variable subnet {
  type = "list"
}

variable multi_az {
  default = true
}
