resource "aws_db_subnet_group" "tantalum_db" {
  name       = "tantalum-db"
  subnet_ids = ["${var.subnet}"]

  tags {
    Name = "tantalum-db"
  }
}

resource "aws_db_instance" "tantalum_db" {
  count                = 4
  allocated_storage    = 30
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.m4.xlarge"
  name                 = "prod${count.index}"
  username             = "admin"
  password             = "5FUb6H8GcChg4znx"
  parameter_group_name = "default.mysql5.7"
  multi_az             = "${var.multi_az}"
  db_subnet_group_name = "${aws_db_subnet_group.tantalum_db.name}"
  identifier           = "tantalum-db-${var.env}-${count.index}"

  tags {
    Name = "tantalum-db-${var.env}-${count.index}"
  }
}
