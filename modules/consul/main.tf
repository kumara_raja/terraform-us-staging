resource aws_instance "consul" {
  count                  = "${length(var.availability_zones)}"
  ami                    = "${var.ami}"
  instance_type          = "c4.large"
  subnet_id              = "${var.subnet[count.index]}"
  key_name               = "${var.key_name}"
  vpc_security_group_ids = ["${var.consul_sg}"]


  user_data = <<EOF
#cloud-config
package_update: true
package_upgrade: true
packages: 
  - 'python'
manage_etc_hosts: false
manage_resolv_conf: false

runcmd:
 - echo "nameserver 8.8.8.8" > /etc/resolv.conf
 - echo "127.0.0.1 localhost $(hostname)" > /etc/hosts 
 - apt update
 - apt install -y python
  EOF

  tags {
    Name           = "consul-${count.index}"
    Service        = "consul"
    consul-cluster = "${var.env}"
  }
}

