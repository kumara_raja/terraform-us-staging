output masters_ips {
  value = "${aws_instance.masters.*.private_ip}"
}
