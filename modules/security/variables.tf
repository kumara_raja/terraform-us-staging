variable "mgmt_vpc_id" {}
variable "svc_vpc_id" {}
variable "peer_vpc_id" {}

variable "env" {}

variable "ssh_cidr" {
  type = "list"
  default = ["10.0.0.0/8"]
}

variable "internal_cidr" {
  type = "list"
  default = ["10.0.0.0/8"]
}

variable "mgmt_cidr" {
  type = "list"
  default = ["10.40.0.0/16"]
}

variable "svc_cidr" {
  type = "list"
  default = ["10.41.0.0/16"]
}

variable "peer_cidr" {
  type = "list"
  default = ["10.46.0.0/16"]
}

variable "att_cidr" {
  type = "list"
  default = [
    "86.11.195.135/32", //jimmy ip for testing remove before launch
    "144.160.130.0/24",
    "144.160.5.0/24",
    "144.160.80.0/24"
  ]
}
