variable vhost_entry_name {}
variable lb_cluster {}
variable lb_dns_name {}
variable vhosts {type = "list"}
variable service {}
variable upstream_alias {}
variable dns_zone {}
variable bp {
  default = "configuration/loadbalancer"
}
variable vhosts_ip_allow {
  default = ""
}

