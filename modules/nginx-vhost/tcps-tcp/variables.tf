variable vhost_entry_name {}
variable lb_cluster {}
variable cert {}
variable ca {}
variable server_key {}
variable lb_dns_name {}
variable vhosts {type = "list"}
variable service {}
variable upstream_alias {}
variable dns_zone {}
variable bp {
  default = "configuration/loadbalancer"
}
variable ssl_enabled {
  default = "true"
}
variable force_to_ssl {
    default = "true"
}
variable tcp_port {
  default = "80"
}
variable vhosts_ip_allow {
  default = ""
}
