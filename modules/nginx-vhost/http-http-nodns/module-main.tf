resource "consul_key_prefix" "vhost_keys" {
  path_prefix = "${var.bp}/${var.lb_cluster}/virtualhosts/${var.vhost_entry_name}/"
  subkeys {
    "alias"            = <<EOF
["${join("\",\"", var.vhosts)}"]
EOF
    "backend"            = "${var.service}"
    "backend-alias"      = "${var.upstream_alias}"
    "unsecure-enabled"   = "true"
    "vhosts_ip_allow" = "${var.vhosts_ip_allow}"
  }
}
