variable "env" {}
variable "vpc_id" {}
variable "cassandra_sg" {}


variable "instance_type" {
  default = "c4.2xlarge"
}
variable "subnet" {
  type = "list"
}

variable "availability_zones" {
  type = "list"
}

variable "ami" {}

variable "key_name" {}
