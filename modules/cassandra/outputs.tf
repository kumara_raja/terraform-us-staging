output cassandra_ips {
  value = "${aws_instance.cassandra.*.private_ip}"
}

