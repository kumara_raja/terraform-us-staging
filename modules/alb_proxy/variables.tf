
variable "vpc_id" {}
variable "availability_zones" {
  type = "list"
}
variable "subnets" {
type = "list"
}

variable "path_pattern" {}
variable "target_ip" {
type = "list"
}
variable "name" {}
variable "certificate_arn" {}

variable "lb_ssl_policy" {}
variable "route53_record_name" {}

variable "ingress_cidr_blocks" {
  type = "list"}
variable "egress_cidr_blocks" {
  type = "list"
}
variable "zone_id" {}
