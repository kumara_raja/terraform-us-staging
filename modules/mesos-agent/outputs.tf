output instances {
  value = ["${aws_instance.mesos-agent.*.id}"]
}

output private_ips {
  value = ["${aws_instance.mesos-agent.*.private_ip}"]
}


output public_ips {
  value = ["${aws_instance.mesos-agent.*.public_ip}"]
}

output az {
  value = ["${aws_instance.mesos-agent.*.availability_zone}"]
}

output hostnames {
  value = ["${aws_instance.mesos-agent.*.tags.Name}"]
}
