resource "consul_keys" "sonic_1_1_template" {
  key {
    path  = "configuration/deployment/templates/sonic-1/_template"
    value = "${file("files/deployment-templates/sonic-1.json")}"
  }
}

// branch/env-type/env-id/app-name/version
resource consul_key_prefix "sonic-1-defaults" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/_default/"

  subkeys {
    "runtime/cpu"                                          = "0.2"
    "runtime/memory"                                       = "1500"
    "runtime/instances"                                    = "1"
    "runtime/health_port_index"                            = "2"
    "runtime/grace_period_seconds"                         = "60"
    "jvm/server"                                           = ""
    "jvm/Xmx"                                              = "1500m"
    "properties/logPrefix"                                 = "$MESOS_TASK_ID"
    "properties/server.port"                               = "$PORT0"
    "properties/management.port"                           = "$PORT2"
    "properties/spring.cloud.consul.token"                 = "0CF6F73C-A62D-4EF2-B08E-128A616CF586"
    "properties/spring.cloud.consul.host"                  = "consul.service.consul"
    "properties/com.sun.management.jmxremote"              = "true"
    "properties/com.sun.management.jmxremote.port"         = "$PORT1"
    "properties/com.sun.management.jmxremote.local.only"   = "false"
    "properties/com.sun.management.jmxremote.authenticate" = "false"
    "properties/com.sun.management.jmxremote.ssl"          = "false"
    "properties/spring.profiles.active"                    = "$ENV,$CLUSTER_NAME,$${ENV}-$${ENV_ID}" //qa
    "properties/environment"                               = "$ENV"                                  //qa
    "properties/cluster_name"                              = "$CLUSTER_NAME"                         //qa-1
    "properties/cluster_id"                                = "$CLUSTER_ID"                           //qa_1
    "properties/platformId"                                = "$PLATFORM_ID"
    "properties/projectId"                                 = "$PROJECT_ID"
    "properties/namespace"                                 = "$ENV_ID"
    "properties/java.library.path"                         = "."
    "properties/cassandra.hosts"                           = "cassandra-1-node-0.node.consul"
    "properties/cassandra.hosts[0]"                        = "cassandra-1-node-0.node.consul"
    "properties/cassandra.hosts[1]"                        = "cassandra-1-node-1.node.consul"
    "properties/cassandra.hosts[2]"                        = "cassandra-1-node-2.node.consul"
  }
}

// sonic defaults
resource "consul_key_prefix" "sonic-1-_any-env-type-_any-env-_any-app-name-svc-sonic" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/branch/_any/env-type/_any/env-id/_any/app-name/svc-sonic/"

  subkeys {
    "runtime/cpu" = "1"
  }
}

// config by branch - think is no longer relevant
resource consul_key_prefix "deployment_branch_sonic_1_firestarter_perf_telematics" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/branch/firestarter/env-type/perf/env-id/3/app-name/api-telematics-tantalum-b2c/"

  subkeys {
    "properties/cassandra.keyspace"    = "perf_3_sonic"
    "properties/kafka.assetSync.topic" = "perf_3_asset_sync"
    "properties/kafka.consumerGroup"   = "daas_perf_3_asset_sync-1"
  }
}

resource consul_key_prefix "deployment_branch_sonic_1_firestarter_prod_telematics" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/branch/firestarter/env-type/prod/env-id/_any/app-name/api-telematics-tantalum-b2c/"

  subkeys {
    "properties/cassandra.keyspace"        = "$${CLUSTER_ID}_sonic"
    "properties/kafka.assetSync.topic"     = "$${CLUSTER_NAME}_asset_sync"
    "properties/kafka.consumerGroup"       = "daas_$${CLUSTER_ID}_asset_sync-1"
    "properties/spring.cloud.consul.token" = "0CF6F73C-A62D-4EF2-B08E-128A616CF586"
  }
}

resource consul_key_prefix "deployment_branch_sonic_1_firestarter_prod_paperboy" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/branch/firestarter/env-type/prod/env-id/_any/app-name/paperboy-tantalum/"

  subkeys {
    "properties/publication.topic"         = "$${CLUSTER_NAME}-paperboy-newsagent"
    "properties/distribution.topic"        = "$${CLUSTER_NAME}-paperboy-paper-round"
    "properties/spring.cloud.consul.token" = "0CF6F73C-A62D-4EF2-B08E-128A616CF586"
    "properties/cassandra.contactPoints"   = "cassandra-1-node-0.node.consul,cassandra-1-node-1.node.consul,cassandra-1-node-2.node.consul"
  }
}

resource consul_key_prefix "deployment_platform_sonic_1_telematics_fs_firestarter_svc-sonic-prod" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/platform/telematics/project/firestarter/env-type/prod/env-id/_any/app-name/svc-sonic/"

  subkeys {
    "properties/kafka.mystTopic"           = "myst_$${ENV}"
    "properties/spring.cloud.consul.token" = "0CF6F73C-A62D-4EF2-B08E-128A616CF586"
    "properties/sonic.cassandra.hosts"     = "cassandra-1-node-0.node.consul,cassandra-1-node-1.node.consul,cassandra-1-node-2.node.consul"
    "properties/log4j.configurationFile"   = "log4j2-kafka.xml"

    "properties/sonic.cassandra.hosts[0]" = "cassandra-1-node-0.node.consul"
    "properties/sonic.cassandra.hosts[1]" = "cassandra-1-node-1.node.consul"
    "properties/sonic.cassandra.hosts[2]" = "cassandra-1-node-2.node.consul"
    "properties/sonic.cassandra.keyspace" = "telematics_firestarter_prod_sonic"
  }
}

/*  cut and paste by accident
resource consul_key_prefix "deployment_platform_sonic_1_telematics_firestarter_any_account" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/platform/telematics/project/firestarter/env-type/_any/env-id/_any/app-name/api-account-tantalum-b2c/"
  subkeys {
    "runtime/memory" = "1700"
  }
}
*/

// sonic defaults by platform

resource consul_key_prefix "deployment_platform_sonic_1_telematics_fs_firestarter_sonic" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/platform/telematics/project/firestarter/env-type/_any/env-id/_any/app-name/svc-sonic/"

  subkeys {
    "runtime/cpu"                             = "1"
    "properties/kafka.mystTopic"              = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_myst"
    "properties/cassandra.outboundKeyspace"   = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_outbound"
    "properties/paperboy.topics.publication"  = "$${CLUSTER_ID}_paperboy_newsagent"
    "properties/paperboy.topics.distribution" = "$${CLUSTER_ID}_paperboy_paper_round"
  }
}

resource consul_key_prefix "deployment_platform_sonic_1_telematics_fs_shared_myst" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/platform/telematics/project/shared/env-type/_any/env-id/_any/app-name/svc-myst/"

  subkeys {
    "properties/kafka.producer.decoded"  = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_myst"
    "properties/kafka.producer.invalid"  = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_myst_invalid"
    "properties/helium.topic"            = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_helium"
    "properties/log4j.configurationFile" = "log4j2-prod.xml"
  }
}

resource consul_key_prefix "deployment_platform_sonic_1_telematics_fs_firestarter_sonic_qa_develop" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/platform/telematics/project/firestarter/env-type/qa/env-id/develop/app-name/svc-sonic/"

  subkeys {
    "properties/kafka.mystTopic" = "telematics_uat_1_myst"
  }
}

// perf-1 settings
resource consul_key_prefix "deployment_platform_sonic_1_telematics_fs_shared_perf" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/platform/telematics/project/shared/env-type/perf/env-id/_any/app-name/_any/"

  subkeys {
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
  }
}

resource consul_key_prefix "deployment_platform_sonic_1_telematics_fs_firestarer_perf" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/platform/telematics/project/firestarter/env-type/perf/env-id/_any/app-name/_any/"

  subkeys {
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
  }
}

resource consul_key_prefix "deployment_platform_sonic_1_telematics_fs_firestarer_svc-sonic_perf" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/platform/telematics/project/firestarter/env-type/perf/env-id/_any/app-name/svc-sonic/"

  subkeys {
    "runtime/cpu" = "2"
  }
}

// staging settings - NOTE: format below needs to be applied when move to production, ant
// then to other environments

resource consul_key_prefix "deployment_platform_sonic_1_telematics_fs_firestarter_svc-sonic-staging" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/platform/telematics/project/firestarter/env-type/staging/env-id/_any/app-name/svc-sonic/"

  subkeys {
    "properties/sonic.kafka.brokers"               = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
    "properties/sonic.kafka.mystTopic"             = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_myst"
    "properties/sonic.cassandra.outboundKeyspace"  = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_outbound"
    "properties/sonic.paperboy.topicsPublication"  = "$${CLUSTER_ID}_paperboy_newsagent"
    "properties/sonic.paperboy.topicsDistribution" = "$${CLUSTER_ID}_paperboy_paper_round"
    "properties/cassandra.contactPoints"           = "cassandra-2-node-0.node.consul,cassandra-2-node-1.node.consul,cassandra-2-node-2.node.consul"
    "properties/log4j.configurationFile"           = "log4j2-kafka.xml"
  }
}

resource consul_key_prefix "deployment_platform_sonic_1_telematics_fs_firestarter_svc-sonic-uat" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/platform/telematics/project/firestarter/env-type/uat/env-id/_any/app-name/svc-sonic/"

  subkeys {
    "properties/log4j.configurationFile" = "log4j2-kafka.xml"
  }
}
