variable "path" {
  default = "configuration/common/port-numbers/"
}
resource consul_key_prefix "port_numbers" {
  path_prefix = "${var.path}"
  subkeys {
    "jenkins/http" =                     "8080"
    "kafka/server" =                     "9092"

    "hdfs-datanode/http" =               "50075"
    "hdfs-datanode/https" =              "50475"
    "hdfs-datanode/http-data-transfer" = "50010"
    "hdfs-datanode/ipc" =                "50020"
    "hdfs-namenode/http" =               "50080"
    "hdfs-namenode/https" =              "50480"
    "hdfs-namenode/port" =               "8020"
    "hdfs-namenode/zkfc" =               "8019"
    "hdfs-journalnode/port_1" =          "8480"
    "hdfs-journalnode/port_2" =          "8485"

    "loadbalancer/http" =                "80"
    "loadbalancer/https" =               "443"
    "loadbalancer/metrics" =             "9145"

    "elasticsearch/client" =             "9200"
    "elasticsearch/transport" =          "9300"

    "fleetnode/http" =                   "9090"

    "influxdb/raft" =                    "8088" //raft peers
    "influxdb/http" =                    "8086"
    "influxdb/admin" =                   "8083"
    "influxdb/graphite" =                "2003"
    "influxdb/collectd" =                "25826"
    "influxdb/opentsdb" =                "4242"
    "influxdb/udp" =                     "8089"

    "cassandra/jmx" =                    "7199"
    "cassandra/node" =                   "7000"
    "cassandra/nodessl" =                "7001"
    "cassandra/cql" =                    "9042"
    "cassandra/thrift" =                 "9160"
    "cassandra/metrics" =                "7400"

    "artifactory/ajp" =                  "8019"
    "artifactory/http" =                 "8081"
    "artifactory/docker" =               "2222"


    "helium/http" =                      "80"
    "helium/https" =                     "443"
    "helium/udp" =                       "4305"
    "helium/tcp" =                       "5654"
    "helium/tls" =                       "5443"
    "helium/mgmt" =                      "9003"
    "helium/jmx" =                       "9004"
    "helium/metrics" =                   "9145"

    "hive/metastore" =                   "9083"
    "hive/hiveserver2" =                 "10000"

    "argon/http" =                       "80"
    "argon/https" =                      "443"
    "argon/udp"  =                       "4325"
    "argon/tcp"  =                       "5674"
    "argon/tls"  =                       "5443"
    "argon/metrics" =                    "9145"

    "neon/http" =                        "80"
    "neon/https" =                       "443"
    "neon/udp"   =                       "4315"
    "neon/tcp"   =                       "5664"
    "neon/metrics" =                     "9145"

    "diproton/tcp" =                     "5684"
    "diproton/udp" =                     "4335"

    "prometheus/http" =                  "9090"
    "prometheus/push" =                  "9091"

    "smtp/tcp" =                         "25"

    "gluu/http" =                        "80"
    "gluu/https" =                       "443"
    "gluu/ldaps" =                       "1636"
    "gluu/ldaps1" =                      "636"
    "gluu/passport" =                    "8090"
  }
}
