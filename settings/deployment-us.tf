resource consul_key_prefix "micro-spring-staging" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/branch/_any/env-type/staging/"
  subkeys {
    "properties/spring.cloud.consul.token" =                 "c94a3bf5-bfca-017e-1f62-cc54e4ab07cd"
  }
}


resource consul_key_prefix "micro-spring-1-staging" {
  path_prefix = "configuration/deployment/templates/micro-spring-1/_settings/branch/_any/env-type/staging/"
  subkeys {
    "properties/spring.cloud.consul.token" =                 "c94a3bf5-bfca-017e-1f62-cc54e4ab07cd"
  }
}

resource consul_key_prefix "micro-1-spring-staging" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/branch/_any/env-type/staging/"
  subkeys {
    "properties/spring.cloud.consul.token" =                 "c94a3bf5-bfca-017e-1f62-cc54e4ab07cd"
  }
}
resource consul_key_prefix "helium-staging-defaults" {
  path_prefix = "configuration/deployment/templates/helium-1/_settings/branch/_any/env-type/staging/"
  subkeys {
    "properties/spring.cloud.consul.token" =                 "c94a3bf5-bfca-017e-1f62-cc54e4ab07cd"
  }
}

resource consul_key_prefix "sonic-staging-defaults" {
  path_prefix = "configuration/deployment/templates/sonic-1/_settings/branch/_any/env-type/staging/"
  subkeys {
    "properties/spring.cloud.consul.token" =                 "c94a3bf5-bfca-017e-1f62-cc54e4ab07cd"
  }
}

resource consul_key_prefix "node-staging-defaults" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/branch/_any/env-type/staging/"
  subkeys {
    "properties/spring.cloud.consul.token" =                 "c94a3bf5-bfca-017e-1f62-cc54e4ab07cd"
  }
}

resource consul_key_prefix "deployment_api-subscriptions_staging" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/staging/env-id/_any/app-name/api-subscriptions/"
  subkeys {
    "properties/https.proxyHost" = "lb-us-2-internal.dev.thala.us"
    "properties/https.proxyPort" = "3128"
  }
}
