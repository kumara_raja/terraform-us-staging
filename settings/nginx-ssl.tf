resource consul_keys "nginx_ssl"{
  key {
    path = "configuration/loadbalancer/defaults/ssl/ciphers"
    value = "HIGH:!aNULL:!MD5"
    delete = true
  }
  key = {
    path = "configuration/loadbalancer/defaults/ssl/protocols"
    value = "TLSv1 TLSv1.1 TLSv1.2"
    delete = true
  }
}