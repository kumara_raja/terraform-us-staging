resource consul_keys "mesos_defaults" {
  key {
    path = "configuration/common/mesos_master"
    value = "zk://core-mgmt.zookeeper.service.consul:2181/mesos/main-1"
    delete = true
  }
//masters
  key {
    path = "configuration/hiera/common/mesos_master/log_dir"
    value = "/data/mesos/log"
    delete = true
  }
  key {
    path = "configuration/hiera/common/mesos_master/base_dir"
    value = "/opt/mesos"
    delete = true
  }
  key {
    path = "configuration/hiera/common/mesos_master/work_dir"
    delete = true
    value = "/data/mesos/work"
  }
  key {
    path = "configuration/hiera/common/mesos_master/size"
    delete = true
    value = "3"
  }
  key {
    path = "configuration/hiera/common/mesos_master/quorum"
    delete = true
    value = "2"
  }
//slaves
  key {
    path = "configuration/hiera/common/mesos_slave/base_dir"
    delete = true
    value = "/opt/mesos"
  }

  key {
    path = "configuration/hiera/common/mesos_slave/work_dir"
    delete = true
    value = "/data/mesos/work"
  }
  key {
    path = "configuration/hiera/common/mesos_slave/log_dir"
    delete = true
    value = "/data/mesos/log"
  }

}