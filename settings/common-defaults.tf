resource consul_keys "commons" {
  key {
    path = "configuration/hiera/common/consul_template/version"
    value = "0.15.0"
    delete = true
  }
  key {
    path = "configuration/common/hdfs_cluster"
    value = "hdfs-1"
    delete = true
  }
  key {
    path = "configuration/hiera/common/hadoop/hdfs_cluster"
    value = "hdfs-1"
    delete = true
  }

  key {
    path = "configuration/hiera/common/hadoop/version"
    value = "2.7.2"
    delete = true
  }
}