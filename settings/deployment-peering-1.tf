resource "consul_keys" "micro_spring_peering_1_template" {
  key {
    path =  "configuration/deployment/templates/micro-1-spring-peering/_template"
    value = "${file("files/deployment-templates/no-image-peering-1.json")}"
  }
}

// branch/env-type/env-id/app-name/version
resource consul_key_prefix "micro-1-spring-peering-defaults" {
  path_prefix = "configuration/deployment/templates/micro-1-spring-peering/_settings/_default/"
  subkeys {
    "runtime/cpu" =                                          "0.2"
    "runtime/memory" =                                       "1500"
    "runtime/instances" =                                    "1"
    "runtime/health_port_index" =                            "2"
    "runtime/grace_period_seconds" =                          "60"
    "jvm/Xmx" =                                              "1500m"
    "jvm/server" =                                           ""
    "properties/logPrefix" =                                 "$MESOS_TASK_ID"
    "properties/server.port" =                               "$PORT0"
    "properties/management.port" =                           "$PORT2"
    "properties/spring.cloud.consul.token" =                 "5a87e827-1c05-091d-2cd6-f7d075db4152"
    "properties/spring.cloud.consul.host" =                  "consul.service.consul"
    "properties/com.sun.management.jmxremote" =              "true"
    "properties/com.sun.management.jmxremote.port" =         "$PORT1"
    "properties/com.sun.management.jmxremote.local.only" =   "false"
    "properties/com.sun.management.jmxremote.authenticate" = "false"
    "properties/com.sun.management.jmxremote.ssl" =          "false"
    "properties/spring.profiles.active" =                    "$ENV,$CLUSTER_NAME,$${ENV}-$${ENV_ID}" //qa
    "properties/environment" =                               "$ENV" //qa
    "properties/cluster_name" =                              "$CLUSTER_NAME" //qa-1
    "properties/cluster_id" =                                "$CLUSTER_ID" //qa_1
    "properties/platformId" =                                "$PLATFORM_ID"
    "properties/projectId"  =                                "$PROJECT_ID"
    "properties/namespace" =                                 "$ENV_ID"
  }
}



