resource "consul_keys" "micro_spring_template" {
  key {
    path =  "configuration/deployment/templates/micro-spring/_template"
    value = "${file("files/deployment-templates/no-image-micro.json")}"
  }
}

// branch/env-type/env-id/app-name/version
resource consul_key_prefix "micro-spring-defaults" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/_default/"
  subkeys {
    "runtime/cpu" =                                          "0.2"
    "runtime/memory" =                                       "1200"
    "runtime/instances" =                                    "1"
    "runtime/health_port_index" =                            "2"
    "runtime/grace_period_seconds" =                          "60"
    "jvm/server" =                                           ""
    "properties/logPrefix" =                                 "$MESOS_TASK_ID"
    "properties/server.port" =                               "$PORT0"
    "properties/management.port" =                           "$PORT2"
    "properties/spring.cloud.consul.token" =                 "5a87e827-1c05-091d-2cd6-f7d075db4152"
    "properties/spring.cloud.consul.host" =                  "consul.service.consul"
    "properties/com.sun.management.jmxremote" =              "true"
    "properties/com.sun.management.jmxremote.port" =         "$PORT1"
    "properties/com.sun.management.jmxremote.local.only" =   "false"
    "properties/com.sun.management.jmxremote.authenticate" = "false"
    "properties/com.sun.management.jmxremote.ssl" =          "false"
    "properties/spring.profiles.active" =                    "$ENV,$CLUSTER_NAME" //qa
    "properties/environment" =                               "$ENV" //qa
    "properties/cluster_name" =                              "$CLUSTER_NAME" //qa-1
    "properties/cluster_id" =                                "$CLUSTER_ID" //qa_1
  }
}


resource "consul_key_prefix" "branch-_any-env-type-ci" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/branch/_any/env-type/ci/"
  subkeys {
    "runtime/cpu" =       "0.2"
    "runtime/memory" =    "1200"
    "runtime/instances" = "1"
    "properties/log4j.configurationFile" =                   "log4j2.xml"
  }
}

resource "consul_key_prefix" "branch-_any-env-type-qa" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/branch/_any/env-type/qa/"
  subkeys {
    "runtime/cpu" =       "0.2"
    "runtime/memory" =    "1200"
    "runtime/instances" = "1"
    "properties/log4j.configurationFile" =                   "log4j2.xml"
  }
}

resource "consul_key_prefix" "branch-_any-env-type-uat" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/branch/_any/env-type/uat/"
  subkeys {
    "runtime/cpu" =       "0.5"
    "runtime/memory" =    "2000"
    "runtime/instances" = "1"
    "jvm/Xmx" =           "1200m"
  //  "properties/log4j.configurationFile" =                   "log4j2-kafka.xml"
  }
}

resource "consul_key_prefix" "micro-sprint-branch-_any-env-type-_any-env-_any-app-name-svc-sonic" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/branch/_any/env-type/_any/env-id/_any/app-name/svc-sonic/"
  subkeys {
    "runtime/cpu" = "1"
  }
}




resource consul_key_prefix "deployment_branch_micro_spring_firestarter" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/branch/firestarter/env-type/_any/env-id/_any/app-name/_any/"
  subkeys {
    "properties/spring.cloud.consul.token" = "07fa4231-ba73-4631-8114-20099d924348"
    "properties/kafka.brokers" =             "kafka-2-0.service.consul:31000"
    "runtime/grace_period_seconds" =                          "120"
  }
}

resource consul_key_prefix "deployment_branch_micro_spring_firestarter_perf" {
  path_prefix ="configuration/deployment/templates/micro-spring/_settings/branch/firestarter/env-type/perf/env-id/3/app-name/_any/"
  subkeys {
    "properties/spring.cloud.consul.token"      = "5a87e827-1c05-091d-2cd6-f7d075db4152"
    "properties/kafka.brokers"                  = "kafka-perf-3-node-0.node.consul:31000"
    "properties/cassandra.hosts"                = "cassandra-perf-3-node-0.node.consul"
    "properties/cassandra.hosts[0]"                = "cassandra-perf-3-node-0.node.consul"
    "properties/cassandra.hosts[1]"                = "cassandra-perf-3-node-1.node.consul"
    "properties/cassandra.hosts[2]"                = "cassandra-perf-3-node-2.node.consul"
    "properties/cassandra.contactPoints"        = "cassandra-perf-3-node-0.node.consul"
    "properties/cassandra.contactPoints.0"        = "cassandra-perf-3-node-0.node.consul"
    "properties/spring.datasource.username"     = "admin"
    "properties/spring.datasource.password"     = "v9wen485"
    "properties/spring.datasource.url"          = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com/perf_3_tt_arm?useSSL=false"
  }
}

resource consul_key_prefix "deployment_branch_micro_spring_firestarter_perf_telematics" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/branch/firestarter/env-type/perf/env-id/3/app-name/api-telematics-tantalum-b2c/"
  subkeys {
    "properties/cassandra.keyspace" = "perf_3_sonic"
    "properties/kafka.assetSync.topic" = "perf_3_asset_sync"
    "properties/kafka.consumerGroup" = "daas_perf_3_asset_sync-1"
  }
}

/* set env-id to 2 for initial testing, but will switch to 1 later */
resource consul_key_prefix "deployment_branch_micro_spring_firestarter_prod" {
  path_prefix ="configuration/deployment/templates/micro-spring/_settings/branch/firestarter/env-type/prod/env-id/1/app-name/_any/"
  subkeys {
    "runtime/cpu" =       "0.5"
    "runtime/memory" =    "1500"
    "runtime/instances" = "1"
    "runtime/grace_period_seconds" =                          "120"
    "jvm/Xmx" =           "1200m"
    "properties/log4j.configurationFile" =                   "log4j2-kafka.xml"
    "properties/spring.cloud.consul.token"      = "259be361-f20a-bb9d-3955-8da16555185a"
    "properties/spring.cloud.consul.host" = "consul.service.consul"
    "properties/kafka.brokers"                  = "kafka-2-node-0.node.consul:31000,kafka-2-node-1.node.consul:31000,kafka-2-node-2.node.consul:31000"
    "properties/cassandra.hosts"                = "cassandra-1-node-0.node.consul"
    "properties/cassandra.hosts[0]"                = "cassandra-1-node-0.node.consul"
    "properties/cassandra.hosts[1]"                = "cassandra-1-node-1.node.consul"
    "properties/cassandra.hosts[2]"                = "cassandra-1-node-2.node.consul"
    "properties/cassandra.contactPoints"        = "cassandra-1-node-0.node.consul,cassandra-1-node-1.node.consul,cassandra-1-node-2.node.consul"
    "properties/spring.datasource.username"     = "admin"
    "properties/spring.datasource.password"     = "v9wen486"
    "properties/spring.datasource.url"          =  "jdbc:mysql://tf-0019dcf2a629e9bdb2d77f4399.crizhv4ha0xo.eu-west-1.rds.amazonaws.com/firestarter_prod_tt_arm?useSSL=false"
  }
}

resource consul_key_prefix "deployment_branch_micro_spring_firestarter_prod_telematics" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/branch/firestarter/env-type/prod/env-id/_any/app-name/api-telematics-tantalum-b2c/"
  subkeys {
    "properties/cassandra.keyspace" = "$${CLUSTER_ID}_sonic"
    "properties/kafka.assetSync.topic" = "$${CLUSTER_NAME}_asset_sync"
    "properties/kafka.consumerGroup" = "daas_$${CLUSTER_ID}_asset_sync-1"
  }
}

resource consul_key_prefix "deployment_branch_micro_spring_firestarter_prod_paperboy" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/branch/firestarter/env-type/prod/env-id/_any/app-name/paperboy-tantalum/"
  subkeys {
    "properties/publication.topic" = "$${CLUSTER_NAME}-paperboy-newsagent"
    "properties/distribution.topic" = "$${CLUSTER_NAME}-paperboy-paper-round"
  }
}


resource consul_key_prefix "deployment_branch_micro_spring_firestarter_prod_sonic" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/branch/firestarter/env-type/prod/env-id/1/app-name/svc-sonic/"
  subkeys {
    "properties/kafka.mystTopic" = "myst_$${ENV}"
  }
}


resource consul_key_prefix "deployment_branch_micro_spring_firestarter_prod_fallout" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/branch/firestarter/env-type/prod/env-id/_any/app-name/svc-fallout-tantalum/"
  subkeys {
    "runtime/cpu" =                                          "0.5"
    "runtime/memory" =                                       "2048"
    "runtime/instances" =                                    "1"
    "jvm/Xmx" =                                              "1800m"
  }
}

resource "consul_keys" "helium_template" {
  key {
    path =  "configuration/deployment/templates/helium/_template"
    value = "${file("files/deployment-templates/helium.json")}"
  }
}

resource consul_key_prefix "helium-defaults" {
  path_prefix = "configuration/deployment/templates/helium/_settings/_default/"
  subkeys {
    "runtime/cpu" =                                          "1"
    "runtime/memory" =                                       "2048"
    "runtime/instances" =                                    "1"
    "runtime/health_port_index" =                            "3"
    "jvm/Xmx" =                                              "900m"
    "jvm/server" =                                           ""
    "properties/log4j.configurationFile" =                   "log4j2.xml"
    "properties/helium.tcp.port" =                           "$PORT0"
    "properties/helium.udp.port" =                           "$PORT1"
    "properties/helium.healthCheckHttpPort" =                "$PORT3"
    "properties/com.sun.management.jmxremote" =              "true"
    "properties/com.sun.management.jmxremote.port" =         "$PORT2"
    "properties/com.sun.management.jmxremote.local.only" =   "false"
    "properties/com.sun.management.jmxremote.authenticate" = "false"
    "properties/com.sun.management.jmxremote.ssl" =          "false"

    "properties/environment" =                               "$ENV" //qa
    "properties/cluster_name" =                              "$CLUSTER_NAME" //qa-1
    "properties/cluster_id" =                                "$CLUSTER_ID" //qa_1
    "properties/kafka.brokers" =                             "kafka-2-0.service.consul:31000"
  }
}

resource consul_key_prefix "deployment_branch_helium_firestarter_perf" {
  path_prefix ="configuration/deployment/templates/helium/_settings/branch/firestarter/env-type/perf/env-id/3/"
  subkeys {
    "properties/spring.cloud.consul.token" = "5a87e827-1c05-091d-2cd6-f7d075db4152"
    "properties/kafka.brokers" = "kafka-perf-3-node-0.node.consul:31000"
    "properties/cassandra.hosts" = "cassandra-perf-3-node-0.node.consul"
    "properties/cassandra.contactPoints" = "cassandra-perf-3-node-0.node.consul"
  }
}

resource consul_key_prefix "deployment_branch_helium_firestarter_prod" {
  path_prefix ="configuration/deployment/templates/helium/_settings/branch/firestarter/env-type/prod/env-id/1/"
  subkeys {
    "properties/spring.cloud.consul.token"      = "259be361-f20a-bb9d-3955-8da16555185a"
    "properties/kafka.brokers"                  = "kafka-2-node-0.node.consul:31000,kafka-2-node-1.node.consul:31000,kafka-2-node-2.node.consul:31000"
    "properties/cassandra.contactPoints.0"        = "cassandra-1-node-0.node.consul"
    "properties/spring.cloud.consul.host" = "consul.service.consul"
  }
}



resource "consul_key_prefix" "branch-_any-env-type-_any-env-id-_any-app-name-svc-galaxy" {
  path_prefix = "configuration/deployment/templates/micro-spring/_settings/branch/_any/env-type/_any/env-id/_any/app-name/svc-galaxy/"
  subkeys {
    "runtime/cpu" = "0.3"
    "runtime/memory" =    "2200"
    "jvm/Xmx" =           "2000m"
  }
}
