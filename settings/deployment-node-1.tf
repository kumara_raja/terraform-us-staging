resource "consul_keys" "node_1_template" {
  key {
    path  = "configuration/deployment/templates/node-1/_template"
    value = "${file("files/deployment-templates/node-1.json")}"
  }
}

// branch/env-type/env-id/app-name/version
resource consul_key_prefix "node-1-defaults" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/_default/"

  subkeys {
    "runtime/cpu"                       = "0.2"
    "runtime/memory"                    = "1500"
    "runtime/instances"                 = "1"
    "runtime/health_port_index"         = "0"
    "runtime/grace_period_seconds"      = "60"
    "properties/server.port"            = "$PORT0"
    "properties/management.port"        = "$PORT2"
    "properties/environment"            = "$ENV"                             //qa
    "properties/cluster_name"           = "$CLUSTER_NAME"                    //qa-1
    "properties/cluster_id"             = "$CLUSTER_ID"                      //qa_1
    "properties/platformId"             = "$PLATFORM_ID"
    "properties/projectId"              = "$PROJECT_ID"
    "properties/namespace"              = "$ENV_ID"
    "env_vars/NODE_BUNDLE"              = "node-8.2.1-npm-5.3.0-yarn-0.27.5"
    "env_vars/REACT_APP_AUTH0_CLIENTID" = "UwBbgScZucA1ygpMxvkGqdvtezSBbCMs"
    "env_vars/NODE_COMMAND"             = "yarn run start"
    "env_vars/BUILD_COMMAND"            = "yarn run build"

    //  that is for dev
    // for production use "qGM1Yu8d3ZxJd15aV25Hv1akejSgVdXx"
  }
}

// ****************
// * KANE SERVERS *
// ****************

resource consul_key_prefix "deployment_platform_node_1_kane_server_uat_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-kane-server/"

  subkeys {
    //"env_vars/KANE_MYSQL_URL" = "mysql://serviceuser:s3rv1c3Us3r@devdb.cwxg534lxijj.eu-west-1.rds.amazonaws.com/telematics_uat_1_tt_kane"
    "env_vars/KANE_MYSQL_URL"              = "mysql://serviceuser:s3rv1c3Us3r@devdb.cwxg534lxijj.eu-west-1.rds.amazonaws.com/uat_shared_tt_kane"
    "env_vars/NODE_COMMAND"                = "yarn run serve"
    "env_vars/API_ENDPOINT"                = "https://fs-api-backoffice-router-uat-1.ttmcorp.com"
    "env_vars/TYPE"                        = "service_account"
    "env_vars/KANE_FIREBASE_PROJECT_ID"    = "fir-admin-auth"
    "env_vars/KANE_FIREBASE_PRIVATE_KEY"   = "-----BEGIN%20PRIVATE%20KEY-----%0AMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCncvOCxOKCIsm0%0AFJiINp7G9PsXF7criedjxGT8J29gBczSIS72EMX6TZ47VR35wywCnX7nIwtRe7sL%0AjGwQNYI9mtKSUVUVz6BHcZ6N7aNPFL6sB4RfF19cmENgGJteV1TyNTP0jhmJXGRb%0Ae4QhOfq7eGMQ%2FIuHI%2BrcdH9hDxLmNiiyRZUbYxpaEaqpXQw2zdfqk3LEetjx7aBa%0As1pkL3%2BA7kE16Wb4KgQnjW1pJH5JPnc3e1ObfFfrSaBJ%2FIQHuiYObbOXHrSNZal4%0A%2FrW3V57evpCe3B2QXPez0TwfTavK0m6jlyHBCPQM7AyO8NVAFz1WJ4zCDxpOXVhV%0AX2qkITv%2FAgMBAAECggEADAp%2FaZc61TInNaSIK2iyFjXQ8HeAxnK43uW6f%2FSzVOur%0AL4O6BXEYq3J1ab1%2FKvyr54GSeeXSjzN0idS5Le6LGl74Zr55KIOw%2BjI4vT%2F%2FZFUQ%0A46dOuip2GF8inoBme8GP7DLYikIa9%2F5a1nFAA8ZQOgw4I3qQQKbb4VUclnV9CGzH%0A%2Bq%2FOZgb8tFctVPW%2B%2Fd9SIQhLFes%2BEDxnXN5z2WwlD5eCYzXcTDxKtoSa2rSWb1NJ%0AVyOl0KEOml9lXhwN%2FTwkMUteduSAwcfVM%2BRUZQWJFl9CQNK6Q5bmn6YcyxzsATVz%0AfbjwEh1FRgOkCfPUyihp95w9gzzhUNak%2BSa9ehiMIQKBgQDla3oDXLpsp0YbaZMu%0Aeq8mWo1U8O%2F9H8a9KltLlbdoSPF3%2BW4TojaXZKE5KALu9WpDCRQF3eOdsWvXIfV7%0A88AnKL4T291YX7DcOhX8ajg0mXZ8CV%2FXl2cviJIBiJLc56MleZPdLjBaRGb1R4Fg%0A3vP0k3caEKzvYosYyd%2Bmqp3%2F5wKBgQC62XBuVghRPkNyfMmdgtkNmb4dojyZpjLJ%0An%2B5FyW%2BEkIA1n40PpU0KUp9a3DtRn2xzEAnRi9OnY6hrdc%2BwxHFH76s4CQcS5rrn%0An%2FP4kSMjqE8uQWokrJfLN6DpVFPvQY2M24yIKkIgbmelTEuP3edXrfabWpegV%2F77%0AufEL4yrAKQKBgBoFI5SICtjR7ACl67yEy6K0%2BHyeywlRdk%2F0IuCivWeFhmWOE68E%0AYd6v2h1KbXK%2BwOuTdwLnVK6I9eszfc0uKygF1fEYqbUhPUTtDZtQTMmici7FPWqN%0AUHWyrqSf4dBOMVHJxTx7xZ4nTeoZzxG2l8KwivyY0ZRg%2F9ey5bvYdA0XAoGANGTO%0AgxH6yRVFgm%2F9dP6A8LBgwByDE66n5SPEg6slay291cuEmZOHk9JxPuAsfgnPNkCU%0AED8%2BPVzTmDZz46RiA4LwJwlZA3LRjCutFNFV4iTK9zR%2BmAQRmHlj1KnB04A1Of9C%0AW9hFrKRoydAoDBJ0UYDFevCfPeYO6PVN40By92kCgYEAkTRwDdPNLhsRnmrelIyx%0A847Wwjev3hw5DkDqG7ovPJNL5ZxwgBn536Z4KgjqajmBbO7GHCYT8FiDtIepU6Xd%0AOcV4Sb7KPCAzmB%2FzGTnWd1jhlVqzQiHAztsOVaNjRwlg9C6yHoUSqg0OQbCEFx50%0ACPtGk6sgBYfXBll2cVLQgUc%3D%0A-----END%20PRIVATE%20KEY-----%0A"
    "env_vars/KANE_FIREBASE_EMAIL"         = "firebase-adminsdk-5ab0r@fir-admin-auth.iam.gserviceaccount.com"
    "env_vars/KANE_MYSQL_URL"              = "mysql://serviceuser:s3rv1c3Us3r@devdb.cwxg534lxijj.eu-west-1.rds.amazonaws.com/uat_shared_tt_kane"
    "env_vars/KANE_CORS_ORIGIN"            = "https://kane-client-uat-1.ttmcorp.com"
    "env_vars/CLIENT_ID"                   = "114004952871366114312"
    "env_vars/AUTH_URI"                    = "https://accounts.google.com/o/oauth2/auth"
    "env_vars/TOKEN_URI"                   = "https://accounts.google.com/o/oauth2/token"
    "env_vars/AUTH_PROVIDER_X509_CERT_URL" = "https://www.googleapis.com/oauth2/v1/certs"
    "env_vars/CLIENT_X509_CERT_URL"        = "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-5ab0r%40fir-admin-auth.iam.gserviceaccount.com"
  }
}

resource consul_key_prefix "deployment_platform_node_1_kane_server_ci_develop" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/develop/app-name/app-kane-server/"

  subkeys {
    "env_vars/KANE_MYSQL_URL"              = "mysql://serviceuser:s3rv1c3Us3r@devdb.cwxg534lxijj.eu-west-1.rds.amazonaws.com/telematics_ci_develop_tt_kane"
    "env_vars/NODE_COMMAND"                = "yarn run serve"
    "env_vars/API_ENDPOINT"                = "https://fs-api-backoffice-router-ci.ttmcorp.com"
    "env_vars/TYPE"                        = "service_account"
    "env_vars/KANE_FIREBASE_PROJECT_ID"    = "fir-admin-auth"
    "env_vars/KANE_FIREBASE_PRIVATE_KEY"   = "-----BEGIN%20PRIVATE%20KEY-----%0AMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCncvOCxOKCIsm0%0AFJiINp7G9PsXF7criedjxGT8J29gBczSIS72EMX6TZ47VR35wywCnX7nIwtRe7sL%0AjGwQNYI9mtKSUVUVz6BHcZ6N7aNPFL6sB4RfF19cmENgGJteV1TyNTP0jhmJXGRb%0Ae4QhOfq7eGMQ%2FIuHI%2BrcdH9hDxLmNiiyRZUbYxpaEaqpXQw2zdfqk3LEetjx7aBa%0As1pkL3%2BA7kE16Wb4KgQnjW1pJH5JPnc3e1ObfFfrSaBJ%2FIQHuiYObbOXHrSNZal4%0A%2FrW3V57evpCe3B2QXPez0TwfTavK0m6jlyHBCPQM7AyO8NVAFz1WJ4zCDxpOXVhV%0AX2qkITv%2FAgMBAAECggEADAp%2FaZc61TInNaSIK2iyFjXQ8HeAxnK43uW6f%2FSzVOur%0AL4O6BXEYq3J1ab1%2FKvyr54GSeeXSjzN0idS5Le6LGl74Zr55KIOw%2BjI4vT%2F%2FZFUQ%0A46dOuip2GF8inoBme8GP7DLYikIa9%2F5a1nFAA8ZQOgw4I3qQQKbb4VUclnV9CGzH%0A%2Bq%2FOZgb8tFctVPW%2B%2Fd9SIQhLFes%2BEDxnXN5z2WwlD5eCYzXcTDxKtoSa2rSWb1NJ%0AVyOl0KEOml9lXhwN%2FTwkMUteduSAwcfVM%2BRUZQWJFl9CQNK6Q5bmn6YcyxzsATVz%0AfbjwEh1FRgOkCfPUyihp95w9gzzhUNak%2BSa9ehiMIQKBgQDla3oDXLpsp0YbaZMu%0Aeq8mWo1U8O%2F9H8a9KltLlbdoSPF3%2BW4TojaXZKE5KALu9WpDCRQF3eOdsWvXIfV7%0A88AnKL4T291YX7DcOhX8ajg0mXZ8CV%2FXl2cviJIBiJLc56MleZPdLjBaRGb1R4Fg%0A3vP0k3caEKzvYosYyd%2Bmqp3%2F5wKBgQC62XBuVghRPkNyfMmdgtkNmb4dojyZpjLJ%0An%2B5FyW%2BEkIA1n40PpU0KUp9a3DtRn2xzEAnRi9OnY6hrdc%2BwxHFH76s4CQcS5rrn%0An%2FP4kSMjqE8uQWokrJfLN6DpVFPvQY2M24yIKkIgbmelTEuP3edXrfabWpegV%2F77%0AufEL4yrAKQKBgBoFI5SICtjR7ACl67yEy6K0%2BHyeywlRdk%2F0IuCivWeFhmWOE68E%0AYd6v2h1KbXK%2BwOuTdwLnVK6I9eszfc0uKygF1fEYqbUhPUTtDZtQTMmici7FPWqN%0AUHWyrqSf4dBOMVHJxTx7xZ4nTeoZzxG2l8KwivyY0ZRg%2F9ey5bvYdA0XAoGANGTO%0AgxH6yRVFgm%2F9dP6A8LBgwByDE66n5SPEg6slay291cuEmZOHk9JxPuAsfgnPNkCU%0AED8%2BPVzTmDZz46RiA4LwJwlZA3LRjCutFNFV4iTK9zR%2BmAQRmHlj1KnB04A1Of9C%0AW9hFrKRoydAoDBJ0UYDFevCfPeYO6PVN40By92kCgYEAkTRwDdPNLhsRnmrelIyx%0A847Wwjev3hw5DkDqG7ovPJNL5ZxwgBn536Z4KgjqajmBbO7GHCYT8FiDtIepU6Xd%0AOcV4Sb7KPCAzmB%2FzGTnWd1jhlVqzQiHAztsOVaNjRwlg9C6yHoUSqg0OQbCEFx50%0ACPtGk6sgBYfXBll2cVLQgUc%3D%0A-----END%20PRIVATE%20KEY-----%0A"
    "env_vars/KANE_FIREBASE_EMAIL"         = "firebase-adminsdk-5ab0r@fir-admin-auth.iam.gserviceaccount.com"
    "env_vars/CLIENT_ID"                   = "114004952871366114312"
    "env_vars/AUTH_URI"                    = "https://accounts.google.com/o/oauth2/auth"
    "env_vars/TOKEN_URI"                   = "https://accounts.google.com/o/oauth2/token"
    "env_vars/AUTH_PROVIDER_X509_CERT_URL" = "https://www.googleapis.com/oauth2/v1/certs"
    "env_vars/CLIENT_X509_CERT_URL"        = "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-5ab0r%40fir-admin-auth.iam.gserviceaccount.com"
  }
}

resource consul_key_prefix "deployment_platform_node_1_kane_server_ci_5" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/5/app-name/app-kane-server/"

  subkeys {
    "env_vars/KANE_MYSQL_URL"                            = "mysql://kane:YhrLrqWu1S8IJq@terraform-007c6c4e7e51e84d7073a70604.cwxg534lxijj.eu-west-1.rds.amazonaws.com"
    "env_vars/NODE_COMMAND"                              = "yarn run serve"
    "env_vars/TYPE"                                      = "service_account"
    "env_vars/KANE_FIREBASE_PROJECT_ID"                  = "fir-admin-auth"
    "env_vars/KANE_FIREBASE_PRIVATE_KEY"                 = "-----BEGIN%20PRIVATE%20KEY-----%0AMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCncvOCxOKCIsm0%0AFJiINp7G9PsXF7criedjxGT8J29gBczSIS72EMX6TZ47VR35wywCnX7nIwtRe7sL%0AjGwQNYI9mtKSUVUVz6BHcZ6N7aNPFL6sB4RfF19cmENgGJteV1TyNTP0jhmJXGRb%0Ae4QhOfq7eGMQ%2FIuHI%2BrcdH9hDxLmNiiyRZUbYxpaEaqpXQw2zdfqk3LEetjx7aBa%0As1pkL3%2BA7kE16Wb4KgQnjW1pJH5JPnc3e1ObfFfrSaBJ%2FIQHuiYObbOXHrSNZal4%0A%2FrW3V57evpCe3B2QXPez0TwfTavK0m6jlyHBCPQM7AyO8NVAFz1WJ4zCDxpOXVhV%0AX2qkITv%2FAgMBAAECggEADAp%2FaZc61TInNaSIK2iyFjXQ8HeAxnK43uW6f%2FSzVOur%0AL4O6BXEYq3J1ab1%2FKvyr54GSeeXSjzN0idS5Le6LGl74Zr55KIOw%2BjI4vT%2F%2FZFUQ%0A46dOuip2GF8inoBme8GP7DLYikIa9%2F5a1nFAA8ZQOgw4I3qQQKbb4VUclnV9CGzH%0A%2Bq%2FOZgb8tFctVPW%2B%2Fd9SIQhLFes%2BEDxnXN5z2WwlD5eCYzXcTDxKtoSa2rSWb1NJ%0AVyOl0KEOml9lXhwN%2FTwkMUteduSAwcfVM%2BRUZQWJFl9CQNK6Q5bmn6YcyxzsATVz%0AfbjwEh1FRgOkCfPUyihp95w9gzzhUNak%2BSa9ehiMIQKBgQDla3oDXLpsp0YbaZMu%0Aeq8mWo1U8O%2F9H8a9KltLlbdoSPF3%2BW4TojaXZKE5KALu9WpDCRQF3eOdsWvXIfV7%0A88AnKL4T291YX7DcOhX8ajg0mXZ8CV%2FXl2cviJIBiJLc56MleZPdLjBaRGb1R4Fg%0A3vP0k3caEKzvYosYyd%2Bmqp3%2F5wKBgQC62XBuVghRPkNyfMmdgtkNmb4dojyZpjLJ%0An%2B5FyW%2BEkIA1n40PpU0KUp9a3DtRn2xzEAnRi9OnY6hrdc%2BwxHFH76s4CQcS5rrn%0An%2FP4kSMjqE8uQWokrJfLN6DpVFPvQY2M24yIKkIgbmelTEuP3edXrfabWpegV%2F77%0AufEL4yrAKQKBgBoFI5SICtjR7ACl67yEy6K0%2BHyeywlRdk%2F0IuCivWeFhmWOE68E%0AYd6v2h1KbXK%2BwOuTdwLnVK6I9eszfc0uKygF1fEYqbUhPUTtDZtQTMmici7FPWqN%0AUHWyrqSf4dBOMVHJxTx7xZ4nTeoZzxG2l8KwivyY0ZRg%2F9ey5bvYdA0XAoGANGTO%0AgxH6yRVFgm%2F9dP6A8LBgwByDE66n5SPEg6slay291cuEmZOHk9JxPuAsfgnPNkCU%0AED8%2BPVzTmDZz46RiA4LwJwlZA3LRjCutFNFV4iTK9zR%2BmAQRmHlj1KnB04A1Of9C%0AW9hFrKRoydAoDBJ0UYDFevCfPeYO6PVN40By92kCgYEAkTRwDdPNLhsRnmrelIyx%0A847Wwjev3hw5DkDqG7ovPJNL5ZxwgBn536Z4KgjqajmBbO7GHCYT8FiDtIepU6Xd%0AOcV4Sb7KPCAzmB%2FzGTnWd1jhlVqzQiHAztsOVaNjRwlg9C6yHoUSqg0OQbCEFx50%0ACPtGk6sgBYfXBll2cVLQgUc%3D%0A-----END%20PRIVATE%20KEY-----%0A"
    "env_vars/KANE_FIREBASE_EMAIL"                       = "firebase-adminsdk-5ab0r@fir-admin-auth.iam.gserviceaccount.com"
    "env_vars/KANE_FIREBASE_CLIENT_ID"                   = "114004952871366114312"
    "env_vars/KANE_FIREBASE_AUTH_URI"                    = "https://accounts.google.com/o/oauth2/auth"
    "env_vars/KANE_FIREBASE_TOKEN_URI"                   = "https://accounts.google.com/o/oauth2/token"
    "env_vars/KANE_FIREBASE_AUTH_PROVIDER_X509_CERT_URL" = "https://www.googleapis.com/oauth2/v1/certs"
    "env_vars/KANE_FIREBASE_CLIENT_X509_CERT_URL"        = "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-5ab0r%40fir-admin-auth.iam.gserviceaccount.com"
  }
}

resource consul_key_prefix "deployment_platform_node_1_kane_server_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-kane-server/"

  subkeys {
    "env_vars/KANE_MYSQL_URL"                            = "mysql://serviceuser:s3rv1c3Us3r@devdb.cwxg534lxijj.eu-west-1.rds.amazonaws.com/telematics_ci_1_tt_kane"
    "env_vars/NODE_COMMAND"                              = "yarn run serve"
    "env_vars/API_ENDPOINT"                              = "https://fs-api-backoffice-router-ci-1.ttmcorp.com"
    "env_vars/TYPE"                                      = "service_account"
    "env_vars/KANE_FIREBASE_PROJECT_ID"                  = "fir-admin-auth"
    "env_vars/KANE_FIREBASE_PRIVATE_KEY"                 = "-----BEGIN%20PRIVATE%20KEY-----%0AMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCncvOCxOKCIsm0%0AFJiINp7G9PsXF7criedjxGT8J29gBczSIS72EMX6TZ47VR35wywCnX7nIwtRe7sL%0AjGwQNYI9mtKSUVUVz6BHcZ6N7aNPFL6sB4RfF19cmENgGJteV1TyNTP0jhmJXGRb%0Ae4QhOfq7eGMQ%2FIuHI%2BrcdH9hDxLmNiiyRZUbYxpaEaqpXQw2zdfqk3LEetjx7aBa%0As1pkL3%2BA7kE16Wb4KgQnjW1pJH5JPnc3e1ObfFfrSaBJ%2FIQHuiYObbOXHrSNZal4%0A%2FrW3V57evpCe3B2QXPez0TwfTavK0m6jlyHBCPQM7AyO8NVAFz1WJ4zCDxpOXVhV%0AX2qkITv%2FAgMBAAECggEADAp%2FaZc61TInNaSIK2iyFjXQ8HeAxnK43uW6f%2FSzVOur%0AL4O6BXEYq3J1ab1%2FKvyr54GSeeXSjzN0idS5Le6LGl74Zr55KIOw%2BjI4vT%2F%2FZFUQ%0A46dOuip2GF8inoBme8GP7DLYikIa9%2F5a1nFAA8ZQOgw4I3qQQKbb4VUclnV9CGzH%0A%2Bq%2FOZgb8tFctVPW%2B%2Fd9SIQhLFes%2BEDxnXN5z2WwlD5eCYzXcTDxKtoSa2rSWb1NJ%0AVyOl0KEOml9lXhwN%2FTwkMUteduSAwcfVM%2BRUZQWJFl9CQNK6Q5bmn6YcyxzsATVz%0AfbjwEh1FRgOkCfPUyihp95w9gzzhUNak%2BSa9ehiMIQKBgQDla3oDXLpsp0YbaZMu%0Aeq8mWo1U8O%2F9H8a9KltLlbdoSPF3%2BW4TojaXZKE5KALu9WpDCRQF3eOdsWvXIfV7%0A88AnKL4T291YX7DcOhX8ajg0mXZ8CV%2FXl2cviJIBiJLc56MleZPdLjBaRGb1R4Fg%0A3vP0k3caEKzvYosYyd%2Bmqp3%2F5wKBgQC62XBuVghRPkNyfMmdgtkNmb4dojyZpjLJ%0An%2B5FyW%2BEkIA1n40PpU0KUp9a3DtRn2xzEAnRi9OnY6hrdc%2BwxHFH76s4CQcS5rrn%0An%2FP4kSMjqE8uQWokrJfLN6DpVFPvQY2M24yIKkIgbmelTEuP3edXrfabWpegV%2F77%0AufEL4yrAKQKBgBoFI5SICtjR7ACl67yEy6K0%2BHyeywlRdk%2F0IuCivWeFhmWOE68E%0AYd6v2h1KbXK%2BwOuTdwLnVK6I9eszfc0uKygF1fEYqbUhPUTtDZtQTMmici7FPWqN%0AUHWyrqSf4dBOMVHJxTx7xZ4nTeoZzxG2l8KwivyY0ZRg%2F9ey5bvYdA0XAoGANGTO%0AgxH6yRVFgm%2F9dP6A8LBgwByDE66n5SPEg6slay291cuEmZOHk9JxPuAsfgnPNkCU%0AED8%2BPVzTmDZz46RiA4LwJwlZA3LRjCutFNFV4iTK9zR%2BmAQRmHlj1KnB04A1Of9C%0AW9hFrKRoydAoDBJ0UYDFevCfPeYO6PVN40By92kCgYEAkTRwDdPNLhsRnmrelIyx%0A847Wwjev3hw5DkDqG7ovPJNL5ZxwgBn536Z4KgjqajmBbO7GHCYT8FiDtIepU6Xd%0AOcV4Sb7KPCAzmB%2FzGTnWd1jhlVqzQiHAztsOVaNjRwlg9C6yHoUSqg0OQbCEFx50%0ACPtGk6sgBYfXBll2cVLQgUc%3D%0A-----END%20PRIVATE%20KEY-----%0A"
    "env_vars/KANE_FIREBASE_EMAIL"                       = "firebase-adminsdk-5ab0r@fir-admin-auth.iam.gserviceaccount.com"
    "env_vars/KANE_FIREBASE_CLIENT_ID"                   = "114004952871366114312"
    "env_vars/KANE_FIREBASE_AUTH_URI"                    = "https://accounts.google.com/o/oauth2/auth"
    "env_vars/KANE_FIREBASE_TOKEN_URI"                   = "https://accounts.google.com/o/oauth2/token"
    "env_vars/KANE_FIREBASE_AUTH_PROVIDER_X509_CERT_URL" = "https://www.googleapis.com/oauth2/v1/certs"
    "env_vars/KANE_FIREBASE_CLIENT_X509_CERT_URL"        = "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-5ab0r%40fir-admin-auth.iam.gserviceaccount.com"
  }
}

resource consul_key_prefix "deployment_platform_node_1_kane_server_staging_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-kane-server/"

  subkeys {
    //"env_vars/KANE_MYSQL_URL" = "mysql://serviceuser:s3rv1c3Us3r@devdb.cwxg534lxijj.eu-west-1.rds.amazonaws.com/telematics_uat_1_tt_kane"
    "env_vars/KANE_MYSQL_URL"                            = "mysql://serviceuser:s3rv1c3Us3r@devdb.cwxg534lxijj.eu-west-1.rds.amazonaws.com/telematics_staging_1_tt_kane"
    "env_vars/NODE_COMMAND"                              = "yarn run serve"
    "env_vars/API_ENDPOINT"                              = "https://fs-api-backoffice-router-staging.ttmcorp.com"
    "env_vars/TYPE"                                      = "service_account"
    "env_vars/KANE_FIREBASE_PROJECT_ID"                  = "kane-staging"
    "env_vars/KANE_FIREBASE_PRIVATE_KEY"                 = "-----BEGIN%20PRIVATE%20KEY-----%0AMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQD0iVOTui5ePyfA%0AGpjRfYoZYmLsmtJjNEjB8JwnLah19Q%2FqhjFHTb6hnTcDieDJQfR85OFLyj5t9C81%0AwKM6hpCY4SIA6sI%2FHNiMGAOgrEk746Bal6g3l8Vp%2B%2Ba34qX%2BYg%2FEqz8WCt554XkY%0AWNJKH7uIG09s2Ae9DxBlpKyB1JErSIzVMkl%2BA92%2FN0ehkYisYkgM5p1ox2tqAtus%0AOBiR4etP45XkXqaySAbTCW8qVF0ObdogLkkreZsXelP2kL8FASQGiL9pWmivGAfQ%0AAJ9QpfOwUgnG0G4kdzPtjbH6efp9UfgCPJWuzUbs9M50Z%2FXIcypPyvseNRbyLfCJ%0A0QQLwMUFAgMBAAECggEACVbhWUgIE1NoKj8ZzQ51YsjOLGkxVdaeXMHaxOhKgc7N%0AP9DGOwnuGPAPNNRkQGuDce2v6RwdTEfCMdRcp5SSGzSl%2FtD4gD6Syq70HIPX%2FDCS%0AZENcilsCkCytwCIo4tFN9t3vd%2BEMZC1HWbWyumAB4o3gd7C8guz7XFZwvDipHrUA%0Ahu276vWaVMdXJFtS%2FB3FyF4g2wOyxLozZa8tFURtQR%2BgYM7cMttM4Nhd35g2Acur%0AgxrofOMAzx%2FQLgeoezpPQla8Jb1FU4vSRJyX5H6Sy715inKAO24A0VTTw%2BhCnVCG%0AIb67JYzTSF%2BIEhuxY3qdX49nbKizLHMlPXiyZF%2Fa9QKBgQD9c0RKeZ%2BdzdgsYbe4%0A3vOf7sxDrAJHri9xTsxXdMyalO%2B%2BXhrF2AryMaRMivRbdl8tWfs2%2F%2F6%2FAR7Tgj5m%0AQWPXSK70J23j6OhdhXBClhCRdoV5t7%2BMo3WmSE5M9tZGklbygREtFPtuwMKjOUMb%0AlZsR6N1JboUdprhrGg1sf5DIBwKBgQD2%2Fxpmc2lVe%2F2k8hKKadbJCVmI%2BZGDAzfw%0A%2BTubBWeyjafmnfgXFqIoWualpMl%2Brau9xnadmECx76IKv9GhMCbqSNieg4sKrH%2Fe%0AnOlKuBTKnmmHk%2Bt67Usq6el4x%2Bgs2mXA2Vi6fwai99lYyAEBJg8ittRu5I1RpaKO%0AESn8SI6PkwKBgQCPTl2OvrZBw%2BI3NglrvqSCCcP1f4zKevLYXnWr%2FZ6yyQPF3%2B%2FD%0AbiTCTM9ZeDgyfvvR6M7iilzAIyIK9dnSd6a8U5yY8LJx6393YbvX6oHzvdA6wmrM%0AWqBrJPchDXCa9qOy%2Bsd7diG0vclQAVCwiCbmOYTlwFgELACajmCIuJhkgQKBgEB2%0AM52pYUQ%2BJ%2BR0iQI2vSeamRN2jtNyc5cgdL3wmy%2BuaLN3ZtgfwBJoJun0J%2BI30Sqm%0AMS7QHZJSyCemCD%2FZIU7j1CUKj68pdfXTXc9yW3rF%2FSS1Ji%2Fe3k9E3Pn4sk%2BCcCv0%0AjYOew0ylAsR47koudKTKsoZ%2Bf6NfZx1MvXhqN0qhAoGAPVf0MpJdA1%2Fe%2B2McRiyE%0AKR2qXlSdldUSKHab6Lrb8FnRPK6TZW1qs0Qzkm7%2FhX9keqFDqIZukaSmOoRtAnfh%0A56w%2BAYMKmwIqt%2FDw5Kkunnf%2ByMJIeyG%2BaePDcC2F2KXj3pz%2FswVOZY%2FSQ9lvzpo4%0AptzmUYYa4t4Eskf0mWsbPEY%3D%0A-----END%20PRIVATE%20KEY-----%0A"
    "env_vars/KANE_FIREBASE_EMAIL"                       = "firebase-adminsdk-dydty@kane-staging.iam.gserviceaccount.com"
    "env_vars/KANE_CORS_ORIGIN"                          = "https://kane-client-staging-1.ttmcorp.com"
    "env_vars/KANE_FIREBASE_CLIENT_ID"                   = "106804317067369620298"
    "env_vars/KANE_FIREBASE_AUTH_URI"                    = "https://accounts.google.com/o/oauth2/auth"
    "env_vars/KANE_FIREBASE_TOKEN_URI"                   = "https://accounts.google.com/o/oauth2/token"
    "env_vars/KANE_FIREBASE_AUTH_PROVIDER_X509_CERT_URL" = "https://www.googleapis.com/oauth2/v1/certs"
    "env_vars/KANE_FIREBASE_CLIENT_X509_CERT_URL"        = "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-dydty%40kane-staging.iam.gserviceaccount.com"
  }
}

resource consul_key_prefix "deployment_platform_node_1_kane_server_prod_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/prod/env-id/1/app-name/app-kane-server/"

  subkeys {
    //"env_vars/KANE_MYSQL_URL" = "mysql://serviceuser:s3rv1c3Us3r@devdb.cwxg534lxijj.eu-west-1.rds.amazonaws.com/telematics_uat_1_tt_kane"
    "env_vars/KANE_MYSQL_URL"                            = "mysql://serviceuser:jfifIdXK75L6RQ4@tf-0020297b221e0351dbc9beaa69.crizhv4ha0xo.eu-west-1.rds.amazonaws.com/telematics_firestarter_prod_tt_kane"
    "env_vars/NODE_COMMAND"                              = "yarn run serve"
    "env_vars/API_ENDPOINT"                              = "https://prod-admin-api-internal.fs-prod.uk"
    "env_vars/TYPE"                                      = "service_account"
    "env_vars/KANE_FIREBASE_PROJECT_ID"                  = "kane-production"
    "env_vars/KANE_FIREBASE_PRIVATE_KEY"                 = "-----BEGIN%20PRIVATE%20KEY-----%0AMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC8fpBocycM%2FVVu%0AW7EIGvHyHCPBrejQS0%2B89gMsxxBW%2BSrH8jJ8kaGO7eSs%2FE54aPEQ6WuXalcoxss0%0Am4nkPwmkRB%2FH11QmPGQsuaVJaPLrnajEX47x5m7ilcnpCottkFUFG%2FdFQt432SGh%0AMES6%2Fx%2BtyEVLhVpDzF2kkCeN2JD0GYN8fHo0wYTCEWRQAvH8L1SLNSbT95YxQ1ZA%0AVmXKoBxefDrzeIEkaKw4savZbFuCMSCS18BV%2FxyFP1ObVRahkrqjCJUuOP%2BRDbbe%0AmwPKsoEuUPQ0ODQYDfAmPLuN1yAh4r9R41o9nzYMp2DH9blx4%2BWzFOKoj6%2Ffd1rG%0Aw43POHHTAgMBAAECggEATBWUSaztftdFJCobr8dckNsd7cA6gNVdZsswpeLmLA0g%0AYRvYmCKagVxyGQcwz3agk7B8jt5MNzfi7yW3R6vQQv%2BCioJWw6ZN0yXOW27i2wtF%0AA5sE0oyuhoRqv0fT1tJTFyBeM5VVqFpbMjqPC6IOUfHMUrsNn98%2Bjz3m0WEwxW5T%0An%2Ba%2BUy7VgSW3SxU3PDKo7g2xyTvO4HrhPtPWEtofNH8rhlQcW8AqN87ZYGi3TJBh%0ADXX3lanrzUjOtZrQEpiZe8W5f%2FOAIXwkLoN7PQBtxJy4urfyNb5cJxcVBfaxLCvN%0ABQ1mRoNM9a2vvQ1JhwPy17fW4USAxD23EI%2FOqNWnGQKBgQDfv2j7L%2BGF4bO1WWSH%0AMFhk%2BkVd0bnPHByuOIZUivFqPgbLDIPJJot6AzSn3Nn1D4j1iLapViL6V1JZf4Hs%0ATm62%2Bshq2pqVXTad0nhNa%2FMJMNHoxQbedbREvw%2BgrcrOb%2FFvVc7UdOvNMVV%2FxSOI%0AdABhXgcA%2FW75nFduke5sKcUN2wKBgQDXqkKC7LCR5SFCM5OPmZ61iKgO48B73NEN%0AmOPdBegca%2FewdvVjtUJqsOwiUVq0eUUIOhDcR%2BNvdzObdI5s4vMLzBCFZMqRJ20n%0AQdG0o1WnY7fi25ZMkI2NBX2cHalqSppsJHM2Xhqeb4R6Un%2FU2cck76I9Q30%2FHLmh%0ASpB1cPA5aQKBgCsuXaxciUG1VWFiZyxQAD88rbZ1i06VNlPJRON8DStb3SPCvoxL%0AjA94HulmjgnlfaElSVtU7Fs0DwiGuyRH3sYhkBxPJBY%2FbxL2U5FdjAHyEhDCNZkp%0ATikWVoXEBKfios3B2bDW%2F8sSAlYuTbU%2F7fLcmq1uO8Sgj58fOsxVI1qRAoGBAIIW%0ADhaBBKSMLDqNd8WXqRbg%2FlY05McR0%2Fu6rD%2BGr5JtQbNsnentzMkkPNsabXcM6yni%0ANIdayYJTyYpMjRh68JW%2BGn0L5fAWcw3J8t9d4m2cha%2FOOmOmkettU24WqjkWFP2E%0AIwa5Tp8gry1FlBvk3O2XqioACo1GG2aB3o8od455AoGBAIakMkSpHH3BJziGdG8K%0A%2FI6vD3upRjuICMG2aEVWv%2B%2F%2FhGXtUxThPo8Sg72EGsfl7L4%2FqiPFS3nGV5s35FpF%0A3kB8EaqPUKevw%2F6mAnPch9xPHjbnAQwUR98PCKjU7eIXIphLZYSbHz1jXl8AVxEi%0ALU3L%2FzLp7LRKsRYfWg4D7taS%0A-----END%20PRIVATE%20KEY-----%0A"
    "env_vars/KANE_FIREBASE_EMAIL"                       = "firebase-adminsdk-hxma1@kane-production.iam.gserviceaccount.com"
    "env_vars/KANE_CORS_ORIGIN"                          = "https://kane-client-prod-1.fs-prod.uk"
    "env_vars/KANE_FIREBASE_CLIENT_ID"                   = "111795093907286715507"
    "env_vars/KANE_FIREBASE_AUTH_URI"                    = "https://accounts.google.com/o/oauth2/auth"
    "env_vars/KANE_FIREBASE_TOKEN_URI"                   = "https://accounts.google.com/o/oauth2/token"
    "env_vars/KANE_FIREBASE_AUTH_PROVIDER_X509_CERT_URL" = "https://www.googleapis.com/oauth2/v1/certs"
    "env_vars/KANE_FIREBASE_CLIENT_X509_CERT_URL"        = "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-hxma1%40kane-production.iam.gserviceaccount.com"
  }
}

// ****************
// * KANE CLIENTS *
// ****************

resource consul_key_prefix "deployment_platform_node_1_kane_client_uat_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-kane-client/"

  subkeys {
    "env_vars/KANE_SERVER_URL"                  = "https://kane-server-uat-1-internal.ttmcorp.com/graphql"
    "env_vars/NODE_COMMAND"                     = "yarn run start"
    "env_vars/BUILD_COMMAND"                    = "yarn run build"
    "env_vars/REACT_APP_FIREBASE_API_KEY"       = "AIzaSyAHu_cxlDuXOfLlgRbdmpV0L1Wr94Ihvxk"
    "env_vars/REACT_APP_FIREBASE_AUTH_DOMAIN"   = "fir-admin-auth.firebaseapp.com"
    "env_vars/REACT_APP_FIREBASE_DB_URL"        = "https://fir-admin-auth.firebaseio.com"
    "env_vars/REACT_APP_FIREBASE_PROJECT_ID"    = "fir-admin-auth"
    "env_vars/REACT_APP_FIREBASE_MSG_SENDER_ID" = "540226607114"
    "env_vars/REACT_APP_GOOGLE_API_KEY"         = "[TANTALUMS_GOOGLE_MAPS_API_KEY]"
  }
}

resource consul_key_prefix "deployment_platform_node_1_kane_client_prod_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/prod/env-id/1/app-name/app-kane-client/"

  subkeys {
    "env_vars/KANE_SERVER_URL"                  = "https://kane-server-prod-1.fs-prod.uk/graphql"
    "env_vars/NODE_COMMAND"                     = "yarn run start"
    "env_vars/BUILD_COMMAND"                    = "yarn run build"
    "env_vars/REACT_APP_FIREBASE_API_KEY"       = "AIzaSyDeemPzNXWxE00aGGeb5bRJEfefIJzTTh0"
    "env_vars/REACT_APP_FIREBASE_AUTH_DOMAIN"   = "kane-production.firebaseapp.com"
    "env_vars/REACT_APP_FIREBASE_DB_URL"        = "https://kane-production.firebaseio.com"
    "env_vars/REACT_APP_FIREBASE_PROJECT_ID"    = "kane-production"
    "env_vars/REACT_APP_FIREBASE_MSG_SENDER_ID" = "449964960689"
    "env_vars/REACT_APP_GOOGLE_API_KEY"         = "[TANTALUMS_GOOGLE_MAPS_API_KEY]"
  }
}

resource consul_key_prefix "deployment_platform_node_1_kane_client_staging_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-kane-client/"

  subkeys {
    "env_vars/KANE_SERVER_URL"                  = "https://kane-server-uat-1.ttmcorp.com/graphql"
    "env_vars/NODE_COMMAND"                     = "yarn run start"
    "env_vars/BUILD_COMMAND"                    = "yarn run build"
    "env_vars/REACT_APP_FIREBASE_API_KEY"       = "AIzaSyAEUGCAyMaQk2EdiI57ygmzvf3CMdhyThQ"
    "env_vars/REACT_APP_FIREBASE_AUTH_DOMAIN"   = "kane-staging.firebaseapp.com"
    "env_vars/REACT_APP_FIREBASE_DB_URL"        = "https://kane-staging.firebaseio.com"
    "env_vars/REACT_APP_FIREBASE_PROJECT_ID"    = "kane-staging"
    "env_vars/REACT_APP_FIREBASE_MSG_SENDER_ID" = "271322465256"
    "env_vars/REACT_APP_GOOGLE_API_KEY"         = "[TANTALUMS_GOOGLE_MAPS_API_KEY]"
  }
}

resource consul_key_prefix "deployment_platform_node_1_kane_client_ci_5" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/5/app-name/app-kane-client/"

  subkeys {
    "env_vars/KANE_SERVER_URL"                  = "https://kane-server-ci-5.ttmcorp.com/graphql"
    "env_vars/NODE_COMMAND"                     = "yarn run start"
    "env_vars/BUILD_COMMAND"                    = "yarn run build"
    "env_vars/REACT_APP_FIREBASE_API_KEY"       = "AIzaSyAHu_cxlDuXOfLlgRbdmpV0L1Wr94Ihvxk"
    "env_vars/REACT_APP_FIREBASE_AUTH_DOMAIN"   = "fir-admin-auth.firebaseapp.com"
    "env_vars/REACT_APP_FIREBASE_DB_URL"        = "https://fir-admin-auth.firebaseio.com"
    "env_vars/REACT_APP_FIREBASE_PROJECT_ID"    = "fir-admin-auth"
    "env_vars/REACT_APP_FIREBASE_MSG_SENDER_ID" = "540226607114"
    "env_vars/REACT_APP_GOOGLE_API_KEY"         = "[TANTALUMS_GOOGLE_MAPS_API_KEY]"
  }
}

resource consul_key_prefix "deployment_platform_node_1_kane_client_ci_develop" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/develop/app-name/app-kane-client/"

  subkeys {
    "env_vars/KANE_SERVER_URL"                  = "https://kane-server-ci.ttmcorp.com/graphql"
    "env_vars/NODE_COMMAND"                     = "node server/server.js"
    "env_vars/BUILD_COMMAND"                    = "yarn run build"
    "env_vars/REACT_APP_FIREBASE_API_KEY"       = "AIzaSyAHu_cxlDuXOfLlgRbdmpV0L1Wr94Ihvxk"
    "env_vars/REACT_APP_FIREBASE_AUTH_DOMAIN"   = "fir-admin-auth.firebaseapp.com"
    "env_vars/REACT_APP_FIREBASE_DB_URL"        = "https://fir-admin-auth.firebaseio.com"
    "env_vars/REACT_APP_FIREBASE_PROJECT_ID"    = "fir-admin-auth"
    "env_vars/REACT_APP_FIREBASE_MSG_SENDER_ID" = "540226607114"
    "env_vars/REACT_APP_GOOGLE_API_KEY"         = "[TANTALUMS_GOOGLE_MAPS_API_KEY]"
    "env_vars/REACT_APP_SENTRY_URI"             = "https://265478fce3d04bb9af59e0b68d94daff@sentry.io/194429"
    "env_vars/REACT_APP_ENVIRONMENT"            = "ci-develop"
  }
}

resource consul_key_prefix "deployment_platform_node_1_kane_client_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-kane-client/"

  subkeys {
    "env_vars/KANE_SERVER_URL"                  = "https://kane-server-ci-1.ttmcorp.com/graphql"
    "env_vars/BUILD_COMMAND"                    = "yarn run build"
    "env_vars/NODE_COMMAND"                     = "node server/server.js"
    "env_vars/REACT_APP_FIREBASE_API_KEY"       = "AIzaSyAHu_cxlDuXOfLlgRbdmpV0L1Wr94Ihvxk"
    "env_vars/REACT_APP_FIREBASE_AUTH_DOMAIN"   = "fir-admin-auth.firebaseapp.com"
    "env_vars/REACT_APP_FIREBASE_DB_URL"        = "https://fir-admin-auth.firebaseio.com"
    "env_vars/REACT_APP_FIREBASE_PROJECT_ID"    = "fir-admin-auth"
    "env_vars/REACT_APP_FIREBASE_MSG_SENDER_ID" = "540226607114"
    "env_vars/REACT_APP_GOOGLE_API_KEY"         = "[TANTALUMS_GOOGLE_MAPS_API_KEY]"
  }
}

// ****************
// * THALA SERVER *
// ****************

resource consul_key_prefix "deployment_platform_node_1_app-thala-server_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-thala-server/"

  subkeys {
    "env_vars/NODE_COMMAND" = "node server/server.js"
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-server_ci_develop" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/develop/app-name/app-thala-server/"

  subkeys {
    "env_vars/NODE_COMMAND" = "node server/server.js"
  }
}

//
// app-thala-client-telefonicauk-idm
//

resource consul_key_prefix "deployment_platform_node_1_app-thala-client-telefonicauk-idm_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-thala-client-telefonicauk-idm/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"        = "yarn run build"
    "env_vars/NODE_COMMAND"         = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"   = "https://api-uat.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST" = "https://api-uat.fs-dev.uk/telefonicauk-auth"
    "env_vars/REACT_APP_IDM_HOST"   = "https://api-uat.fs-dev.uk/telefonicauk-idm"

    //"env_vars/REACT_APP_OAUTH_CLIENT_ID" = "5DxE3BUlgTh6LRh7ZLqlc2E6ZFhVHr3J"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "jS1CHAADa25n22DtsxhrsaGaUpPnmh2r"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "284972601990832"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
    "env_vars/REACT_APP_ENVIRONMENT"       = "ci-1"
  }
}

//
// app-thala-client-telefonicauk-idm
//

resource consul_key_prefix "deployment_platform_node_1_app-thala-client-telefonicauk-idm_uat" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-thala-client-telefonicauk-idm/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"        = "yarn run build"
    "env_vars/NODE_COMMAND"         = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"   = "https://api-uat.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST" = "https://api-uat.fs-dev.uk/telefonicauk-auth"
    "env_vars/REACT_APP_IDM_HOST"   = "https://api-uat.fs-dev.uk/telefonicauk-idm"

    //"env_vars/REACT_APP_OAUTH_CLIENT_ID" = "5DxE3BUlgTh6LRh7ZLqlc2E6ZFhVHr3J"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "jS1CHAADa25n22DtsxhrsaGaUpPnmh2r"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "284972601990832"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
    "env_vars/REACT_APP_ENVIRONMENT"       = "uat-1"
  }
}

// *****************************
// * IDM CLIENTS (CONSENT APPS)*
// *****************************

//
// app-idm-client-att
//

# resource consul_key_prefix "deployment_platform_node_1_app-idm-client-att_ci_1" {
#   path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-idm-client-att/"
#   subkeys {
#     // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
#     "env_vars/BUILD_COMMAND" = "yarn run build",
#     "env_vars/NODE_COMMAND" = "node server/server.js"
#     "env_vars/REACT_APP_API_HOST" = "https://api-uat.fs-dev.uk/thala",
#     "env_vars/REACT_APP_HYDRA_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_IDM_HOST" = "https://api-consent-idm-att-ci-1-consent-idm.ttmcorp.com",
#     "env_vars/REACT_APP_OAUTH_CLIENT_ID" = "w3dr0oHQ0vt4h2KOjXkwig53q7yfYyb3"
#     "env_vars/REACT_APP_SENTRY_URI" = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
#     "env_vars/REACT_APP_GOOGLE_CLIENT_ID" = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com",
#     "env_vars/REACT_APP_FACEBOOK_APP_ID" = "284972601990832",
#     "env_vars/REACT_APP_WHITELABEL_CLIENT" = "att"
#     "env_vars/REACT_APP_ENVIRONMENT" = "uat-1"
#   }
# }

# resource consul_key_prefix "deployment_platform_node_1_app-idm-client-att_uat_1" {
#   path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-idm-client-att/"
#   subkeys {
#     // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
#     "env_vars/BUILD_COMMAND" = "yarn run build",
#     "env_vars/NODE_COMMAND" = "node server/server.js",
#     "env_vars/REACT_APP_API_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_HYDRA_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_IDM_HOST" = "https://api-uat.fs-dev.uk/att-idm",
#     "env_vars/REACT_APP_GOOGLE_CLIENT_ID" = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com",
#     "env_vars/REACT_APP_FACEBOOK_APP_ID" = "284972601990832",
#     "env/vars/REACT_APP_OAUTH_CLIENT_ID" = "w3dr0oHQ0vt4h2KOjXkwig53q7yfYyb3"
#     "env_vars/REACT_APP_SENTRY_URI" = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
#     "env_vars/REACT_APP_WHITELABEL_CLIENT" = "att"
#     "env_vars/REACT_APP_ENVIRONMENT" = "uat-1"
#   }
# }

# resource consul_key_prefix "deployment_platform_node_1_app-idm-client-att_staging_1" {
#   path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-idm-client-att/"
#   subkeys {
#     // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
#     "env_vars/BUILD_COMMAND" = "yarn run build",
#     "env_vars/NODE_COMMAND" = "node server/server.js",
#     "env_vars/REACT_APP_API_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_HYDRA_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_IDM_HOST" = "https://api-qa.fs-dev.uk/att-idm",
#     "env_vars/REACT_APP_GOOGLE_CLIENT_ID" = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com",
#     "env_vars/REACT_APP_FACEBOOK_APP_ID" = "284972601990832",
#     "env/vars/REACT_APP_OAUTH_CLIENT_ID" = "w3dr0oHQ0vt4h2KOjXkwig53q7yfYyb3"
#     "env_vars/REACT_APP_SENTRY_URI" = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
#     "env_vars/REACT_APP_WHITELABEL_CLIENT" = "att"
#     "env_vars/REACT_APP_ENVIRONMENT" = "staging-1"
#   }
# }

//
// app-idm-client-iqcar
//

# resource consul_key_prefix "deployment_platform_node_1_app-idm-client-iqcar_ci_1" {
#   path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-idm-client-iqcar/"
#   subkeys {
#     // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
#     "env_vars/BUILD_COMMAND" = "yarn run build",
#     "env_vars/NODE_COMMAND" = "node server/server.js",
#     "env_vars/REACT_APP_API_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_HYDRA_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_IDM_HOST" = "https://api-consent-idm-iqcar-ci-1-consent-idm.ttmcorp.com",
#     "env_vars/REACT_APP_GOOGLE_CLIENT_ID" = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com",
#     "env_vars/REACT_APP_FACEBOOK_APP_ID" = "284972601990832",
#     "env_vars/REACT_APP_OAUTH_CLIENT_ID" = "WlCpSLSOFUlEeM6IYexqSj6I1kvfp5ve",
#     "env_vars/REACT_APP_SENTRY_URI" = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196",
#     "env_vars/REACT_APP_WHITELABEL_CLIENT" = "iqcar",
#     "env_vars/REACT_APP_ENVIRONMENT" = "ci-1"
#   }
# }

# resource consul_key_prefix "deployment_platform_node_1_app-idm-client-iqcar_uat_1" {
#   path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-idm-client-iqcar/"
#   subkeys {
#     // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
#     "env_vars/BUILD_COMMAND" = "yarn run build",
#     "env_vars/NODE_COMMAND" = "node server/server.js",
#     "env_vars/REACT_APP_API_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_HYDRA_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_IDM_HOST" = "https://api-uat.fs-dev.uk/iqcar-idm",
#     "env_vars/REACT_APP_GOOGLE_CLIENT_ID" = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com",
#     "env_vars/REACT_APP_FACEBOOK_APP_ID" = "137628600254425",
#     "env_vars/REACT_APP_OAUTH_CLIENT_ID" = "WlCpSLSOFUlEeM6IYexqSj6I1kvfp5ve",
#     "env_vars/REACT_APP_SENTRY_URI" = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196",
#     "env_vars/REACT_APP_WHITELABEL_CLIENT" = "iqcar",
#     "env_vars/REACT_APP_ENVIRONMENT" = "uat-1"
#   }
# }

# resource consul_key_prefix "deployment_platform_node_1_app-idm-client-iqcar_staging_1" {
#   path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-idm-client-iqcar/"
#   subkeys {
#     // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
#     "env_vars/BUILD_COMMAND" = "yarn run build",
#     "env_vars/NODE_COMMAND" = "node server/server.js",
#     "env_vars/REACT_APP_API_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_HYDRA_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_IDM_HOST" = "https://api-qa.fs-dev.uk/iqcar-idm",
#     "env_vars/REACT_APP_GOOGLE_CLIENT_ID" = "828379513022-6fufjegb36r481huras2rocm1q3l5lfd.apps.googleusercontent.com",
#     "env_vars/REACT_APP_OAUTH_CLIENT_ID" = "GPYBewjyPOIREsG6EOCUAayPuY2GMhtn",
#     "env_vars/REACT_APP_FACEBOOK_APP_ID" = "284972601990832",
#     "env_vars/REACT_APP_SENTRY_URI" = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196",
#     "env_vars/REACT_APP_WHITELABEL_CLIENT" = "iqcar",
#     "env_vars/REACT_APP_ENVIRONMENT" = "staging-1"
#   }
# }

# resource consul_key_prefix "deployment_platform_node_1_app-idm-client-iqcar_prod_1" {
#   path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/prod/env-id/1/app-name/app-idm-client-iqcar/"
#   subkeys {
#     // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
#     "env_vars/BUILD_COMMAND" = "yarn run build",
#     "env_vars/NODE_COMMAND" = "node server/server.js",
#     "env_vars/REACT_APP_API_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_HYDRA_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_IDM_HOST" = "https://api.fs-prod.uk/iqcar-idm",
#     "env_vars/REACT_APP_GOOGLE_CLIENT_ID" = "828379513022-6fufjegb36r481huras2rocm1q3l5lfd.apps.googleusercontent.com",
#     "env_vars/REACT_APP_OAUTH_CLIENT_ID" = "GPYBewjyPOIREsG6EOCUAayPuY2GMhtn",
#     "env_vars/REACT_APP_FACEBOOK_APP_ID" = "1213869768756440",
#     "env_vars/REACT_APP_SENTRY_URI" = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196",
#     "env_vars/REACT_APP_WHITELABEL_CLIENT" = "iqcar",
#     "env_vars/REACT_APP_ENVIRONMENT" = "prod-1"
#   }
# }
//
// app-idm-client-tantalum
//

# resource consul_key_prefix "deployment_platform_node_1_app-idm-client-tantalum_ci_1" {
#   path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-idm-client-tantalum/"
#   subkeys {
#     // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
#     "env_vars/BUILD_COMMAND" = "yarn run build",
#     "env_vars/NODE_COMMAND" = "node server/server.js",
#     "env_vars/REACT_APP_API_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_HYDRA_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_IDM_HOST" = "https://api-consent-idm-tantalum-ci-1-consent-idm.ttmcorp.com",
#     "env_vars/REACT_APP_GOOGLE_CLIENT_ID" = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com",
#     "env_vars/REACT_APP_FACEBOOK_APP_ID" = "284972601990832",
#     "env/vars/REACT_APP_OAUTH_CLIENT_ID" = "5DxE3BUlgTh6LRh7ZLqlc2E6ZFhVHr3J"
#     "env_vars/REACT_APP_SENTRY_URI" = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
#     "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
#     "env_vars/REACT_APP_ENVIRONMENT" = "ci-1"
#   }
# }

# resource consul_key_prefix "deployment_platform_node_1_app-idm-client-tantalum_uat_1" {
#   path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-idm-client-tantalum/"
#   subkeys {
#     // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
#     "env_vars/BUILD_COMMAND" = "yarn run build",
#     "env_vars/NODE_COMMAND" = "node server/server.js",
#     "env_vars/REACT_APP_API_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_HYDRA_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_IDM_HOST" = "https://api-uat.fs-dev.uk/tantalum-idm",
#     "env_vars/REACT_APP_GOOGLE_CLIENT_ID" = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com",
#     "env_vars/REACT_APP_FACEBOOK_APP_ID" = "284972601990832",
#     "env/vars/REACT_APP_OAUTH_CLIENT_ID" = "5DxE3BUlgTh6LRh7ZLqlc2E6ZFhVHr3J"
#     "env_vars/REACT_APP_SENTRY_URI" = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
#     "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
#     "env_vars/REACT_APP_ENVIRONMENT" = "uat-1"
#   }
# }

# resource consul_key_prefix "deployment_platform_node_1_app-idm-client-tantalum_staging_1" {
#   path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-idm-client-tantalum/"
#   subkeys {
#     // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
#     "env_vars/BUILD_COMMAND" = "yarn run build",
#     "env_vars/NODE_COMMAND" = "node server/server.js",
#     "env_vars/REACT_APP_API_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_HYDRA_HOST" = "https://dev/null",
#     "env_vars/REACT_APP_IDM_HOST" = "https://api-qa.fs-dev.uk/tantalum-idm",
#     "env_vars/REACT_APP_GOOGLE_CLIENT_ID" = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com",
#     "env_vars/REACT_APP_FACEBOOK_APP_ID" = "284972601990832",
#     "env/vars/REACT_APP_OAUTH_CLIENT_ID" = "5DxE3BUlgTh6LRh7ZLqlc2E6ZFhVHr3J"
#     "env_vars/REACT_APP_SENTRY_URI" = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
#     "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
#     "env_vars/REACT_APP_ENVIRONMENT" = "staging-1"
#   }
# }

//
// app-idm-client-telefonicauk
//

resource consul_key_prefix "deployment_platform_node_1_app-idm-client-telefonicauk_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-idm-client-telefonicauk/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://dev/null"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://dev/null"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-consent-idm-telefonicauk-ci-1-consent-idm.ttmcorp.com"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "284972601990832"
    "env/vars/REACT_APP_OAUTH_CLIENT_ID"   = "jS1CHAADa25n22DtsxhrsaGaUpPnmh2r"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
    "env_vars/REACT_APP_ENVIRONMENT"       = "ci-1"
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-idm-client-telefonicauk_uat_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-idm-client-telefonicauk/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://dev/null"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://dev/null"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-uat.fs-dev.uk/telefonicauk-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "284972601990832"
    "env/vars/REACT_APP_OAUTH_CLIENT_ID"   = "jS1CHAADa25n22DtsxhrsaGaUpPnmh2r"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
    "env_vars/REACT_APP_ENVIRONMENT"       = "uat-1"
  }
}

///////////////////////////////////////////////////////////////////////////
//
//  THALA New Naming convention apps 
//  (create new for now. once released delete old apps from ^^above^^)
//
///////////////////////////////////////////////////////////////////////////

//
// THALA-IDM IQCAR
//

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-iqcar_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-thala-idm-iqcar/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-qa.fs-dev.uk/iqcar-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "284972601990832"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "iqcar"
    "env_vars/REACT_APP_ENVIRONMENT"       = "ci-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-iqcar_uat_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-thala-idm-iqcar/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-uat.fs-dev.uk/iqcar-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "137628600254425"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "iqcar"
    "env_vars/REACT_APP_ENVIRONMENT"       = "uat-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-iqcar_staging_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-thala-idm-iqcar/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-qa.fs-dev.uk/iqcar-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "828379513022-6fufjegb36r481huras2rocm1q3l5lfd.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "137628600254425"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "iqcar"
    "env_vars/REACT_APP_ENVIRONMENT"       = "staging-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-iqcar_prod_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/prod/env-id/1/app-name/app-thala-idm-iqcar/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api.fs-prod.uk/iqcar-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "828379513022-6fufjegb36r481huras2rocm1q3l5lfd.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "1213869768756440"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "iqcar"
    "env_vars/REACT_APP_ENVIRONMENT"       = "prod-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

//
// THALA-WEB IQCAR
//

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-iqcar_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-thala-web-iqcar/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api-qa.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api-uat.fs-dev.uk/iqcar-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api-uat.fs-dev.uk"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "WlCpSLSOFUlEeM6IYexqSj6I1kvfp5ve"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "iqcar"
    "env_vars/REACT_APP_ENVIRONMENT"       = "ci-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-iqcar_uat_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-thala-web-iqcar/"

  subkeys {
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api-uat.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api-uat.fs-dev.uk/iqcar-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api-uat.fs-dev.uk"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "WlCpSLSOFUlEeM6IYexqSj6I1kvfp5ve"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "iqcar"
    "env_vars/REACT_APP_ENVIRONMENT"       = "uat-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-iqcar_staging_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-thala-web-iqcar/"

  subkeys {
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api-qa.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api-qa.fs-dev.uk/iqcar-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api-qa.fs-dev.uk"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "GPYBewjyPOIREsG6EOCUAayPuY2GMhtn"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "iqcar"
    "env_vars/REACT_APP_ENVIRONMENT"       = "staging-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-iqcar_prod_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/prod/env-id/1/app-name/app-thala-web-iqcar/"

  subkeys {
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api.fs-prod.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api.fs-prod.uk/iqcar-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api.fs-prod.uk"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "GPYBewjyPOIREsG6EOCUAayPuY2GMhtn"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "iqcar"
    "env_vars/REACT_APP_ENVIRONMENT"       = "prod-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

//
// THALA-IDM TANTALUM
//

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-tantalum_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-thala-idm-tantalum/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-qa.fs-dev.uk/tantalum-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "284972601990832"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
    "env_vars/REACT_APP_ENVIRONMENT"       = "ci-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-tantalum_uat_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-thala-idm-tantalum/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-uat.fs-dev.uk/tantalum-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "137628600254425"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
    "env_vars/REACT_APP_ENVIRONMENT"       = "uat-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-tantalum_staging_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-thala-idm-tantalum/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-qa.fs-dev.uk/tantalum-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "137628600254425"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
    "env_vars/REACT_APP_ENVIRONMENT"       = "staging-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-tantalum_prod_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/prod/env-id/1/app-name/app-thala-idm-tantalum/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api.fs-prod.uk/tantalum-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "162419134463880"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
    "env_vars/REACT_APP_ENVIRONMENT"       = "prod-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

//
// THALA-WEB TANTALUM
//

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-tantalum_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-thala-web-tantalum/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api-qa.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api-uat.fs-dev.uk/tantalum-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api-uat.fs-dev.uk"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "5DxE3BUlgTh6LRh7ZLqlc2E6ZFhVHr3J"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
    "env_vars/REACT_APP_ENVIRONMENT"       = "ci-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-tantalum_uat_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-thala-web-tantalum/"

  subkeys {
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api-uat.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api-uat.fs-dev.uk/tantalum-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api-uat.fs-dev.uk"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "5DxE3BUlgTh6LRh7ZLqlc2E6ZFhVHr3J"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
    "env_vars/REACT_APP_ENVIRONMENT"       = "uat-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-tantalum_staging_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-thala-web-tantalum/"

  subkeys {
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api-qa.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api-qa.fs-dev.uk/tantalum-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api-qa.fs-dev.uk"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "5DxE3BUlgTh6LRh7ZLqlc2E6ZFhVHr3J"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
    "env_vars/REACT_APP_ENVIRONMENT"       = "staging-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-tantalum_prod_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/prod/env-id/1/app-name/app-thala-web-tantalum/"

  subkeys {
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api.fs-prod.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api.fs-prod.uk/tantalum-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api.fs-prod.uk"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "9pBisGRA6hY7npBsQEzN3Smvw3KCZTsn"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "tantalum"
    "env_vars/REACT_APP_ENVIRONMENT"       = "prod-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

//
// THALA-IDM ATT
//

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-att_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-thala-idm-att/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-qa.fs-dev.uk/att-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "284972601990832"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "att"
    "env_vars/REACT_APP_ENVIRONMENT"       = "ci-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-att_uat_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-thala-idm-att/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-uat.fs-dev.uk/att-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "137628600254425"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "att"
    "env_vars/REACT_APP_ENVIRONMENT"       = "uat-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

//
// THALA-WEB ATT
//

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-att_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-thala-web-att/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api-qa.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api-uat.fs-dev.uk/att-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api-uat.fs-dev.uk"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "w3dr0oHQ0vt4h2KOjXkwig53q7yfYyb3"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "att"
    "env_vars/REACT_APP_ENVIRONMENT"       = "ci-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-att_uat_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-thala-web-att/"

  subkeys {
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api-uat.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api-uat.fs-dev.uk/att-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api-uat.fs-dev.uk"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "w3dr0oHQ0vt4h2KOjXkwig53q7yfYyb3"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "att"
    "env_vars/REACT_APP_ENVIRONMENT"       = "uat-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

# resource consul_key_prefix "deployment_platform_node_1_app-thala-web-att_prod_1" {
#   path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/prod/env-id/1/app-name/app-thala-web-att/"
#   subkeys {
#     "env_vars/BUILD_COMMAND" = "yarn run build",
#     "env_vars/NODE_COMMAND" = "node server/server.js",
#     "env_vars/REACT_APP_API_HOST" = "https://api.fs-prod.uk/thala",
#     "env_vars/REACT_APP_HYDRA_HOST" = "https://api.fs-prod.uk/att-auth",
#     "env_vars/REACT_APP_VERIFY_HOST" = "https://api.fs-prod.uk",
#     "env_vars/REACT_APP_OAUTH_CLIENT_ID" = "?????9pBisGRA6hY7npBsQEzN3Smvw3KCZTsn",
#     "env_vars/REACT_APP_SENTRY_URI" = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521",
#     "env_vars/REACT_APP_WHITELABEL_CLIENT" = "att",
#     "env_vars/REACT_APP_ENVIRONMENT" = "prod-1",
#     "env_vars/REACT_APP_BASENAME" = ""
#   }
# }

//
// THALA-IDM THALA
//

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-thala_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-thala-idm-thala/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-qa.fs-dev.uk/thala-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "284972601990832"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "thala"
    "env_vars/REACT_APP_ENVIRONMENT"       = "ci-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-thala_uat_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-thala-idm-thala/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-uat.fs-dev.uk/thala-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "137628600254425"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "thala"
    "env_vars/REACT_APP_ENVIRONMENT"       = "uat-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-thala_staging_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-thala-idm-thala/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api-qa.fs-dev.uk/thala-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "137628600254425"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "thala"
    "env_vars/REACT_APP_ENVIRONMENT"       = "staging-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-idm-thala_prod_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/prod/env-id/1/app-name/app-thala-idm-thala/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_IDM_HOST"          = "https://api.fs-prod.uk/thala-idm"
    "env_vars/REACT_APP_GOOGLE_CLIENT_ID"  = "293200090203-hpqcj29qu44ah33ttfues22hv3d7ut1r.apps.googleusercontent.com"
    "env_vars/REACT_APP_FACEBOOK_APP_ID"   = "137628600254425"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://da2c9b0b495b4b3382afd637cdf0686e@sentry.io/207196"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "thala"
    "env_vars/REACT_APP_ENVIRONMENT"       = "prod-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

//
// THALA-WEB THALA
//

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-thala_ci_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/ci/env-id/1/app-name/app-thala-web-thala/"

  subkeys {
    // "env_vars/NODE_COMMAND" = "./node_modules/.bin/serve -s build",
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api-qa.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api-uat.fs-dev.uk/thala-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api-uat.fs-dev.uk"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "5DxE3BUlgTh6LRh7ZLqlc2E6ZFhVHr3J"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "thala"
    "env_vars/REACT_APP_ENVIRONMENT"       = "ci-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-thala_uat_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-thala-web-thala/"

  subkeys {
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api-uat.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api-uat.fs-dev.uk/thala-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api-uat.fs-dev.uk"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "5DxE3BUlgTh6LRh7ZLqlc2E6ZFhVHr3J"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "thala"
    "env_vars/REACT_APP_ENVIRONMENT"       = "uat-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-thala_staging_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-thala-web-thala/"

  subkeys {
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api-qa.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api-qa.fs-dev.uk/thala-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api-qa.fs-dev.uk"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "5DxE3BUlgTh6LRh7ZLqlc2E6ZFhVHr3J"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "thala"
    "env_vars/REACT_APP_ENVIRONMENT"       = "staging-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "deployment_platform_node_1_app-thala-web-thala_prod_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/prod/env-id/1/app-name/app-thala-web-thala/"

  subkeys {
    "env_vars/BUILD_COMMAND"               = "yarn run build"
    "env_vars/NODE_COMMAND"                = "node server/server.js"
    "env_vars/REACT_APP_API_HOST"          = "https://api.fs-prod.uk/thala"
    "env_vars/REACT_APP_HYDRA_HOST"        = "https://api.fs-prod.uk/thala-auth"
    "env_vars/REACT_APP_VERIFY_HOST"       = "https://api.fs-prod.uk"
    "env_vars/REACT_APP_OAUTH_CLIENT_ID"   = "9pBisGRA6hY7npBsQEzN3Smvw3KCZTsn"
    "env_vars/REACT_APP_SENTRY_URI"        = "https://0b4e042159cf4b6d8a90884f58c31915@sentry.io/235521"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "thala"
    "env_vars/REACT_APP_ENVIRONMENT"       = "prod-1"
    "env_vars/REACT_APP_BASENAME"          = ""
  }
}

resource consul_key_prefix "node_1_app-att-web" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/_any/env-id/_any/app-name/app-att-web/"

  subkeys {
    "env_vars/BUILD_COMMAND"       = "npm run build"
    "env_vars/NODE_COMMAND"        = "npm start"
    "runtime/grace_period_seconds" = "600"
  }
}

resource consul_key_prefix "node_1_app-att-web_uat-1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-att-web/"

  subkeys {
    "env_vars/REACT_APP_TELEMATICS_ROUTER_URL"   = "https://api-uat.fs-dev.uk/thala"
    "env_vars/REACT_APP_HYDRA_LOGIN_WRAPPER_URL" = "https://api-uat.fs-dev.uk/att-auth"
    "env_vars/REACT_APP_CLIENT_ID"               = "w3dr0oHQ0vt4h2KOjXkwig53q7yfYyb3"
  }
}

resource consul_key_prefix "node_1_app-att-web_staging-1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-att-web/"

  subkeys {
    "env_vars/REACT_APP_TELEMATICS_ROUTER_URL"   = "https://api-staging-us.thala.us/thala"
    "env_vars/REACT_APP_HYDRA_LOGIN_WRAPPER_URL" = "https://api-staging-us.thala.us/att-auth"
    "env_vars/REACT_APP_CLIENT_ID"               = "w3dr0oHQ0vt4h2KOjXkwig53q7yfYyb3"
  }
}

resource consul_key_prefix "node_1_app-att-connect" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/_any/env-id/_any/app-name/app-att-web-login/"

  subkeys {
    "env_vars/BUILD_COMMAND"               = "npm run build"
    "env_vars/NODE_COMMAND"                = "npm start"
    "env_vars/REACT_APP_WHITELABEL_CLIENT" = "att"
  }
}

resource consul_key_prefix "node_1_app-att-connect_uat_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/uat/env-id/1/app-name/app-att-web-login/"

  subkeys {
    "env_vars/REACT_APP_IDM_HOST" = "https://api-uat.fs-dev.uk/att-idm"
  }
}

resource consul_key_prefix "node_1_app-att-connect_staging_1" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/staging/env-id/1/app-name/app-att-web-login/"

  subkeys {
    "env_vars/REACT_APP_IDM_HOST" = "https://api-staging-us.thala.us/att-idm"
  }
}

resource consul_key_prefix "node_1_swagger-accident-management" {
  path_prefix = "configuration/deployment/templates/node-1/_settings/platform/_any/project/_any/env-type/_any/env-id/_any/app-name/swagger-accident-management/"

  subkeys {
    "runtime/grace_period_seconds" = "600"
    "env_vars/NODE_COMMAND"        = "./node_modules/http-server/bin/http-server"
  }
}
