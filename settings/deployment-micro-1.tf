resource "consul_keys" "micro_spring_1_template" {
  key {
    path =  "configuration/deployment/templates/micro-1-spring/_template"
    value = "${file("files/deployment-templates/no-image-micro-1.json")}"
  }
}

// branch/env-type/env-id/app-name/version
resource consul_key_prefix "micro-1-spring-defaults" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/_default/"
  subkeys {
    "runtime/cpu" =                                          "0.2"
    "runtime/memory" =                                       "1500"
    "runtime/instances" =                                    "1"
    "runtime/health_port_index" =                            "2"
    "runtime/grace_period_seconds" =                          "60"
    "jvm/Xmx" =                                              "1500m"
    "jvm/server" =                                           ""
    "properties/logPrefix" =                                 "$MESOS_TASK_ID"
    "properties/server.port" =                               "$PORT0"
    "properties/management.port" =                           "$PORT2"
    "properties/spring.cloud.consul.token" =                 "5a87e827-1c05-091d-2cd6-f7d075db4152"
    "properties/spring.cloud.consul.host" =                  "consul.service.consul"
    "properties/com.sun.management.jmxremote" =              "true"
    "properties/com.sun.management.jmxremote.port" =         "$PORT1"
    "properties/com.sun.management.jmxremote.local.only" =   "false"
    "properties/com.sun.management.jmxremote.authenticate" = "false"
    "properties/com.sun.management.jmxremote.ssl" =          "false"
    "properties/spring.profiles.active" =                    "$ENV,$CLUSTER_NAME,$${ENV}-$${ENV_ID}" //qa
    "properties/environment" =                               "$ENV" //qa
    "properties/cluster_name" =                              "$CLUSTER_NAME" //qa-1
    "properties/cluster_id" =                                "$CLUSTER_ID" //qa_1
    "properties/platformId" =                                "$PLATFORM_ID"
    "properties/projectId"  =                                "$PROJECT_ID"
    "properties/namespace" =                                 "$ENV_ID"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_firestarter_any_account" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/_any/env-id/_any/app-name/api-account-tantalum-b2c/"
  subkeys {
    "runtime/memory" = "1700"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_firestarter_sonic" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/_any/env-id/_any/app-name/svc-sonic/"
  subkeys {
    "runtime/cpu" = "1"
    "properties/kafka.mystTopic" = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_myst"
    "properties/cassandra.outboundKeyspace" = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_outbound"
    "properties/paperboy.topics.publication" = "$${CLUSTER_ID}_paperboy_newsagent"
    "properties/paperboy.topics.distribution" = "$${CLUSTER_ID}_paperboy_paper_round"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_shared_myst" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/shared/env-type/_any/env-id/_any/app-name/svc-myst/"
  subkeys {
    "properties/kafka.producer.decoded" = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_myst"
    "properties/kafka.producer.invalid" = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_myst_invalid"
    "properties/helium.topic"           = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_helium"
    "properties/log4j.configurationFile" =                   "log4j2-prod.xml"
  }
}

/* Telematics any PROD specific settings */
resource consul_key_prefix "deployment_platform_micro_1_spring_fs_prod" {
  path_prefix ="configuration/deployment/templates/micro-1-spring/_settings/platform/_any/project/_any/env-type/prod/env-id/_any/app-name/_any/"
  subkeys {
    "runtime/cpu" =       "0.5"
    "runtime/memory" =    "1500"
    "runtime/instances" = "2"
    "runtime/grace_period_seconds" =                          "120"
    "jvm/Xmx" =           "1200m"
    "properties/log4j.configurationFile" =                   "log4j2-kafka.xml"
    "properties/spring.cloud.consul.token"      = "259be361-f20a-bb9d-3955-8da16555185a"
    "properties/spring.cloud.consul.host" = "consul.service.consul"
    "properties/kafka.brokers"                  = "kafka-2-node-0.node.consul:31000,kafka-2-node-1.node.consul:31000,kafka-2-node-2.node.consul:31000"
    "properties/cassandra.hosts"                = "cassandra-1-node-0.node.consul"
    "properties/cassandra.hosts[0]"                = "cassandra-1-node-0.node.consul"
    "properties/cassandra.hosts[1]"                = "cassandra-1-node-1.node.consul"
    "properties/cassandra.hosts[2]"                = "cassandra-1-node-2.node.consul"
    "properties/cassandra.contactPoints"        = "cassandra-1-node-0.node.consul,cassandra-1-node-1.node.consul,cassandra-1-node-2.node.consul"
  }
}


/* Telematics idm PROD specific settings */
resource consul_key_prefix "deployment_platform_micro_1_spring_idm_fs_prod" {
  path_prefix ="configuration/deployment/templates/micro-1-spring/_settings/platform/idm/project/_any/env-type/prod/env-id/_any/app-name/_any/"
  subkeys {
    "properties/spring.datasource.username"     = "admin"
    "properties/spring.datasource.password"     = "v9wen486"
    "properties/spring.datasource.url"          = "jdbc:mysql://terraform-00373eca7852650706c8cd0b99.crizhv4ha0xo.eu-west-1.rds.amazonaws.com:3306/idm_consent_prod_idm?useSSL=false"
  }
}
resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_prod_vehicle" {
  path_prefix ="configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/_any/env-type/prod/env-id/_any/app-name/api-vehicle-tantalum/"
  subkeys {
    "properties/spring.datasource.url"          = "jdbc:mysql://tf-00cf9ff3e39b46d5914908e2c2.crizhv4ha0xo.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_prod_tt_vehicle?useSSL=false"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_prod_vehicle_db" {
  path_prefix ="configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/_any/env-type/prod/env-id/_any/app-name/api-vehicle-db-tantalum/"
  subkeys {
    "properties/spring.datasource.url"          = "jdbc:mysql://tf-00cf9ff3e39b46d5914908e2c2.crizhv4ha0xo.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_prod_tt_vehicle?useSSL=false"
  }
}
/* Telematics Firestarter PROD specific settings */
resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_prod" {
  path_prefix ="configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/_any/env-type/prod/env-id/_any/app-name/_any/"
  subkeys {
    "properties/spring.datasource.username"     = "admin"
    "properties/spring.datasource.password"     = "v9wen486"
    "properties/spring.datasource.url"          = "jdbc:mysql://tf-00cf9ff3e39b46d5914908e2c2.crizhv4ha0xo.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_prod_tt_arm?useSSL=false"
  }
}

resource "consul_key_prefix" "micro-sprint-1-branch-_any-env-type-_any-env-_any-app-name-svc-sonic" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/branch/_any/env-type/_any/env-id/_any/app-name/svc-sonic/"
  subkeys {
    "runtime/cpu" = "1"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fallout" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/_any/project/_any/env-type/_any/env-id/_any/app-name/svc-fallout-tantalum/"
  subkeys {
    "runtime/memory" =                                          "3000"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_prod_fallout" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/prod/env-id/_any/app-name/svc-fallout-tantalum/"
  subkeys {
    "runtime/cpu" =                                          "0.5"
    "runtime/memory" =                                       "2048"
    "runtime/instances" =                                    "1"
    "jvm/Xmx" =                                              "1800m"
    "properties/cassandra.hosts" = "cassandra-1-node-0.node.consul,cassandra-1-node-1.node.consul,cassandra-1-node-2.node.consul"
    "properties/cassandra.keyspace" = "$${PLATFORM_ID}_$${PROJECT_ID}_$${ENV}_sonic"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_prod_paperboy" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/prod/env-id/_any/app-name/svc-paperboy-tantalum/"
  subkeys {
    "properties/publication.topic" = "$${CLUSTER_ID}_paperboy_newsagent"
    "properties/distribution.topic" = "$${CLUSTER_ID}_paperboy_paper_round"
    "properties/cassandra.keyspace" = "$${PLATFORM_ID}_$${PROJECT_ID}_$${ENV}_paperboy"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematifs_fs_prod_telematics" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/prod/env-id/1/app-name/api-telematics-tantalum-b2c/"
  subkeys {
    "properties/cassandra.keyspace" = "$${PLATFORM_ID}_$${PROJECT_ID}_$${ENV}_sonic"
    "properties/kafka.assetSync.topic" = "$${CLUSTER_NAME}_asset_sync"
    "properties/kafka.consumerGroup" = "daas_$${CLUSTER_ID}_asset_sync_1"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_prod_sonic" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/prod/env-id/1/app-name/svc-sonic/"
  subkeys {
    "runtime/cpu" = "2"
    "properties/cassandra.sonicKeyspace" = "$${PLATFORM_ID}_$${PROJECT_ID}_$${ENV}_sonic"
    "properties/cassandra.outboundKeyspace" = "$${PLATFORM_ID}_$${ENV}_outbound"
    "properties/kafka.assetSync.topic" = "$${CLUSTER_NAME}_asset_sync"
    "properties/kafka.consumerGroup" = "daas_$${CLUSTER_ID}_asset_sync_1"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_prod_galaxy" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/prod/env-id/1/app-name/svc-galaxy/"
  subkeys {
    "properties/cassandra.keyspace" = "telematics_firestarter_prod_sonic"
    "properties/cassandra.outboundKeyspace" = "telematics_prod_1_riven"
  }
}


resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_prod_1_arm_tantalum" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/prod/env-id/1/app-name/micro-arm-tantalum-b2c/"
  subkeys {
    "runtime/instances" =                                    "4"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_shared_prod_1_riven" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/shared/env-type/prod/env-id/1/app-name/micro-riven-tantalum/"
  subkeys {
    "properties/cassandra.sonicKeyspace" = "$${PLATFORM_ID}_firestarter_$${ENV}_sonic"
    "properties/cassandra.outboundKeyspace" = "$${PLATFORM_ID}_$${ENV}_outbound"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_firestarter_prod_1_account" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/prod/env-id/1/app-name/api-account-tantalum-b2c/"
  subkeys {
    "properties/cassandra.keyspace" = "$${PLATFORM_ID}_firestarter_$${ENV}_sonic"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_shared_prod_1_half_life" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/shared/env-type/prod/env-id/1/app-name/svc-half-life/"
  subkeys {
    "properties/spring.datasource.url" = "jdbc:mysql://tf-0020297b221e0351dbc9beaa69.crizhv4ha0xo.eu-west-1.rds.amazonaws.com:3306/telematics_prod_tt_raw_data?useSSL=false"
    "properties/spring.datasource.username" = "admin"
    "properties/spring.datasource.password" = "v9wen486"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_shared_prod_1_administration" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/shared/env-type/prod/env-id/1/app-name/api-administration-tantalum/"
  subkeys {
    "properties/cassandra.sonicKeyspace" = "$${PLATFORM_ID}_firestarter_$${ENV}_$${ENV_ID}_sonic"
    "properties/spring.datasource.url" = "jdbc:mysql://tf-0020297b221e0351dbc9beaa69.crizhv4ha0xo.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_prod_tt_admin?useSSL=false"
    "properties/b2b.datasource.url" = "jdbc:mysql://tf-00b366fbf665f5e6d504419e49.crizhv4ha0xo.eu-west-1.rds.amazonaws.com:3306/telematics_armada_prod_tt_arm?useSSL=false"
    "properties/b2c.datasource.url" = "jdbc:mysql://tf-00cf9ff3e39b46d5914908e2c2.crizhv4ha0xo.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_prod_tt_arm?useSSL=false"
    "properties/spring.datasource.username" = "admin"
    "properties/spring.datasource.password" = "v9wen486"
    "properties/b2c.datasource.username" = "admin"
    "properties/b2c.datasource.password" = "v9wen486"
    "properties/b2b.datasource.username" = "admin"
    "properties/b2b.datasource.password" = "v9wen486"
  }
}

// Telematics Firestarter Dev Settings

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_any_paperboy" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/_any/env-id/_any/app-name/paperboy-tantalum/"
  subkeys {
    "properties/publication.topic" = "$${CLUSTER_ID}_paperboy_newsagent"
    "properties/distribution.topic" = "$${CLUSTER_ID}_paperboy_paper_round"
    "properties/cassandra.keyspace" = "$${PLATFORM_ID}_$${PROJECT_ID}_$${ENV}_$${ENV_ID}_paperboy"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_any_telematics" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/_any/env-id/_any/app-name/api-telematics-tantalum-b2c/"
  subkeys {
    "properties/cassandra.keyspace" = "$${CLUSTER_ID}_sonic"
    "properties/publication.topic" = "$${CLUSTER_ID}_paperboy_newsagent"
    "properties/distribution.topic" = "$${CLUSTER_ID}_paperboy_paper_round"
    "properties/kafka.assetSync.topic" = "$${CLUSTER_ID}_asset_sync"
    "properties/kafka.consumerGroup" = "daas_$${CLUSTER_ID}_asset_sync_1"
  }
}

/*
resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_ci_1_arm_tantalum" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/ci/env-id/1/app-name/micro-arm-tantalum-b2c/"
  subkeys {
//    "properties/spring.datasource.url" = "jdbc:mysql://tf-20161129094734731889776yyp.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_ci_1_tt_arm?useSSL=false"
    "properties/spring.datasource.url" = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_ci_1_tt_arm?useSSL=false"
    "properties/spring.datasource.username" = "admin"
     "properties/spring.datasource.password" = "v9wen485"
  }
}
*/

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_ci_2_arm_tantalum" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/ci/env-id/2/app-name/micro-arm-tantalum-b2c/"
  subkeys {
    "properties/spring.datasource.url" = "jdbc:mysql://tf-20161129094734731889776yyp.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_ci_2_tt_arm?useSSL=false"
    "properties/spring.datasource.password" = "weferwf7809tastg79g"
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_shared_ci_any_riven" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/shared/env-type/ci/env-id/_any/app-name/micro-riven-tantalum/"
  subkeys {
    "properties/cassandra.sonicKeyspace" = "$${PLATFORM_ID}_firestarter_$${ENV}_$${ENV_ID}_sonic"
  }
}

/*
resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_shared_ci_1_administration" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/shared/env-type/ci/env-id/1/app-name/api-administration-tantalum/"
  subkeys {
    "properties/spring.datasource.url" = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_ci_1_tt_admin?useSSL=false"
    "properties/b2c.datasource.url" = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_ci_1_tt_arm?useSSL=false"
    "properties/b2b.datasource.url" = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_armada_ci_1_tt_arm?useSSL=false"
    "properties/spring.datasource.username" = "admin"
    "properties/spring.datasource.password" = "v9wen485"
    "properties/b2c.datasource.username" = "admin"
    "properties/b2c.datasource.password" = "v9wen485"
    "properties/b2b.datasource.username" = "admin"
    "properties/b2b.datasource.password" = "v9wen485"
  }
}
*/

// staging-1
resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_staging" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/_any/env-type/staging/env-id/_any/app-name/_any/"
  subkeys {
    "properties/kafka.brokers" = "kafka-1-node-0.node.consul:31000,kafka-1-node-1.node.consul:31000,kafka-1-node-2.node.consul:31000"
    "properties/log4j.configurationFile" = "log4j2-kafka.xml"
    "properties/spring.datasource.password" = "s3rv1c3Us3r"
    "properties/spring.datasource.url" = "jdbc:mysql://terraform-20180404092828620300000001.chzq8hafwwvi.us-east-1.rds.amazonaws.com:3306/telematics_firestarter_staging_1_subscription?useSSL=false"
    "properties/spring.datasource.username" = "serviceuser"
  }
}

// uat-1
resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_uat" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/_any/env-type/uat/env-id/_any/app-name/_any/"
  subkeys {
    "properties/log4j.configurationFile" = "log4j2-kafka.xml"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_uat_micro-metadata-tantalum" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/_any/env-type/uat/env-id/_any/app-name/micro-metadata-tantalum/"
  subkeys {
    "properties/log4j.configurationFile" = "log4j2.xml"
  }
}
// staging settings - NOTE: format below needs to be applied when move to production, ant
// then to other environments

resource consul_key_prefix "deployment_platform_sonic_1_telematics_fs_firestarter_svc-sonic-staging-release-1-0" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/staging/env-id/_any/app-name/svc-sonic/"
  subkeys {
    "properties/sonic.kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
    "properties/sonic.kafka.mystTopic" = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_myst"
    "properties/sonic.cassandra.outboundKeyspace" = "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_outbound"
    "properties/sonic.paperboy.topicsPublication" = "$${CLUSTER_ID}_paperboy_newsagent"
    "properties/sonic.paperboy.topicsDistribution" = "$${CLUSTER_ID}_paperboy_paper_round"
    "properties/cassandra.contactPoints" = "cassandra-2-node-0.node.consul,cassandra-2-node-1.node.consul,cassandra-2-node-2.node.consul"
  }
}

resource consul_key_prefix "deployment_platform_paperboy_1_telematics_fs_firestarter_svc-paperboy-staging-release-1-0" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/staging/env-id/_any/app-name/svc-paperboy-tantalum/"
  subkeys {
    "properties/kafka.brokers[0]" = "kafka-2-node-0.node.consul:9092"
    "properties/kafka.brokers[1]" = "kafka-2-node-1.node.consul:9092"
    "properties/kafka.brokers[2]" = "kafka-2-node-2.node.consul:9092"
  }
}
resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_staging_1_arm_tantalum" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/staging/env-id/1/app-name/micro-arm-tantalum-b2c/"
  subkeys {
    //    "properties/spring.datasource.url" = "jdbc:mysql://tf-20161129094734731889776yyp.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_staging_1_tt_arm?useSSL=false"
    //"properties/spring.datasource.url" = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_staging_1_tt_arm?useSSL=false"
    "properties/spring.datasource.url" = "jdbc:mysql://devdb.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_staging_1_tt_arm?useSSL=false"
    "properties/spring.datasource.username" = "admin"
    "properties/spring.datasource.password" = "weferwf7809tastg79g"
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
    "properties/log4j.configurationFile" = "log4j2-kafka.xml"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_shared_staging_any_riven" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/shared/env-type/staging/env-id/_any/app-name/micro-riven-tantalum/"
  subkeys {
    "properties/cassandra.sonicKeyspace" = "$${PLATFORM_ID}_firestarter_$${ENV}_$${ENV_ID}_sonic"
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
    "properties/log4j.configurationFile" = "log4j2-kafka.xml"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_shared_staging_1_administration" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/shared/env-type/staging/env-id/1/app-name/api-administration-tantalum/"
  subkeys {
    "properties/cassandra.sonicKeyspace" = "$${PLATFORM_ID}_firestarter_$${ENV}_$${ENV_ID}_sonic"
    //"properties/spring.datasource.url" = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_staging_1_tt_admin?useSSL=false"
    "properties/spring.datasource.url" = "jdbc:mysql://devdb.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_staging_1_tt_admin?useSSL=false"
    //"properties/b2c.datasource.url" = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_staging_1_tt_arm?useSSL=false"
    "properties/b2c.datasource.url" = "jdbc:mysql://devdb.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_staging_1_tt_arm?useSSL=false"
    //"properties/b2b.datasource.url" = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_armada_staging_1_tt_arm?useSSL=false"
    "properties/b2b.datasource.url" = "jdbc:mysql://devdb.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_armada_staging_1_tt_arm?useSSL=false"
    "properties/spring.datasource.username" = "admin"
    "properties/spring.datasource.password" = "weferwf7809tastg79g"
    "properties/b2c.datasource.username" = "admin"
    "properties/b2c.datasource.password" = "weferwf7809tastg79g"
    "properties/b2b.datasource.username" = "admin"
    "properties/b2b.datasource.password" = "weferwf7809tastg79g"
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
    "properties/log4j.configurationFile" = "log4j2-kafka.xml"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_shared_myst_staging" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/shared/env-type/staging/env-id/_any/app-name/svc-myst/"
  subkeys {
    "properties/log4j.configurationFile" = "log4j2-prod.xml"
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_staging_fallout" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/staging/env-id/_any/app-name/svc-fallout-tantalum/"
  subkeys {
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"

  }
}

//uat

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_firestarter_sonic_uat_1" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/uat/env-id/1/app-name/svc-sonic/"
  subkeys {
    "runtime/cpu" = "1"
    "properties/cassandra.sonicKeyspace" = "uat_b2c_sonic"
    "properties/cassandra.outboundKeyspace" = "uat_shared_outbound"
    "properties/cassandra.contactPoints" = "cassandra-2-node-0.node.consul,cassandra-2-node-1.node.consul,cassandra-2-node-2.node.consul"
  }
}
resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_firestarter_galaxy_uat_1" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/uat/env-id/1/app-name/ui-galaxy-tantalum/"
  subkeys {
    "properties/cassandra.keyspace"      = "uat_b2c_sonic"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_shared_uat_1_administration" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/shared/env-type/uat/env-id/1/app-name/api-administration-tantalum/"
  subkeys {
    "properties/spring.datasource.url" = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_uat_1_tt_admin?useSSL=false"
    "properties/b2c.datasource.url" = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_uat_1_tt_arm?useSSL=false"
    "properties/b2b.datasource.url" = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_armada_uat_1_tt_arm?useSSL=false"
    "properties/spring.datasource.username" = "admin"
    "properties/spring.datasource.password" = "v9wen485"
    "properties/b2c.datasource.username" = "admin"
    "properties/b2c.datasource.password" = "v9wen485"
    "properties/b2b.datasource.username" = "admin"
    "properties/b2b.datasource.password" = "v9wen485"
  }
}

// qa

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_firestarter_sonic_qa_develop" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/qa/env-id/develop/app-name/svc-sonic/"
  subkeys {
    "properties/kafka.mystTopic" = "telematics_uat_1_myst"
  }
}


// perf-1 settings
resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_shared_perf" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/shared/env-type/perf/env-id/_any/app-name/_any/"
  subkeys {
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
  }
}
resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_firestarer_perf" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/perf/env-id/_any/app-name/_any/"
  subkeys {
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
  }
}

resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_firestarer_svc-sonic_perf" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/perf/env-id/_any/app-name/svc-sonic/"
  subkeys {
    "runtime/cpu" = "2"
  }
}



resource consul_key_prefix "deployment_platform_micro_1_spring_ecosystems" {
  path_prefix ="configuration/deployment/templates/micro-1-spring/_settings/platform/ecosystems/project/_any/env-type/_any/env-id/_any/app-name/_any/"
  subkeys {
    "properties/spring.cloud.vault.enabled"     = "true"
  }
}

//configuration/deployment/templates/" + this.deploy_template + "/_settings/"

//actually this is set in sping-cloud stuff
/*
resource consul_key_prefix "deployment_platform_micro_1_spring_telematics_fs_perf_1_arm_tantalum" {
  path_prefix = "configuration/deployment/templates/micro-1-spring/_settings/platform/telematics/project/firestarter/env-type/perf/env-id/1/app-name/micro-arm-tantalum-b2c/"
  subkeys {
    "properties/spring.datasource.url" = "jdbc:mysql://tf-00aa6c14c801aef24fdd33e794.cwxg534lxijj.eu-west-1.rds.amazonaws.com:3306/telematics_firestarter_perf_1_tt_arm?useSSL=false"
    "properties/spring.datasource.username" = "admin"
    "properties/spring.datasource.password" = "v9wen485"
  }
}
*/
