resource consul_key_prefix "helium-1-defaults" {
  path_prefix = "configuration/deployment/templates/helium-1/_settings/_default/"
  subkeys {
    "runtime/cpu" =                                          "1"
    "runtime/memory" =                                       "2048"
    "runtime/instances" =                                    "1"
    "runtime/health_port_index" =                            "3"
    "jvm/Xmx" =                                              "900m"
    "jvm/server" =                                           ""
    "properties/log4j.configurationFile" =                   "log4j2.xml"
    "properties/com.sun.management.jmxremote" =              "true"
    "properties/com.sun.management.jmxremote.port" =         "$PORT2"
    "properties/com.sun.management.jmxremote.local.only" =   "false"
    "properties/com.sun.management.jmxremote.authenticate" = "false"
    "properties/com.sun.management.jmxremote.ssl" =          "false"

    "properties/environment" =                               "$ENV" //qa
    "properties/cluster_name" =                              "$CLUSTER_NAME" //qa-1
    "properties/cluster_id" =                                "$CLUSTER_ID" //qa_1
    "properties/platformId" =                                "$PLATFORM_ID"
    "properties/projectId" =                                 "$PROJECT_ID"
    "properties/namespace" =                                 "$ENV_ID"
    "properties/kafka.brokers" =                             "kafka-2-0.service.consul:31000"
  }
}

//platform telematics helium config

resource "consul_keys" "helium_1_template" {
  key {
    path =  "configuration/deployment/templates/helium-1/_template"
    value = "${file("files/deployment-templates/helium-1.json")}"
  }
}

resource consul_key_prefix "deployment_telematics_shared_helium_any" {
  path_prefix ="configuration/deployment/templates/helium-1/_settings/platform/telematics/project/shared/env-type/_any/env-id/_any/app-name/svc-helium/"
  subkeys {
    "properties/helium.tcp.port" =                           "$PORT0"
    "properties/helium.udp.port" =                           "$PORT1"
    "properties/helium.healthCheckHttpPort" =                "$PORT3"
  }
}

resource consul_key_prefix "deployment_telematics_shared_helium_ci" {
  path_prefix ="configuration/deployment/templates/helium-1/_settings/platform/telematics/project/shared/env-type/ci/env-id/_any/"
  subkeys {
    "properties/spring.cloud.consul.token"      =     "5a87e827-1c05-091d-2cd6-f7d075db4152"
    "properties/spring.cloud.consul.host"       =     "consul.service.consul"
    "properties/kafka.heliumTopic"               =     "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_helium"
  }
}

resource consul_key_prefix "deployment_telematics_shared_helium_uat_1" {
  path_prefix ="configuration/deployment/templates/helium-1/_settings/platform/telematics/project/shared/env-type/uat/env-id/1/"
  subkeys {
    "properties/spring.cloud.consul.token"      =     "5a87e827-1c05-091d-2cd6-f7d075db4152"
    "properties/spring.cloud.consul.host"       =     "consul.service.consul"
    "properties/kafka.heliumTopic"               =     "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_helium"
  }
}

resource consul_key_prefix "deployment_telematics_shared_helium_staging" {
  path_prefix = "configuration/deployment/templates/helium-1/_settings/platform/telematics/project/shared/env-type/staging/env-id/_any/"
  subkeys {
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
    "properties/log4j.configurationFile" = "log4j2-prod.xml"
    "properties/spring.cloud.consul.token"      =     "5a87e827-1c05-091d-2cd6-f7d075db4152"
    "properties/spring.cloud.consul.host"       =     "consul.service.consul"
    "properties/kafka.heliumTopic"               =     "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_helium"
  }
}

resource consul_key_prefix "deployment_telematics_shared_helium_qa" {
  path_prefix = "configuration/deployment/templates/helium-1/_settings/platform/telematics/project/shared/env-type/qa/env-id/_any/"
  subkeys {
    "properties/log4j.configurationFile" = "log4j2-prod.xml"
    "properties/spring.cloud.consul.token"      =     "5a87e827-1c05-091d-2cd6-f7d075db4152"
    "properties/spring.cloud.consul.host"       =     "consul.service.consul"
    "properties/kafka.heliumTopic"               =     "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_helium"
  }
}

resource consul_key_prefix "deployment_telematics_shared_helium_perf" {
  path_prefix = "configuration/deployment/templates/helium-1/_settings/platform/telematics/project/shared/env-type/perf/env-id/_any/"
  subkeys {
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
    "properties/log4j.configurationFile" = "log4j2-prod.xml"
    "properties/spring.cloud.consul.token"      =     "5a87e827-1c05-091d-2cd6-f7d075db4152"
    "properties/spring.cloud.consul.host"       =     "consul.service.consul"
    "properties/kafka.heliumTopic"               =     "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_helium"
  }
}

// Samsung - dev

resource consul_key_prefix "deployment_samsung_shared_helium_any" {
  path_prefix ="configuration/deployment/templates/helium-1/_settings/platform/samsung/project/shared/env-type/_any/env-id/_any/app-name/svc-helium/"
  subkeys {
    "properties/helium.tcp.port" =                           "$PORT0"
    "properties/helium.udp.port" =                           "$PORT1"
    "properties/helium.healthCheckHttpPort" =                "$PORT3"
  }
}

resource consul_key_prefix "deployment_samsung_shared_helium_ci" {
  path_prefix = "configuration/deployment/templates/helium-1/_settings/platform/samsung/project/shared/env-type/ci/env-id/_any/"
  subkeys {
    "properties/kafka.brokers" = "kafka-2-node-0.node.consul:9092,kafka-2-node-1.node.consul:9092,kafka-2-node-2.node.consul:9092"
    "properties/log4j.configurationFile" = "log4j2-prod.xml"
    "properties/spring.cloud.consul.token"      =     "5a87e827-1c05-091d-2cd6-f7d075db4152"
    "properties/spring.cloud.consul.host"       =     "consul.service.consul"
    "properties/kafka.heliumTopic"               =     "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_helium"
  }
}

// FS PROD

resource consul_key_prefix "deployment_telematics_firestarter_helium_1_prod" {
  path_prefix ="configuration/deployment/templates/helium-1/_settings/platform/telematics/project/shared/env-type/prod/env-id/_any/"
  subkeys {
    "properties/spring.cloud.consul.token"      = "259be361-f20a-bb9d-3955-8da16555185a"
    "properties/kafka.brokers"                  = "kafka-2-node-0.node.consul:31000,kafka-2-node-1.node.consul:31000,kafka-2-node-2.node.consul:31000"
    "properties/cassandra.contactPoints.0"        = "cassandra-1-node-0.node.consul"
    "properties/spring.cloud.consul.host" = "consul.service.consul"
    "properties/kafka.heliumTopic"               =     "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_helium"
    "app-name/svc-argon/properties/argon.tcp.port" =                           "$PORT0"
    "app-name/svc-argon/properties/argon.udp.port" =                           "$PORT1"
    "app-name/svc-argon/properties/argon.healthCheckHttpPort" =                "$PORT3"
    "app-name/svc-argon/properties/s3_access" =                                "AKIAJLVJVNYU3K33GKXQ"
    "app-name/svc-argon/properties/s3_secret" =                                "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"
  }
}

// ARGON settings

resource consul_key_prefix "deployment_telematics_shared_argon" {
  path_prefix ="configuration/deployment/templates/helium-1/_settings/platform/telematics/project/shared/env-type/_any/env-id/_any/app-name/svc-argon/"
  subkeys {
    "properties/argon.tcp.port" =                           "$PORT0"
    "properties/argon.udp.port" =                           "$PORT1"
    "properties/argon.healthCheckHttpPort" =                "$PORT3"
    "properties/s3_access" =                                "AKIAJN4WOXTH7QQZIXPA"
    "properties/s3_secret" =                                "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"
  }
}


resource consul_key_prefix "deployment_telematics_shared_neon" {
  path_prefix ="configuration/deployment/templates/helium-1/_settings/platform/telematics/project/shared/env-type/_any/env-id/_any/app-name/svc-neon/"
  subkeys {
    "properties/neon.tcp.port" =                           "$PORT0"
    "properties/neon.udp.port" =                           "$PORT1"
    "properties/neon.healthCheckHttpPort" =                "$PORT3"
  }
}

resource consul_key_prefix "deployment_telematics_shared_diproton" {
  path_prefix ="configuration/deployment/templates/helium-1/_settings/platform/telematics/project/shared/env-type/_any/env-id/_any/app-name/svc-diproton/"
  subkeys {
    "properties/diproton.tcp.port" =                           "$PORT0"
    "properties/diproton.udp.port" =                           "$PORT1"
    "properties/management.port" =                             "$PORT3"
    "properties/kafka.heliumTopic" =                           "$${PLATFORM_ID}_$${ENV}_$${ENV_ID}_diproton"
  }
}
