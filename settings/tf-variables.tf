variable "tf_path" {
  default = "configuration/terraform/"
}

resource consul_key_prefix tf_variables {
  path_prefix = "${var.tf_path}"

  subkeys {
    region                      = "us-east-1"
    accountid                   = "804191495435"
    root_partition_size         = " 20"
    key_name                    = "haralds_pub"
    key_public                  = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDVCf3nwdglbtkk+32Z7aEuS6yZE3bUBVMJPnGkFggQRyEeK/ddvtyFUVPkEGOh5UFdhoHEW6PaDPKn0FuZwFi5xCo6chFmA8/EA4q5vDbCTUA5kze9f+ZreaGuCnDwIEWgrdKJpQUMcSjurQnU2cWBdC5qU9a/3UqvGhJwpINeU9nXklgbfZ9mW/iMkBgGGFz8hJTdLmkQjbeEcFsjlZgJnUf/aS1hPLwdLxIbfOwGmmrbLqh8SNM9NrkcNsjwMVH3piKELhi75x9X/ViHaMt5/04LpzwgzrgK3XWc+LLorWfQXTuyYGUh9pMI/pLEOrX8r4dSW2y6jZCUQftp9Xon"
    domain                      = "consul"
    s3bucket                    = "xb3874568yxjnbakwpiergcpabperruyt0q26bvwy45"
    "vpc_prefixes/mgmt"         = "10.1.0.0/16"
    "vpc_prefixes/prod"         = "10.2.0.0/16"
    "vpc_prefixes/dev"          = "10.3.0.0/16"
    "vpc_prefixes/staging"      = "10.4.0.0/16"
    "default_amis/consul"       = "ami-0e61f37d"
    "default_amis/jumpbox"      = "ami-0e61f37d"
    "default_amis/windows"      = "ami-916af8e2"
    "default_amis/mercury"      = "ami-f73b5c84"
    "default_amis/pulse"        = "ami-f73b5c84"
    "default_amis/mssql_server" = "ami-f568fa86"
    "default_amis/etl"          = "ami-f73b5c84"
    "default_amis/mesos"        = "ami-998915ea"
    "default_amis/linux"        = "ami-0e61f37d"
    "default_amis/test_linux"   = "ami-27930e54"
    "public_dns/domain"         = "ttmcorp.com"
    "public_dns/zone_id"        = "Z1W5KWX4QKM5JV"
  }
}
