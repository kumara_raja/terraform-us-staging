resource "consul_keys" "docker_1_template" {
  key {
    path  = "configuration/deployment/templates/docker-1/_template"
    value = "${file("files/deployment-templates/docker-1.json")}"
  }
}

resource consul_key_prefix "docker-1-defaults" {
  path_prefix = "configuration/deployment/templates/docker-1/_settings/_default/"

  subkeys {
    "runtime/cpu"                  = "0.2"
    "runtime/memory"               = "1500"
    "runtime/instances"            = "1"
    "runtime/health_port_index"    = "2"
    "runtime/grace_period_seconds" = "60"
    "properties/logPrefix"         = "$MESOS_TASK_ID"
    "properties/environment"       = "$ENV"           //qa
    "properties/cluster_name"      = "$CLUSTER_NAME"  //qa-1
    "properties/cluster_id"        = "$CLUSTER_ID"    //qa_1
    "properties/platformId"        = "$PLATFORM_ID"
    "properties/projectId"         = "$PROJECT_ID"
    "properties/namespace"         = "$ENV_ID"
  }
}
