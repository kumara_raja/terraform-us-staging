#!/bin/bash
cp ../ansible.cfg ~/.ansible.cfg 
ANSIBLE_DIR="$PWD"
TERRAFORM_DIR=../prod/
chmod -R 777 ./terraform  
cd $TERRAFORM_DIR
TFSTATE=$(terraform state pull)
cd $ANSIBLE_DIR
echo $TFSTATE > terraform.tfstate 
terraform-inventory -list > host_list

