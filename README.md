# Terraform US Prod

## Requirements

* [terraform](https://www.terraform.io/)
* [terraform-inventory](https://github.com/adammck/terraform-inventory)
* [ansible](https://www.ansible.com/)
* [ansible-galaxy](https://galaxy.ansible.com/home)

## Setup

* Create `providers.tf` file in `prod`, copy the existing `providers.tf.sample` file and replace Amazon credentials with the token and secret for both the US prod account, and EU dev account. This is to automate the peering process and route table setup between regions.
* Ensure `_remote.tf` is setup as expected.
* Ensure `variables.tf` is setup as expected.

## Provisioning

Run `terraform plan` to see what infrastructure changes are due to be made.

```
terraform plan prod
```

Once satisfied with the changes to be made, run `terraform apply` to apply the changes.

```
terraform apply prod
```

## Configuration

Static EC2 instances will be provisioned via Ansible playbooks. In the root directory there is a `bin` folder with a series of bash scripts which sync the remote state, and use `terraform-inventory` to make the terraform state available as an ansible inventory file.

Once the terraform provisioning is successful, you should be able to run each of these scripts in any order, with the exception of the zookeeper scripts which need to be run first.

## Gotchas

* You will need to comment out `consul.tf` and `fleet.tf` as these have some dependencies.
