variable "axiappapi_target_ip" {
  type = "list"
   default = ["172.18.3.56", "172.18.8.251"]
}
variable "axiappapi_name" {
   default = "axiappapi"
}
variable "axiappapi_certificate_arn" {
  default = "arn:aws:acm:us-east-1:804191495435:certificate/11f5e3e7-d604-4753-a97c-ee30e6763529"
}

variable "axiappapi_lb_ssl_policy" {
  default = "ELBSecurityPolicy-2015-05"
}
variable "axiappapi_route53_record_name" {
  default = "axiappapi.spark-telematics.us"
}

variable "axiappapi_ingress_cidr_blocks" {
  type = "list"
  default = ["10.12.0.0/16"]
}
variable "axiappapi_egress_cidr_blocks" {
  type = "list"
  default = ["172.18.3.56/32", "172.18.8.251/32"]
  }
variable "axiappapi_path_pattern" {
  default = "/axiappapi/*" }

variable "axiappapi_zone_id" {
  default = "Z3OGB3Y98LGQHS"
}



module "axiappapi_proxy" {
  source             = "../modules/alb_proxy"
  availability_zones = "${var.availability_zones}"
  zone_id            = "${var.axiappapi_zone_id}"
  vpc_id             = "${aws_vpc.vpc.*.id[2]}"
  subnets            = ["${module.peer_subnet.private}"]
  target_ip          = "${var.axiappapi_target_ip}"
  name               = "${var.axiappapi_name}"
  certificate_arn    = "${var.axiappapi_certificate_arn}"
  lb_ssl_policy      = "${var.axiappapi_lb_ssl_policy}"
  route53_record_name= "${var.axiappapi_route53_record_name}"
  ingress_cidr_blocks= "${var.axiappapi_ingress_cidr_blocks}"
  egress_cidr_blocks = "${var.axiappapi_egress_cidr_blocks}"
  path_pattern       = "${var.axiappapi_path_pattern}"
}