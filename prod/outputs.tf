output vpc_mgmt {
  value = "${aws_vpc.vpc.*.id[0]}"
}

output vpc_mgmt_pub_subnet {
  value = "${module.mgmt_subnet.public}"
}

output vpc_mgmt_priv_subnet {
  value = "${module.mgmt_subnet.private}"
}

output vpc_svc {
  value = "${aws_vpc.vpc.*.id[1]}"
}

output vpc_svc_pub_subnet {
  value = "${module.svc_subnet.public}"
}

output vpc_svc_priv_subnet {
  value = "${module.svc_subnet.private}"
}

output vpc_peer {
  value = "${aws_vpc.vpc.*.id[2]}"
}

output vpc_peer_pub_subnet {
  value = "${module.peer_subnet.public}"
}

output vpc_peer_priv_subnet {
  value = "${module.peer_subnet.private}"
}

output vpc_ngfw {
  value = "${aws_vpc.vpc.*.id[3]}"
}

output consul_client_mgmt_sg {
  value = "${module.consul.consul_sg}"
}

output consul_ips {
  value = "${module.consul.consul_ips}"
}





