//public LB SVC


module "lb-2" {
  source    = "../modules/mesos-agent"
  ami       = "ami-5c66ea23"
  instance_type = "t2.medium"
  env       = "prod-us"
  net = "svc"
  security_groups = ["${module.security.lb-2_sg}"]
  key = "${aws_key_pair.public_key.key_name}"
  role = "mesos-agent"
  size = "3"
  subnet_groups = "${module.svc_subnet.public}"
  vpc      = "${aws_vpc.vpc.*.id[1]}"
  mesos_role = "*"
  disk_type = "gp2"
  disk_size = "100"
  cluster = "lb-2"
  app = "lb-2"
  type = "lb-2"
  ports = "[\"443-443\",\"80-80\"]"

  associate_public_ip  = "true"
}




resource "aws_route53_record" "lb-2_external" {
  name    = "lb-2.spark-telematics.us"
  type    = "A"
  records = ["${module.lb-2.public_ips}"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}

resource "aws_route53_record" "lb-2_internal" {
  name    = "lb-2-internal.spark-telematics.us"
  type    = "A"
  records = ["${module.lb-2.private_ips}"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}

data template_file "lb-2_nginx_config" {
  template = "${file("../files/nginx.conf.ctmpl")}"
  vars {
    cluster = "lb-2"
  }
}

resource consul_keys "lb-2_template" {
  key {
    path   = "configuration/loadbalancer/lb-2/template"
    value  = "${data.template_file.lb-2_nginx_config.rendered}"
    delete = true
  }
}






module api_router_prod_1_vhost_public {
  source               = "../modules/nginx-vhost/https-http"
  lb_cluster           = "lb-2"
  lb_dns_name          = "lb-2.spark-telematics.us"
  vhosts               = ["api-route-prod.spark-telematics.us"]
  vhost_entry_name     = "api-route-prod-us-public"
  upstream_alias       = "api-route-prod-us-public"
  dns_zone             = "Z3OGB3Y98LGQHS"
  service              = "telematics-firestarter-prod-1.api-router-tantalum-b2c"
  force_to_ssl         = "true"
  cert                 = "star_spark_telematics_us"
  ca                   = "digicert2018"
  server_key           = "star_spark_telematics_us"
  client_ca            = "tantalum"
  client_certs_enabled = "false"
}



module api_router_prod_1_vhost_internal {
  source               = "../modules/nginx-vhost/https-http"
  lb_cluster           = "lb-2"
  lb_dns_name          = "lb-2-internal.spark-telematics.us"
  vhosts               = ["api-route-prod-internal.spark-telematics.us"]
  vhost_entry_name     = "api-route-prod-us-internal"
  upstream_alias       = "api-route-prod-us-internal"
  dns_zone             = "Z3OGB3Y98LGQHS"
  service              = "telematics-firestarter-prod-1.api-router-tantalum-b2c"
  force_to_ssl         = "true"
  cert                 = "star_spark_telematics_us"
  ca                   = "digicert2018"
  server_key           = "star_spark_telematics_us"
  client_ca            = "tantalum"
  client_certs_enabled = "false"
}



//firebase-iq in dev
module api_authentication_prod_1_vhost_public {
  source           = "../modules/nginx-vhost/https-http"
  lb_cluster       = "lb-2"
  lb_dns_name      = "lb-2.spark-telematics.us"
  vhosts           = ["authentication.spark-telematics.us"]
  service          = "telematics-firestarter-prod-1.api-authentication-tantalum"
  vhost_entry_name = "api-authentication-tantalum-public"
  upstream_alias   = "api-authentication-tantalum-public"
  dns_zone             = "Z3OGB3Y98LGQHS"
  force_to_ssl         = "true"
  cert                 = "star_spark_telematics_us"
  ca                   = "digicert2018"
  server_key           = "star_spark_telematics_us"
  client_ca            = "tantalum"
  client_certs_enabled = "false"
}