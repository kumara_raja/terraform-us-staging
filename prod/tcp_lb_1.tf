
variable "size" {
  type = "string"
  default = "3"
}


module "tcp-lb-1" {
  source    = "../modules/mesos-agent"
  ami       = "ami-5c66ea23"
  instance_type = "t2.medium"
  env       = "prod-us"
  net = "svc"
  security_groups = ["${module.security.tcp-lb-1_sg}"]
  key = "${aws_key_pair.public_key.key_name}"
  role = "mesos-agent"
  size = "${var.size}"
  subnet_groups = "${module.svc_subnet.private}"
  vpc      = "${aws_vpc.vpc.*.id[1]}"
  mesos_role = "*"
  disk_type = "gp2"
  disk_size = "100"
  cluster = "tcp-lb-1"
  app = "tcp-lb-1"
  type = "tcp-lb-1"
  ports = "[\"443-443\",\"80-80\",\"5654-5654\",\"4305-4305\",\"5443-5443\",\"9003-9003\",\"9004-9004\",\"9145-9145\"]"
  associate_public_ip  = "false"
}



resource "aws_route53_record" "tcp-lb-1_internal" {
  name    = "tcp-lb-1-internal.spark-telematics.us"
  type    = "A"
  records = ["${module.tcp-lb-1.private_ips}"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}


resource "aws_route53_record" "helium_internal" {
  name    = "helium-internal.spark-telematics.us"
  type    = "CNAME"
  records = ["tcp-lb-1-internal.spark-telematics.us"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}


resource "consul_key_prefix" "vhost_keys_tcp_telematics-shared-prod-1" {
  path_prefix = "configuration/loadbalancer/tcp-lb-1/virtualhosts/telematics-shared-prod-1-helium-tcp/"
  subkeys {
    "alias"            = <<EOF
["helium-internal.spark-telematics.us"]
EOF
    "backend"           = "telematics-shared-prod-1.svc-helium"
    "backend-alias"     = "telematics-shared-prod-1-helium-tcp"
    "ssl-enabled"       = "false"
    "force-to-ssl"      = "false"
    "tcp-vhost"         = "true"
    "udp-vhost"         = "false"
    "tcp-port"          = "5654"
    "upstream-ssl"      = "false"
  }
}





data template_file "tcp-lb-1_nginx_config" {
  template = "${file("../files/tcp-nginx.conf.ctmpl")}"
  vars {
    cluster = "tcp-lb-1"
  }
}

resource consul_keys "tcp-lb-1_template" {
  key {
    path   = "configuration/loadbalancer/tcp-lb-1/template"
    value  = "${data.template_file.tcp-lb-1_nginx_config.rendered}"
    delete = true
  }
}


