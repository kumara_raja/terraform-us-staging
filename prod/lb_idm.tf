//Public LB SVC for IDM

module "lb-idm" {
  source    = "../modules/mesos-agent"
  ami       = "ami-5c66ea23"
  instance_type = "t2.medium"
  env       = "prod-us"
  net = "svc"
  security_groups = ["${module.security.lb-idm_sg}"]
  key = "${aws_key_pair.public_key.key_name}"
  role = "mesos-agent"
  size = "3"
  subnet_groups = "${module.svc_subnet.public}"
  vpc      = "${aws_vpc.vpc.*.id[0]}"
  mesos_role = "*"
  disk_type = "gp2"
  disk_size = "100"
  cluster = "lb-idm"
  app = "lb-idm"
  ports = "[\"443-443\",\"80-80\"]"
  type = "lb-idm"

  associate_public_ip  = "true"
}




resource "aws_route53_record" "lb_external" {
  name    = "lb-idm-prod-us.spark-telematics.us"
  type    = "A"
  records = ["${module.lb-idm.public_ips}"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}

resource "aws_route53_record" "lb_internal" {
  name    = "lb-idm-prod-us-internal.spark-telematics.us"
  type    = "A"
  records = ["${module.lb-idm.private_ips}"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}

data template_file "nginx_config" {
  template = "${file("../files/lb-idm-nginx.conf.ctmpl")}"
  vars {
    cluster = "lb-idm"
  }
}

resource consul_keys "template" {
  key {
    path   = "configuration/loadbalancer/lb-idm/template"
    value  = "${data.template_file.nginx_config.rendered}"
    delete = true
  }
}




module api-consent-tantalum-idm-consent-prod_vhost_public {
  source           = "../modules/nginx-vhost/https-http"
  lb_cluster       = "lb-idm"
  cert             = "star_spark_telematics_us"
  ca               = "digicert2018"
  server_key       = "star_spark_telematics_us"
  lb_dns_name      = "lb-idm-prod-us.spark-telematics.us"
  vhosts           = ["api-consent-tantalum-idm-consent-prod.spark-telematics.us"]
  service          = "idm-consent-prod-1.api-consent-tantalum"
  vhost_entry_name = "api-consent-tantalum-idm-consent-prod-1-public"
  upstream_alias   = "api-consent-tantalum-idm-consent-prod-1-public"
  dns_zone          = "Z3OGB3Y98LGQHS"

  force_to_ssl     = "true"
  client_ca = "tantalum"
  ssl_enabled      = "true"
  client_certs_enabled = "false"
}



module api-consent-tantalum-idm-consent-prod_vhost_internal {
  source           = "../modules/nginx-vhost/https-http"
  lb_cluster       = "lb-idm"
  cert             = "star_spark_telematics_us"
  ca               = "digicert2018"
  server_key       = "star_spark_telematics_us"
  lb_dns_name      = "lb-idm-prod-us-internal.spark-telematics.us"
  vhosts           = ["api-consent-tantalum-idm-consent-prod-internal.spark-telematics.us"]
  service          = "idm-consent-prod-1.api-consent-tantalum"
  vhost_entry_name = "api-consent-tantalum-idm-consent-prod-1-internal"
  upstream_alias   = "api-consent-tantalum-idm-consent-prod-1-internal"
  dns_zone          = "Z3OGB3Y98LGQHS"

  force_to_ssl     = "true"
  client_ca = "tantalum"
  ssl_enabled      = "true"
  client_certs_enabled = "false"
}





module app-oauth-att-prod-1-idm-dev-services_vhost_apigee {
  source           = "../modules/nginx-vhost/https-http"
  lb_cluster       = "lb-idm"
  cert             = "star_spark_telematics_us"
  ca               = "digicert2018"
  server_key       = "star_spark_telematics_us"
  lb_dns_name      = "lb-idm-prod-us.spark-telematics.us"
  vhosts           = ["app-oauth-att-idm-prod.spark-telematics.us"]
  service          = "app-oauth-att-prod-1-idm-svc-services"
  vhost_entry_name = "app-oauth-att-prod-1-idm-svc-services-apigee"
  upstream_alias   = "app-oauth-att-prod-1-idm-svc-services-apigee"
  dns_zone          = "Z3OGB3Y98LGQHS"
  force_to_ssl     = "true"
  client_ca         = "tantalum"
  client_certs_enabled = "false"
}



module app-oauth-att-prod-1-idm-dev-services_vhost_internal {
  source           = "../modules/nginx-vhost/https-http"
  lb_cluster       = "lb-idm"
  cert             = "star_spark_telematics_us"
  ca               = "digicert2018"
  server_key       = "star_spark_telematics_us"
  lb_dns_name      = "lb-idm-prod-us-internal.spark-telematics.us"
  vhosts           = ["app-oauth-att-idm-prod-internal.spark-telematics.us"]
  service          = "app-oauth-att-prod-1-idm-svc-services"
  vhost_entry_name = "app-oauth-att-prod-1-idm-svc-services-internal"
  upstream_alias   = "app-oauth-att-prod-1-idm-svc-services-internal"
  dns_zone          = "Z3OGB3Y98LGQHS"
  force_to_ssl     = "true"
  client_ca         = "tantalum"
  client_certs_enabled = "false"
}










