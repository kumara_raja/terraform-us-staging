//private LB


module "lb-1" {
  source    = "../modules/mesos-agent"
  ami       = "ami-5c66ea23"
  instance_type = "t2.medium"
  env       = "prod-us"
  net = "mgmt"
  security_groups = ["${module.security.lb-1_sg}"]
  key = "${aws_key_pair.public_key.key_name}"
  role = "mesos-agent"
  size = "2"
  subnet_groups = "${module.mgmt_subnet.private}"
  vpc      = "${aws_vpc.vpc.*.id[0]}"
  mesos_role = "*"
  disk_type = "gp2"
  disk_size = "100"
  cluster = "lb-1"
  app = "lb-1"
  type = "lb-1"
  ports = "[\"443-443\",\"80-80\",\"3128-3128\"]"

  associate_public_ip  = "false"
}




resource "aws_route53_record" "lb-1_internal" {
  name    = "lb-1-internal.spark-telematics.us"
  type    = "A"
  records = ["${module.lb-1.private_ips}"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}

data template_file "lb-1_nginx_config" {
  template = "${file("../files/nginx.conf.ctmpl")}"
  vars {
    cluster = "lb-1"
  }
}

resource consul_keys "lb-1_template" {
  key {
    path   = "configuration/loadbalancer/lb-1/template"
    value  = "${data.template_file.lb-1_nginx_config.rendered}"
    delete = true
  }
}






module jimmy_test_prod_1_vhost_internal {
  source               = "../modules/nginx-vhost/https-http"
  lb_cluster           = "lb-1"
  lb_dns_name          = "lb-1-internal.spark-telematics.us"
  vhosts               = ["jimmy.spark-telematics.us"]
  vhost_entry_name     = "jimmy-cert-test-internal"
  upstream_alias       = "jimmy-cert-test-internal"
  dns_zone             = "Z3OGB3Y98LGQHS"
  service              = "helloworld"
  force_to_ssl         = "true"
  cert                 = "star_spark_telematics_us"
  server_key           = "star_spark_telematics_us"
  ca                   = "digicert2018"
  client_ca            = "digicertCA"
  client_certs_enabled = "true"
}
