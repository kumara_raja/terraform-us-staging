data "aws_iam_policy_document" "spot-fleet-peer-instance-assume-role-policy-peer" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["spotfleet.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "spot-fleet-peer-tagging-role" {
  assume_role_policy = "${data.aws_iam_policy_document.spot-fleet-peer-instance-assume-role-policy-peer.json}"
  name               = "spot-fleet-peer-tagging-role"
}

resource "aws_iam_role_policy_attachment" "spot_request_policy_peer" {
  role       = "${aws_iam_role.spot-fleet-peer-tagging-role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole"
}

resource "aws_iam_instance_profile" "spot-fleet-peer" {

  name = "spot-fleet-peer"
  role = "${aws_iam_role.spot-fleet-peer.name}"

}


resource "aws_iam_role" "spot-fleet-peer" {
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}


resource "aws_iam_role_policy" "s3" {
  name   = "vault-s3"
  role   = "${aws_iam_role.spot-fleet-peer.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:Get*",
        "s3:List*"
      ],
      "Resource": "arn:aws:s3:::ignite-receiver-prod/*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket"
      ],
      "Resource": "arn:aws:s3:::ignite-receiver-prod"
    }
  ]
}
EOF
}



data "template_file" "mesos_slave_peer" {
  template = "${file("../templates/mesos-slave-peer.tpl")}"
  vars {
    NET = "svc-peering"
    PORTS = "[30000-34000]"
    ENV = "prod"
    ROLE = "*"
    TYPE = "shared"

  }
}

resource "aws_spot_fleet_request" "spot_request_peer" {
  iam_fleet_role                      = "${aws_iam_role.spot-fleet-peer-tagging-role.arn}"
  allocation_strategy                 = "diversified"
  spot_price                          = "0.05"
  target_capacity                     = 25
  replace_unhealthy_instances         = true
  terminate_instances_with_expiration = false
  valid_until                         = "2022-12-31T23:59:59Z"

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.large"
    weighted_capacity      = 2
    subnet_id              = "${module.peer_subnet.private[0]}"
    vpc_security_group_ids = ["${module.security.peer_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_peer.rendered}"
    iam_instance_profile_arn  = "${aws_iam_instance_profile.spot-fleet-peer.arn}"

    tags = {
      Name      = "spot-fleet-peer"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.large"
    weighted_capacity      = 2
    subnet_id              = "${module.peer_subnet.private[1]}"
    vpc_security_group_ids = ["${module.security.peer_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_peer.rendered}"
    iam_instance_profile_arn  = "${aws_iam_instance_profile.spot-fleet-peer.arn}"


    tags = {
      Name      = "spot-fleet-peer"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.large"
    weighted_capacity      = 2
    subnet_id              = "${module.peer_subnet.private[2]}"
    vpc_security_group_ids = ["${module.security.peer_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_peer.rendered}"
    iam_instance_profile_arn  = "${aws_iam_instance_profile.spot-fleet-peer.arn}"


    tags = {
      Name      = "spot-fleet-peer"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.xlarge"
    weighted_capacity      = 4
    subnet_id              = "${module.peer_subnet.private[0]}"
    vpc_security_group_ids = ["${module.security.peer_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_peer.rendered}"
    iam_instance_profile_arn  = "${aws_iam_instance_profile.spot-fleet-peer.arn}"


    tags = {
      Name      = "spot-fleet-peer"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.xlarge"
    weighted_capacity      = 4
    subnet_id              = "${module.peer_subnet.private[1]}"
    vpc_security_group_ids = ["${module.security.peer_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_peer.rendered}"
    iam_instance_profile_arn  = "${aws_iam_instance_profile.spot-fleet-peer.arn}"


    tags = {
      Name      = "spot-fleet-peer"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.xlarge"
    weighted_capacity      = 4
    subnet_id              = "${module.peer_subnet.private[2]}"
    vpc_security_group_ids = ["${module.security.peer_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_peer.rendered}"
    iam_instance_profile_arn  = "${aws_iam_instance_profile.spot-fleet-peer.arn}"


    tags = {
      Name      = "spot-fleet-peer"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.2xlarge"
    weighted_capacity      = 8
    subnet_id              = "${module.peer_subnet.private[0]}"
    vpc_security_group_ids = ["${module.security.peer_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_peer.rendered}"
    iam_instance_profile_arn  = "${aws_iam_instance_profile.spot-fleet-peer.arn}"


    tags = {
      Name      = "spot-fleet-peer"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.2xlarge"
    weighted_capacity      = 8
    subnet_id              = "${module.peer_subnet.private[1]}"
    vpc_security_group_ids = ["${module.security.peer_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_peer.rendered}"
    iam_instance_profile_arn  = "${aws_iam_instance_profile.spot-fleet-peer.arn}"


    tags = {
      Name      = "spot-fleet-peer"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.2xlarge"
    weighted_capacity      = 8
    subnet_id              = "${module.peer_subnet.private[2]}"
    vpc_security_group_ids = ["${module.security.peer_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_peer.rendered}"
    iam_instance_profile_arn  = "${aws_iam_instance_profile.spot-fleet-peer.arn}"


    tags = {
      Name      = "spot-fleet-peer"
      SpotFleet = true
    }
  }


  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.4xlarge"
    weighted_capacity      = 16
    subnet_id              = "${module.peer_subnet.private[0]}"
    vpc_security_group_ids = ["${module.security.peer_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_peer.rendered}"
    iam_instance_profile_arn  = "${aws_iam_instance_profile.spot-fleet-peer.arn}"


    tags = {
      Name      = "spot-fleet-peer"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.10xlarge"
    weighted_capacity      = 16
    subnet_id              = "${module.peer_subnet.private[1]}"
    vpc_security_group_ids = ["${module.security.peer_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_peer.rendered}"
    iam_instance_profile_arn  = "${aws_iam_instance_profile.spot-fleet-peer.arn}"


    tags = {
      Name      = "spot-fleet-peer"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.16xlarge"
    weighted_capacity      = 8 
    subnet_id              = "${module.peer_subnet.private[2]}"
    vpc_security_group_ids = ["${module.security.peer_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_peer.rendered}"
    iam_instance_profile_arn  = "${aws_iam_instance_profile.spot-fleet-peer.arn}"


    tags = {
      Name      = "spot-fleet-peer"
      SpotFleet = true
    }
  }
}
