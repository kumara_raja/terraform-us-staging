terraform {
  backend "s3" {
    bucket         = "tantalum-terraform-state-prod-us-east-1"
    key            = "prod/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "tantalum-terraform-state-lock-prod-us-east-1"
  }
}
