



module "lb-peer" {
  source    = "../modules/mesos-agent"
  ami       = "ami-5c66ea23"
  instance_type = "t2.medium"
  env       = "prod-us"
  net = "svc-peering"
  security_groups = ["${module.security.lb-peer_sg}"]
  key = "${aws_key_pair.public_key.key_name}"
  role = "mesos-agent"
  size = "3"
  subnet_groups = "${module.peer_subnet.private}"
  vpc      = "${aws_vpc.vpc.*.id[2]}"
  mesos_role = "*"
  disk_type = "gp2"
  disk_size = "100"
  cluster = "lb-peer"
  app = "lb-peer"
  type = "lb-peer"
  ports = "[\"443-443\",\"80-80\"]"
}



resource "aws_route53_record" "lb-peer_internal" {
  name    = "lb-peer-internal.spark-telematics.us"
  type    = "A"
  records = ["${module.lb-peer.private_ips}"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}

data template_file "lb-peer_nginx_config" {
  template = "${file("../files/nginx.conf.ctmpl")}"
  vars {
    cluster = "lb-peer"
  }
}

resource consul_keys "lb-peer_template" {
  key {
    path   = "configuration/loadbalancer/lb-peer/template"
    value  = "${data.template_file.lb-peer_nginx_config.rendered}"
    delete = true
  }
}






module svc_harman_gateway_prod_1_vhost_certs {
  source               = "../modules/nginx-vhost/https-http"
  lb_cluster           = "lb-peer"
  lb_dns_name          = "lb-peer-internal.spark-telematics.us"
  vhosts               = ["harman-gateway.spark-telematics.us"]
  vhost_entry_name     = "api-route-prod-us-certs"
  upstream_alias       = "api-route-prod-us-certs"
  dns_zone             = "Z3OGB3Y98LGQHS"
  service              = "telematics-firestarter-prod-1.svc-harman-gateway"
  force_to_ssl         = "true"
  cert                 = "star_spark_telematics_us"
  ca                   = "digicert2018"
  server_key           = "star_spark_telematics_us"
  client_ca            = "harman"
  client_certs_enabled = "false"
}




module svc_harman_gateway_prod_1_vhost_internal {
  source               = "../modules/nginx-vhost/https-http"
  lb_cluster           = "lb-peer"
  lb_dns_name          = "lb-peer-internal.spark-telematics.us"
  vhosts               = ["harman-gateway-internal.spark-telematics.us"]
  vhost_entry_name     = "api-route-prod-us-internal"
  upstream_alias       = "api-route-prod-us-internal"
  dns_zone             = "Z3OGB3Y98LGQHS"
  service              = "telematics-firestarter-prod-1.svc-harman-gateway"
  force_to_ssl         = "true"
  cert                 = "star_spark_telematics_us"
  ca                   = "digicert2018"
  server_key           = "star_spark_telematics_us"
  client_ca            = "tantalum"
  client_certs_enabled = "false"
}


module api-mediation-harman_prod_1_vhost_internal {
  source               = "../modules/nginx-vhost/https-http"
  lb_cluster           = "lb-peer"
  lb_dns_name          = "lb-peer-internal.spark-telematics.us"
  vhosts               = ["api-mediation-harman-internal.spark-telematics.us"]
  vhost_entry_name     = "api-mediation-harman-prod-us-internal"
  upstream_alias       = "api-mediation-harman-prod-us-internal"
  dns_zone             = "Z3OGB3Y98LGQHS"
  service              = "telematics-firestarter-prod-1.api-mediation-harman"
  force_to_ssl         = "true"
  cert                 = "star_spark_telematics_us"
  ca                   = "digicert2018"
  server_key           = "star_spark_telematics_us"
  client_ca            = "tantalum"
  client_certs_enabled = "false"
}

