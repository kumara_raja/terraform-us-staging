terraform {
  required_version = "0.11.7"
}
resource "aws_vpc" "vpc" {
  count                = "${length(var.vpcs)}"
  cidr_block           = "${var.vpc_cidr[var.vpcs[count.index]]}"
  enable_dns_hostnames = true

  tags {
    Name = "${var.env}-${var.vpcs[count.index]}"
  }
}

//
// DHCP Option Sets
//

resource "aws_vpc_dhcp_options" "dhcp_options" {
  domain_name         = "consul service.consul node.consul"
  domain_name_servers = ["${module.consul.consul_ips}"]
}

resource aws_vpc_dhcp_options_association "dhcp_options_association" {
  count           = "${length(var.vpcs)}"
  dhcp_options_id = "${aws_vpc_dhcp_options.dhcp_options.id}"
  vpc_id          = "${aws_vpc.vpc.*.id[count.index]}"
}

//
// Subnets
//

module "mgmt_subnet" {
  source             = "../modules/subnet"
  vpc_id             = "${aws_vpc.vpc.*.id[0]}"
  cidr_block         = "${aws_vpc.vpc.*.cidr_block[0]}"
  vpc_name           = "${lookup(aws_vpc.vpc.*.tags[0], "Name")}"
  availability_zones = "${var.availability_zones}"
}

module "svc_subnet" {
  source             = "../modules/subnet"
  vpc_id             = "${aws_vpc.vpc.*.id[1]}"
  cidr_block         = "${aws_vpc.vpc.*.cidr_block[1]}"
  vpc_name           = "${lookup(aws_vpc.vpc.*.tags[1], "Name")}"
  availability_zones = "${var.availability_zones}"
}

module "peer_subnet" {
  source             = "../modules/subnet"
  vpc_id             = "${aws_vpc.vpc.*.id[2]}"
  cidr_block         = "${aws_vpc.vpc.*.cidr_block[2]}"
  vpc_name           = "${lookup(aws_vpc.vpc.*.tags[2], "Name")}"
  availability_zones = "${var.availability_zones}"
}

//
// NAT Gateways
//

resource aws_eip "nat_eip" {
  count = 3
  vpc   = true
}

resource aws_nat_gateway "mgmt_gw" {
  allocation_id = "${aws_eip.nat_eip.*.id[0]}"
  subnet_id     = "${module.mgmt_subnet.public[0]}"

  tags {
    Name = "${var.env}-mgmt"
  }
}

resource aws_nat_gateway "svc_gw" {
  allocation_id = "${aws_eip.nat_eip.*.id[1]}"
  subnet_id     = "${module.svc_subnet.public[0]}"

  tags {
    Name = "${var.env}-svc"
  }
}

resource aws_nat_gateway "peer_gw" {
  allocation_id = "${aws_eip.nat_eip.*.id[2]}"
  subnet_id     = "${module.peer_subnet.public[0]}"

  tags {
    Name = "${var.env}-peer"
  }
}


//
// Internet Gateway
//

resource "aws_internet_gateway" "igw" {
  count  = "${length(var.vpcs)}"
  vpc_id = "${aws_vpc.vpc.*.id[count.index]}"

  tags {
    Name = "${var.env}-${var.vpcs[count.index]}"
  }
}

//
// Peering Connections
//

resource aws_vpc_peering_connection "mgmt" {
  peer_owner_id = "883746017348"
  peer_vpc_id   = "vpc-a15ca3c5"
  peer_region   = "eu-west-1"
  vpc_id        = "${aws_vpc.vpc.*.id[0]}"
  auto_accept   = false

  tags {
    Name = "prod-us-mgmt-dev-eu-mgmt"
    Side = "Requester"
  }
}

resource aws_vpc_peering_connection "svc-ecosystems" {
  peer_owner_id = "955933158488"
  peer_vpc_id   = "vpc-054b9637732480d4b"
  peer_region   = "us-east-1"
  vpc_id        = "${aws_vpc.vpc.*.id[1]}"
  auto_accept   = false

  tags {
    Name = "prod-us-svc-ecosystems-prod"
    Side = "Requester"
  }
}

resource aws_vpc_peering_connection "mgmt-svc" {
  peer_owner_id = "804191495435"
  peer_vpc_id   = "${aws_vpc.vpc.*.id[1]}"
  vpc_id        = "${aws_vpc.vpc.*.id[0]}"
  auto_accept   = true

  tags {
    Name = "prod-us-mgmt-prod-us-svc"
    Side = "Requester"
  }
}

resource aws_vpc_peering_connection "svc-peer" {
  peer_owner_id = "804191495435"
  peer_vpc_id   = "${aws_vpc.vpc.*.id[2]}"
  vpc_id        = "${aws_vpc.vpc.*.id[1]}"
  auto_accept   = true

  tags {
    Name = "prod-us-svc-prod-us-peer"
    Side = "Requester"
  }
}

resource aws_vpc_peering_connection "mgmt-peer" {
  peer_owner_id = "804191495435"
  peer_vpc_id   = "${aws_vpc.vpc.*.id[2]}"
  vpc_id        = "${aws_vpc.vpc.*.id[0]}"
  auto_accept   = true

  tags {
    Name = "prod-us-mgmt-prod-us-peer"
    Side = "Requester"
  }
}

//
// Route Tables
//


resource aws_route_table "private_route_table" {

  vpc_id     = "${aws_vpc.vpc.*.id[0]}"
  
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.mgmt_gw.id}"
  }

  route {
    cidr_block                = "10.1.0.0/16"
    vpc_peering_connection_id = "${aws_vpc_peering_connection.mgmt.id}"
  }

  route {
    cidr_block = "10.41.0.0/16"
    vpc_peering_connection_id = "${aws_vpc_peering_connection.mgmt-svc.id}"
  }

  route {
    cidr_block = "10.46.0.0/16"
    vpc_peering_connection_id = "${aws_vpc_peering_connection.mgmt-peer.id}"
  }

  tags {
    Name = "prod-us-mgmt-private"
  }
}

resource aws_route_table "svc_private_route_table" {

  vpc_id     = "${aws_vpc.vpc.*.id[1]}"
  
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.svc_gw.id}"
  }

  route {
    cidr_block = "10.40.0.0/16"
    vpc_peering_connection_id = "${aws_vpc_peering_connection.mgmt-svc.id}"
  }

  route {
    cidr_block = "10.1.0.0/16"
    vpc_peering_connection_id = "pcx-06c068fed80d01757"
  }

  route {
    cidr_block = "10.12.0.0/16"
    vpc_peering_connection_id = "${aws_vpc_peering_connection.svc-ecosystems.id}"
  }

  route {
    cidr_block = "10.46.0.0/16"
    vpc_peering_connection_id = "${aws_vpc_peering_connection.svc-peer.id}"
  }

  tags {
    Name = "prod-us-svc-private"
  }
}

resource aws_route_table "peer_private_route_table" {

  vpc_id     = "${aws_vpc.vpc.*.id[2]}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.peer_gw.id}"
  }

  route {
    cidr_block = "10.1.0.0/16"
    vpc_peering_connection_id = "pcx-01d9267b3c16d15b3"
  }

  route {
    cidr_block = "10.158.40.0/21"
    vpc_peering_connection_id = "pcx-0526734355673c11c"

  }

  route {
    cidr_block = "10.40.0.0/16"
    vpc_peering_connection_id = "${aws_vpc_peering_connection.mgmt-peer.id}"
  }

  route {
    cidr_block = "10.41.0.0/16"
    vpc_peering_connection_id = "${aws_vpc_peering_connection.svc-peer.id}"
  }

  tags {
    Name = "prod-us-peer-private"
  }
}

//this doesnt work cross accounts yet so cli was used
//aws route53 associate-vpc-with-hosted-zone --hosted-zone-id Z24ARI6PGLG9YB --vpc VPCRegion=us-east-1,VPCId=vpc-0b401a7ba0c3bc232
/*
resource "aws_route53_zone_association" "harman-dns-peer-vpc" {
  zone_id = "Z24ARI6PGLG9YB"
  vpc_id  = "${aws_vpc.vpc.*.id[2]}"
}
*/

resource aws_route_table "public_route_table" {

  vpc_id     = "${aws_vpc.vpc.*.id[0]}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.*.id[0]}"
  }

  tags {
    Name = "prod-us-mgmt-public"
  }
}


resource aws_route_table "svc_public_route_table" {

  vpc_id     = "${aws_vpc.vpc.*.id[1]}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.*.id[1]}"
  }

  route {
    cidr_block = "10.1.0.0/16"
    vpc_peering_connection_id = "pcx-06c068fed80d01757"
  }

  route {
    cidr_block = "10.40.0.0/16"
    vpc_peering_connection_id = "${aws_vpc_peering_connection.mgmt-svc.id}"
  }

  tags {
    Name = "prod-us-svc-public"
  }
}

resource aws_route_table "peer_public_route_table" {

  vpc_id     = "${aws_vpc.vpc.*.id[2]}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.*.id[2]}"
  }

  tags {
    Name = "prod-us-peer-public"
  }
}


//
// Route Table Associations
//

resource aws_route_table_association "private_route_table_association" {
  count          = "${length(var.availability_zones)}"
  route_table_id = "${aws_route_table.private_route_table.id}"
  subnet_id      = "${module.mgmt_subnet.private[count.index]}"
}

resource aws_route_table_association "public_route_table_association" {
  count          = "${length(var.availability_zones)}"
  route_table_id = "${aws_route_table.public_route_table.id}"
  subnet_id      = "${module.mgmt_subnet.public[count.index]}"
}


resource aws_route_table_association "svc_private_route_table_association" {
  count          = "${length(var.availability_zones)}"
  route_table_id = "${aws_route_table.svc_private_route_table.id}"
  subnet_id      = "${module.svc_subnet.private[count.index]}"
}

resource aws_route_table_association "svc_public_route_table_association" {
  count          = "${length(var.availability_zones)}"
  route_table_id = "${aws_route_table.svc_public_route_table.id}"
  subnet_id      = "${module.svc_subnet.public[count.index]}"
}


resource aws_route_table_association "peer_private_route_table_association" {
  count          = "${length(var.availability_zones)}"
  route_table_id = "${aws_route_table.peer_private_route_table.id}"
  subnet_id      = "${module.peer_subnet.private[count.index]}"
}

resource aws_route_table_association "peer_public_route_table_association" {
  count          = "${length(var.availability_zones)}"
  route_table_id = "${aws_route_table.peer_public_route_table.id}"
  subnet_id      = "${module.peer_subnet.public[count.index]}"
}




resource aws_key_pair "public_key" {
  key_name   = "${var.env}-public-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDVCf3nwdglbtkk+32Z7aEuS6yZE3bUBVMJPnGkFggQRyEeK/ddvtyFUVPkEGOh5UFdhoHEW6PaDPKn0FuZwFi5xCo6chFmA8/EA4q5vDbCTUA5kze9f+ZreaGuCnDwIEWgrdKJpQUMcSjurQnU2cWBdC5qU9a/3UqvGhJwpINeU9nXklgbfZ9mW/iMkBgGGFz8hJTdLmkQjbeEcFsjlZgJnUf/aS1hPLwdLxIbfOwGmmrbLqh8SNM9NrkcNsjwMVH3piKELhi75x9X/ViHaMt5/04LpzwgzrgK3XWc+LLorWfQXTuyYGUh9pMI/pLEOrX8r4dSW2y6jZCUQftp9Xon"
}


module "security" {
  source = "../modules/security"
  mgmt_vpc_id = "${aws_vpc.vpc.*.id[0]}"
  svc_vpc_id = "${aws_vpc.vpc.*.id[1]}"
  peer_vpc_id = "${aws_vpc.vpc.*.id[2]}"
  env = "${var.env}"
}

module "masters" {
  source             = "../modules/masters"
  env                = "${var.env}"
  availability_zones = "${var.availability_zones}"
  subnet             = "${module.mgmt_subnet.private}"
  vpc_id             = "${aws_vpc.vpc.*.id[0]}"
  key_name           = "${aws_key_pair.public_key.key_name}"
  ami                = "ami-5c66ea23"
  masters_sg	     = "${module.security.masters_sg}"
  zone_id           = "Z3OGB3Y98LGQHS"
  zone_domain       = "spark-telematics.us"
}





module "database" {
  source = "../modules/database"
  env    = "${var.env}"
  subnet = "${module.mgmt_subnet.private}"
}
