variable base_path {
  default = "ssl-files/"
}
variable ca_path {
  default = "ssl-files/ca/"
}
variable cert_path {
  default = "ssl-files/certs/"
}
variable key_path {
  default = "ssl-files/keys/"
}
variable client_cert_path {
  default = "ssl-files/client_certs/"
}

resource "consul_key_prefix" "server_key" {
  path_prefix = "${var.key_path}"
  subkeys {
    star_spark_telematics_us = "${file("../files/keys/spark-telematics.key")}"
  }

}



resource "consul_key_prefix" "certs" {
  path_prefix = "${var.cert_path}"
  subkeys {
    "star_spark_telematics_us" = "${file("../files/certs/spark-telematics.pem")}"
  }
}


resource "consul_key_prefix" "ca" {
  path_prefix = "${var.ca_path}"
  subkeys {
    "digicert2018" = "${file("../files/ca/DigiCertCA2018.crt")}"
    "digicertCA" = "${file("../files/ca/DigiCertCA.crt")}"
    "prod_internalCA" = "${file("../files/ca/prod_internalCA.crt")}"


  }
}



