//private LB SVC


module "lb-3" {
  source    = "../modules/mesos-agent"
  ami       = "ami-5c66ea23"
  instance_type = "t2.medium"
  env       = "prod-us"
  net = "svc"
  security_groups = ["${module.security.lb-3_sg}"]
  key = "${aws_key_pair.public_key.key_name}"
  role = "mesos-agent"
  size = "3"
  subnet_groups = "${module.svc_subnet.private}"
  vpc      = "${aws_vpc.vpc.*.id[1]}"
  mesos_role = "*"
  disk_type = "gp2"
  disk_size = "100"
  cluster = "lb-3"
  app = "lb-3"
  type = "lb-3"
  ports = "[\"443-443\",\"80-80\"]"

  associate_public_ip  = "false"
}




resource "aws_route53_record" "lb-3_internal" {
  name    = "lb-3-internal.spark-telematics.us"
  type    = "A"
  records = ["${module.lb-3.private_ips}"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}

data template_file "lb-3_nginx_config" {
  template = "${file("../files/nginx.conf.ctmpl")}"
  vars {
    cluster = "lb-3"
  }
}

resource consul_keys "lb-3_template" {
  key {
    path   = "configuration/loadbalancer/lb-3/template"
    value  = "${data.template_file.lb-3_nginx_config.rendered}"
    delete = true
  }
}




module api-admin-report_telematics-firestarter-prod-1_vhost_internal {
  source               = "../modules/nginx-vhost/https-http"
  lb_cluster           = "lb-3"
  lb_dns_name          = "lb-3-internal.spark-telematics.us"
  vhosts               = ["admin-report-internal.spark-telematics.us"]
  vhost_entry_name     = "api-admin-report-telematics-internal"
  upstream_alias       = "api-admin-report-telematics-internal"
  dns_zone             = "Z3OGB3Y98LGQHS"
  service              = "telematics-firestarter-prod-1.api-admin-report"
  force_to_ssl         = "true"
  cert                 = "star_spark_telematics_us"
  ca                   = "digicert2018"
  server_key           = "star_spark_telematics_us"
  client_ca            = "tantalum"
  client_certs_enabled = "false"
}


module svc-admin-tantalum_telematics-firestarter-prod-1_vhost_internal {
  source               = "../modules/nginx-vhost/https-http"
  lb_cluster           = "lb-3"
  lb_dns_name          = "lb-3-internal.spark-telematics.us"
  vhosts               = ["admin-tantalum-telematics-internal.spark-telematics.us"]
  vhost_entry_name     = "svc-admin-tantalum-telematics-internal"
  upstream_alias       = "svc-admin-tantalum-telematics-internal"
  dns_zone             = "Z3OGB3Y98LGQHS"
  service              = "telematics-firestarter-prod-1.svc-admin-tantalum"
  force_to_ssl         = "true"
  cert                 = "star_spark_telematics_us"
  ca                   = "digicert2018"
  server_key           = "star_spark_telematics_us"
  client_ca            = "tantalum"
  client_certs_enabled = "false"
}




module svc-admin-tantalum_telematics-shared-prod-1_vhost_internal {
  source               = "../modules/nginx-vhost/https-http"
  lb_cluster           = "lb-3"
  lb_dns_name          = "lb-3-internal.spark-telematics.us"
  vhosts               = ["admin-tantalum-shared-internal.spark-telematics.us"]
  vhost_entry_name     = "svc-admin-tantalum-shared-internal"
  upstream_alias       = "svc-admin-tantalum-shared-internal"
  dns_zone             = "Z3OGB3Y98LGQHS"
  service              = "telematics-shared-prod-1.svc-admin-tantalum"
  force_to_ssl         = "true"
  cert                 = "star_spark_telematics_us"
  ca                   = "digicert2018"
  server_key           = "star_spark_telematics_us"
  client_ca            = "tantalum"
  client_certs_enabled = "false"
}
