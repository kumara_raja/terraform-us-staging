//Public LB SVC. for AT&T subscriptions. acl, allocated ips and squid proxy

variable "eipalloc" {
  type = "map"
  default = {
    eipalloc-03727eae5090bba66 = "18.204.54.122"
    eipalloc-0958bf2270cf5f4b3 = "18.204.115.94"
    eipalloc-0bca0a20db4727183 = "52.204.163.7"
 
  }
}


module "lb-subscriptions" {
  source    = "../modules/mesos-agent"
  ami       = "ami-5c66ea23"
  instance_type = "t2.medium"
  env       = "prod-us"
  net = "svc"
  security_groups = ["${module.security.lb-subscriptions_sg}"]
  key = "${aws_key_pair.public_key.key_name}"
  role = "mesos-agent"
  size = "${length(var.eipalloc)}"
  subnet_groups = "${module.svc_subnet.public}"
  vpc      = "${aws_vpc.vpc.*.id[1]}"
  mesos_role = "*"
  disk_type = "gp2"
  disk_size = "100"
  cluster = "lb-subscriptions"
  app = "lb-subscriptions"
  type = "lb-subscriptions"
  ports = "[\"443-443\",\"80-80\",\"3128-3128\"]"
  associate_public_ip = true
}


resource "aws_eip_association" "eip_assoc" {
  count         = "${length(var.eipalloc)}"
  instance_id   = "${(element(module.lb-subscriptions.instances,count.index))}"
  allocation_id = "${(element(keys(var.eipalloc),count.index))}"
}




resource "aws_route53_record" "lb-subscriptions_external" {
  name    = "lb-subscriptions.spark-telematics.us"
  type    = "A"
  records = ["${values(var.eipalloc)}"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}

resource "aws_route53_record" "lb-subscriptions_internal" {
  name    = "lb-subscriptions-internal.spark-telematics.us"
  type    = "A"
  records = ["${module.lb-subscriptions.private_ips}"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}

data template_file "lb-subscriptions_nginx_config" {
  template = "${file("../files/nginx.conf.ctmpl")}"
  vars {
    cluster = "lb-subscriptions"
  }
}

resource consul_keys "lb-subscriptions_template" {
  key {
    path   = "configuration/loadbalancer/lb-subscriptions/template"
    value  = "${data.template_file.lb-subscriptions_nginx_config.rendered}"
    delete = true
  }
}




module api-subscriptions_prod_1_vhost_public {
  source               = "../modules/nginx-vhost/https-http"
  lb_cluster           = "lb-subscriptions"
  lb_dns_name          = "lb-subscriptions.spark-telematics.us"
  vhosts               = ["subscriptions.spark-telematics.us"]
  vhost_entry_name     = "api-subscriptions-prod-us-public"
  upstream_alias       = "api-subscriptions-prod-us-public"
  dns_zone             = "Z3OGB3Y98LGQHS"
  service              = "telematics-firestarter-prod-1.api-subscriptions"
  force_to_ssl         = "true"
  cert                 = "star_spark_telematics_us"
  ca                   = "digicert2018"
  server_key           = "star_spark_telematics_us"
  client_ca            = "tantalum"
  client_certs_enabled = "false"
}



module subscriptions_proxy_vhost_internal {
  source               = "../modules/nginx-vhost/http-http"
  lb_cluster           = "lb-subscriptions"
  lb_dns_name          = "lb-subscriptions-internal.spark-telematics.us"
  vhosts               = ["subscriptions-proxy.spark-telematics.us"]
  vhost_entry_name     = "subscriptions_proxy-prod-us-internal"
  upstream_alias       = "subscriptions_proxy-prod-us-internal"
  dns_zone             = "Z3OGB3Y98LGQHS"
  service              = "service_svc_subscriptions-proxy"
}

