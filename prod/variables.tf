variable "env" {
  default = "prod-us"
}

variable "availability_zones" {
  type = "list"

  default = [
    "us-east-1a",
    "us-east-1b",
    "us-east-1c",
  ]
}

variable "vpcs" {
  type = "list"

  default = [
    "mgmt",
    "svc",
    "peer",
    "ngfw",
  ]
}

variable "vpc_cidr" {
  type = "map"

  default = {
    mgmt = "10.40.0.0/16"
    svc  = "10.41.0.0/16"
    peer = "10.46.0.0/16"
    ngfw = "10.47.0.0/16"
  }
}
