data "aws_iam_policy_document" "instance-assume-role-policy-for-spotfleet-mgmt" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["spotfleet.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "spot-fleet-mgmt-tagging-role" {
  assume_role_policy = "${data.aws_iam_policy_document.instance-assume-role-policy-for-spotfleet-mgmt.json}"
  name               = "spot-fleet-mgmt-tagging-role"
}

resource "aws_iam_role_policy_attachment" "spot_request_policy_mgmt" {
  role       = "${aws_iam_role.spot-fleet-mgmt-tagging-role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole"
}

data "template_file" "mesos_slave_mgmt" {
  template = "${file("../templates/mesos-slave.tpl")}"
  vars {
    NET = "mgmt"
    PORTS = "[30000-34000]"
    ENV = "prod"
    ROLE = "*"
    TYPE = "shared"

  }
}

resource "aws_spot_fleet_request" "spot_request_mgmt" {
  iam_fleet_role                      = "${aws_iam_role.spot-fleet-mgmt-tagging-role.arn}"
  allocation_strategy                 = "diversified"
  spot_price                          = "0.05"
  target_capacity                     = 50
  replace_unhealthy_instances         = true
  terminate_instances_with_expiration = false
  valid_until                         = "2022-12-31T23:59:59Z"

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.large"
    weighted_capacity      = 2
    subnet_id              = "${module.mgmt_subnet.private[0]}"
    vpc_security_group_ids = ["${module.security.mgmt_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_mgmt.rendered}"

    tags = {
      Name      = "spot-fleet-mgmt"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.large"
    weighted_capacity      = 2
    subnet_id              = "${module.mgmt_subnet.private[1]}"
    vpc_security_group_ids = ["${module.security.mgmt_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_mgmt.rendered}"

    tags = {
      Name      = "spot-fleet-mgmt"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.large"
    weighted_capacity      = 2
    subnet_id              = "${module.mgmt_subnet.private[2]}"
    vpc_security_group_ids = ["${module.security.mgmt_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_mgmt.rendered}"

    tags = {
      Name      = "spot-fleet-mgmt"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.xlarge"
    weighted_capacity      = 4
    subnet_id              = "${module.mgmt_subnet.private[0]}"
    vpc_security_group_ids = ["${module.security.mgmt_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_mgmt.rendered}"

    tags = {
      Name      = "spot-fleet-mgmt"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.xlarge"
    weighted_capacity      = 4
    subnet_id              = "${module.mgmt_subnet.private[1]}"
    vpc_security_group_ids = ["${module.security.mgmt_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_mgmt.rendered}"

    tags = {
      Name      = "spot-fleet-mgmt"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.xlarge"
    weighted_capacity      = 4
    subnet_id              = "${module.mgmt_subnet.private[2]}"
    vpc_security_group_ids = ["${module.security.mgmt_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_mgmt.rendered}"

    tags = {
      Name      = "spot-fleet-mgmt"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.2xlarge"
    weighted_capacity      = 8
    subnet_id              = "${module.mgmt_subnet.private[0]}"
    vpc_security_group_ids = ["${module.security.mgmt_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_mgmt.rendered}"

    tags = {
      Name      = "spot-fleet-mgmt"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.2xlarge"
    weighted_capacity      = 8
    subnet_id              = "${module.mgmt_subnet.private[1]}"
    vpc_security_group_ids = ["${module.security.mgmt_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_mgmt.rendered}"

    tags = {
      Name      = "spot-fleet-mgmt"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.2xlarge"
    weighted_capacity      = 8
    subnet_id              = "${module.mgmt_subnet.private[2]}"
    vpc_security_group_ids = ["${module.security.mgmt_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_mgmt.rendered}"

    tags = {
      Name      = "spot-fleet-mgmt"
      SpotFleet = true
    }
  }


  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.4xlarge"
    weighted_capacity      = 16
    subnet_id              = "${module.mgmt_subnet.private[0]}"
    vpc_security_group_ids = ["${module.security.mgmt_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_mgmt.rendered}"

    tags = {
      Name      = "spot-fleet-mgmt"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.10xlarge"
    weighted_capacity      = 16
    subnet_id              = "${module.mgmt_subnet.private[1]}"
    vpc_security_group_ids = ["${module.security.mgmt_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_mgmt.rendered}"

    tags = {
      Name      = "spot-fleet-mgmt"
      SpotFleet = true
    }
  }

  launch_specification {
    ami                    = "ami-5c66ea23"
    instance_type          = "m4.16xlarge"
    weighted_capacity      = 8 
    subnet_id              = "${module.mgmt_subnet.private[2]}"
    vpc_security_group_ids = ["${module.security.mgmt_spot_sg}"]
    key_name               = "${aws_key_pair.public_key.key_name}"
    user_data              = "${data.template_file.mesos_slave_mgmt.rendered}"

    tags = {
      Name      = "spot-fleet-mgmt"
      SpotFleet = true
    }
  }
}
