


module "consul" {
  source             = "../modules/consul"
  env                = "${var.env}"
  availability_zones = "${var.availability_zones}"
  subnet             = "${module.mgmt_subnet.private}"
  vpc_id             = "${aws_vpc.vpc.*.id[0]}"
  key_name           = "${aws_key_pair.public_key.key_name}"
  ami                = "ami-5c66ea23"
  consul_sg      = "${module.security.consul_sg}"
}




provider "consul" {
  address    = "consul.service.consul:8500"
  datacenter = "dc1"
  token      = "0CF6F73C-A62D-4EF2-B08E-128A616CF586"
}


resource "consul_catalog_entry" "dns-peer" {
  count   = 1
  address = "10.46.0.2"
  node    = "dns-peer"

  service = {
    address = "10.46.0.2"
    id      = "dns-peer"
    name    = "dns-peer"
    port    = 53
    tags    = ["dns"]
  }
}



resource "consul_catalog_entry" "mesos-master" {
  count   = 3
  address = "${module.masters.masters_ips[count.index]}"
  node    = "mesos-${count.index}"

  service = {
    address = "${module.masters.masters_ips[count.index]}"
    id      = "mesos-${count.index}"
    name    = "mesos"
    port    = 5050
    tags    = ["mesos"]
  }
}

resource "consul_catalog_entry" "marathon" {
  count   = 3
  address =  "${module.masters.masters_ips[count.index]}"
  node    = "marathon-${count.index}"

  service = {
    address = "${module.masters.masters_ips[count.index]}"
    id      = "marathon-${count.index}"
    name    = "marathon"
    port    = 8080
    tags    = ["marathon"]
  }
}

resource "consul_catalog_entry" "zookeeper" {
  count   = 3
  address =  "${module.masters.masters_ips[count.index]}"
  node    = "zookeeper-${count.index}"

  service = {
    address = "${module.masters.masters_ips[count.index]}"
    id      = "marathon-${count.index}"
    name    = "zookeeper"
    port    = 2181
    tags    = ["zookeeper"]
  }
}

resource "consul_catalog_entry" "kafka" {
  count   = 3
  address = "${module.kafka.kafka_ips[count.index]}"
  node    = "kafka-${count.index}"

  service = {
    address = "${module.kafka.kafka_ips[count.index]}"
    id      = "kafka-${count.index}"
    name    = "kafka"
    port    = 8080
    tags    = ["kafka"]
  }
}
/*
resource "consul_catalog_entry" "zkkafka" {
  count   = 3
  address = "${module.kafka.kafka_ips[count.index]}"
  node    = "zkkafka-${count.index}"

  service = {
    address = "${module.kafka.kafka_ips[count.index]}"
    id      = "zkkafka-${count.index}"
    name    = "zkkafka"
    port    = 2181
    tags    = ["zkkafka"]
  }
}
*/


resource "consul_catalog_entry" "cassandra" {
  count   = 3
  address = "${module.cassandra.cassandra_ips[count.index]}"
  node    = "cassandra-1-node-${count.index}"

  service = {
    address = "${module.cassandra.cassandra_ips[count.index]}"
    id      = "cassandra-${count.index}"
    name    = "cassandra"
    port    = 8080
    tags    = ["cassandra"]
  }

}
