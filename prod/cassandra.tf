
module "cassandra" {
  source             = "../modules/cassandra"
  env                = "${var.env}"
  availability_zones = "${var.availability_zones}"
  subnet             = "${module.mgmt_subnet.private}"
  vpc_id             = "${aws_vpc.vpc.*.id[0]}"
  key_name           = "${aws_key_pair.public_key.key_name}"
  ami                = "ami-5c66ea23"
  cassandra_sg	     = "${module.security.cassandra_sg}"
}
