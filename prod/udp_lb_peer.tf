
module "udp-lb-peer" {
  source    = "../modules/mesos-agent"
  ami       = "ami-5c66ea23"
  instance_type = "t2.medium"
  env       = "prod-us"
  net = "svc-peering"
  security_groups = ["${module.security.udp-lb-peer_sg}"]
  key = "${aws_key_pair.public_key.key_name}"
  role = "mesos-agent"
  size = "1"
  subnet_groups = "${module.peer_subnet.private}"
  vpc      = "${aws_vpc.vpc.*.id[2]}"
  mesos_role = "*"
  disk_type = "gp2"
  disk_size = "100"
  cluster = "udp-lb-peer"
  app = "udp-lb-peer"
  type = "udp-lb-peer"
  ports = "[\"53-53\"]"
  private_ips = ["10.46.0.100"]
}



resource "aws_route53_record" "udp-lb-peer_internal" {
  name    = "udp-lb-peer-internal.spark-telematics.us"
  type    = "A"
  records = ["${module.udp-lb-peer.private_ips}"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}


data template_file "udp-lb-peer_nginx_config" {
  template = "${file("../files/tcp-nginx.conf.ctmpl")}"
  vars {
    cluster = "udp-lb-peer"
  }
}

resource consul_keys "udp-lb-peer_template" {
  key {
    path   = "configuration/loadbalancer/udp-lb-peer/template"
    value  = "${data.template_file.udp-lb-peer_nginx_config.rendered}"
    delete = true
  }
}



// Nb. not a good idea to address a dns by its dns as its a catch 22
resource "aws_route53_record" "dns_peer_internal" {
  name    = "dns-peer-internal.spark-telematics.us"
  type    = "CNAME"
  records = ["udp-lb-peer-internal.spark-telematics.us"]
  zone_id = "Z3OGB3Y98LGQHS"
  ttl     = "300"
}


resource "consul_key_prefix" "vhost_keys_dns_peer" {
  path_prefix = "configuration/loadbalancer/udp-lb-peer/virtualhosts/dns-peer/"
  subkeys {
    "alias"            = <<EOF
["dns-peer-internal.spark-telematics.us"]
EOF
    "backend"           = "dns-peer"
    "backend-alias"     = "dns-peer"
    "ssl-enabled"       = "false"
    "force-to-ssl"      = "false"
    "tcp-vhost"         = "false"
    "udp-vhost"         = "true"
    "udp-port"          = "53"
    "upstream-ssl"      = "false"
  }
}


