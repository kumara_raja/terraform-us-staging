
variable "domain" {
    default = "us-prod"
}

variable "version" {
    default = "6.2"
}

variable "node_type" {
    default = "r3.large.elasticsearch"
}

variable "instance_count" {
    default = "2"
}

variable "master_node_count" {
    default = 3
}

variable "volume_size" {
    default = 100
}

variable "private_subnets" {
    type = "list"
    default = [
       "subnet-09f3ad1dea3b69a51",
       "subnet-02cd2ae8f295ff0f9"
    ]
}

variable "es_sg"{}
