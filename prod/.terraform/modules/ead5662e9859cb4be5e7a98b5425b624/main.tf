data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

resource "aws_iam_service_linked_role" "es" {
  aws_service_name = "es.amazonaws.com"
}

resource "aws_elasticsearch_domain" "es" {
    domain_name = "${var.domain}"
    elasticsearch_version = "${var.version}"

    advanced_options {
        "rest.action.multi.allow_explicit_index" = "true"
    }

    cluster_config {
        instance_type = "${var.node_type}"
        instance_count = "${var.instance_count}"
        dedicated_master_enabled = true  
        zone_awareness_enabled = false
        dedicated_master_count = "${var.master_node_count}" 
        zone_awareness_enabled = true
    }

    ebs_options {
        ebs_enabled = true
        volume_size = "${var.volume_size}"
    }

    vpc_options {
        subnet_ids = ["${var.private_subnets}"]
        security_group_ids = ["${var.es_sg}"]
    }

    tags {
        Domain = "us-prod"
        Name = "ES-cluster"
    }

  access_policies = <<CONFIG
 {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": "*",
            "Effect": "Allow",
            "Resource": "arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/${var.domain}/*"
        }
    ]
}
CONFIG

}


