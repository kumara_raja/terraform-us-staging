variable "env" {}
variable "vpc_id" {}
variable "kafka_sg" {}
variable "subnet" {
  type = "list"
}

variable "availability_zones" {
  type = "list"
}

variable "ami" {}

variable "key_name" {}
