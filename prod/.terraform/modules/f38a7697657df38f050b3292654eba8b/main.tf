resource aws_instance "kafka" {
  count                       = "${length(var.availability_zones)}"
  ami                         = "${var.ami}"
  associate_public_ip_address = false
  instance_type               = "c4.4xlarge"
  key_name                    = "${var.key_name}"
  subnet_id                   = "${var.subnet[count.index]}"
  vpc_security_group_ids      = ["${var.kafka_sg}"]


  user_data = <<EOF
#cloud-config
package_update: true
package_upgrade: true
packages: 
  - 'python'
manage_etc_hosts: false
manage_resolv_conf: false

runcmd:
 - echo "127.0.0.1 localhost $(hostname)" > /etc/hosts 
# - apt update
# - apt install -y python
EOF

  tags {
    Name    = "kafka-${count.index}"
    Service = "kafka"
    Kafka   = "true"
  }
}


