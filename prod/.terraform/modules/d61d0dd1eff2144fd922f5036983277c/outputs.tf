output consul_ips {
  value = "${aws_instance.consul.*.private_ip}"
}

output consul_sg {
  value = "${var.consul_sg}"
}
