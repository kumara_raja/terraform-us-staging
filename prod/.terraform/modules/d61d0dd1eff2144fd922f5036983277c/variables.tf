variable "env" {}
variable "vpc_id" {}
variable "consul_sg" {}

variable "subnet" {
  type = "list"
}

variable "availability_zones" {
  type = "list"
}

variable "ami" {}

variable "key_name" {}
