variable "vpc_id" {}

variable "vpc_name" {}

variable "availability_zones" {
  type = "list"
}

variable "cidr_block" {}
