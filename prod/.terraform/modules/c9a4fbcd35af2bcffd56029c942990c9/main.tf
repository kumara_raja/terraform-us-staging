resource aws_subnet "private" {
  vpc_id                  = "${var.vpc_id}"
  count                   = "${length(var.availability_zones)}"
  cidr_block              = "${cidrsubnet(var.cidr_block, 6, count.index)}"
  availability_zone       = "${var.availability_zones[count.index]}"
  map_public_ip_on_launch = false

  tags {
    Name = "${var.vpc_name}-private-${var.availability_zones[count.index]}"
  }
}

resource aws_subnet "public" {
  vpc_id                  = "${var.vpc_id}"
  count                   = "${length(var.availability_zones)}"
  cidr_block              = "${cidrsubnet(var.cidr_block, 6, count.index + length(var.availability_zones) + 1)}"
  availability_zone       = "${var.availability_zones[count.index]}"
  map_public_ip_on_launch = false

  tags {
    Name = "${var.vpc_name}-public-${var.availability_zones[count.index]}"
  }
}
