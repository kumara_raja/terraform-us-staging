resource "aws_route53_record" "dns_records" {
  count   = "${length(var.vhosts)}"
  name    = "${var.vhosts[count.index]}"
  type    = "CNAME"
  zone_id = "${var.dns_zone}"
  ttl     = "300"
  records = ["${var.lb_dns_name}"]
}
resource "consul_key_prefix" "vhost_keys" {
  path_prefix = "${var.bp}/${var.lb_cluster}/virtualhosts/${var.vhost_entry_name}/"
  subkeys {
    "alias"            = <<EOF
["${join("\",\"", var.vhosts)}"]
EOF
    "backend"          = "${var.service}"
    "backend-alias"    = "${var.upstream_alias}"
    "ssl-enabled"      = "${var.ssl_enabled}"
    "unsecure-enabled" = "${var.unsecure_enabled}"
    "force-to-ssl"     = "${var.force_to_ssl}"
    "ssl-server-key"   = "${var.server_key}"
    "ssl-ca"           = "${var.ca}"
    "ssl-cert"         = "${var.cert}"
    "vhosts_ip_allow"  = "${var.vhosts_ip_allow}"
    "client_certs_enabled"  = "${var.client_certs_enabled}"
    "ssl-client-ca"    = "${var.client_ca}"
  }
}
