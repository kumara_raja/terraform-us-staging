variable "env" {}

variable "vpc_id" {}

variable "masters_sg" {}

variable "subnet" {
  type = "list"
}

variable "availability_zones" {
  type = "list"
}

variable "ami" {}

variable "key_name" {}

variable "hm_instances" {
	default = "3"
}

variable "zone_id" {}

variable "zone_domain" {}