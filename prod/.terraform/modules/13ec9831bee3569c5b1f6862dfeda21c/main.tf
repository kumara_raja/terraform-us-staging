resource aws_instance "masters" {
  count                       = "${var.hm_instances}"
  ami                         = "${var.ami}"
  associate_public_ip_address = false
  instance_type               = "c4.large"
  key_name                    = "${var.key_name}"
  subnet_id                   = "${var.subnet[count.index]}"
  vpc_security_group_ids      = ["${var.masters_sg}"]

  user_data = <<EOF
#cloud-config
package_update: true
package_upgrade: true
packages: 
  - 'python'
manage_etc_hosts: false
manage_resolv_conf: false

runcmd:
 - echo "127.0.0.1 localhost $(hostname)" > /etc/hosts 
# - apt update
# - apt install -y python
EOF

  tags {
    Name    = "cluster-master-${count.index}"
    Service = "master"
    Mesos   = "true"
	  Marathon = "true"
	  Zookeeper = "true"
  }
}


resource "aws_route53_record" "marathon" {
  zone_id = "${var.zone_id}"
  name    = "marathon.${var.zone_domain}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.masters.*.private_ip}"]
}
