variable ami {}
variable instance_type {}
variable env {}
variable net {}
variable security_groups {
  type = "list"
}
variable key {}
variable role {}

variable size {
  default = "1"
}

variable template {

  default = "../templates/mesos-slave.tpl"
}

variable subnet_groups {
  type = "list"
}
variable vpc {}
variable mesos_role {}
variable disk_type {}
variable disk_size {}
variable cluster {}
variable app {}
variable ports {}
variable type {}

variable associate_public_ip {
  default = "false"
}

//default loop through a blank list. (max 8 instances unless you increase)
variable private_ips {
  type = "list"
  default = ["","","","","","","",""]
}
