

resource "aws_security_group" "peer_spot_sg" {
  name   = "peer_spot"
  vpc_id = "${var.peer_vpc_id}"

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = "${var.ssh_cidr}"
  }

  ingress {
    description = "mesos agents"
    from_port   = 5051
    to_port     = 5051
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "microservices"
    from_port   = 30000
    to_port     = 34000
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    self = true
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Mesos = true
    Service  = "spot"
  }
}



resource "aws_security_group" "svc_spot_sg" {
  name   = "svc_spot"
  vpc_id = "${var.svc_vpc_id}"

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = "${var.ssh_cidr}"
  }

  ingress {
    description = "mesos agents"
    from_port   = 5051
    to_port     = 5051
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "microservices"
    from_port   = 30000
    to_port     = 34000
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    self = true
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Mesos = true
    Service  = "spot"
  }
}

resource "aws_security_group" "mgmt_spot_sg" {
  name   = "mgmt_spot"
  vpc_id = "${var.mgmt_vpc_id}"

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "mesos agents"
    from_port   = 5051
    to_port     = 5051
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "microservices"
    from_port   = 30000
    to_port     = 34000
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    self = true
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Mesos = true
    Service  = "spot"
  }
}

resource "aws_security_group" "masters_sg" {
  name   = "marathon"
  vpc_id = "${var.mgmt_vpc_id}"

  ingress {
    description = "internal traffic"
    from_port = 0
    to_port = 0
    protocol = -1
    self = true
  }

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "marathon"
    from_port   = 8080
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "mesos masters"
    from_port   = 5050
    to_port     = 5050
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "zookeeper"
    from_port   = 2181
    to_port     = 2181
    protocol    = "tcp"
    cidr_blocks = ["${var.mgmt_cidr}", "${var.svc_cidr}", "${var.peer_cidr}" ]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    self = true
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name = "masters"
    Marathon = true
    Zookeeper = true
    Mesos = true
    Service  = "master"
  }
}



resource "aws_security_group" "lb-1" {
  name   = "lb-1"
  vpc_id = "${var.mgmt_vpc_id}"

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5050
    to_port     = 5050
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5051
    to_port     = 5051
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    lb-1   = true
    Service = "lb-1"
  }
}


resource "aws_security_group" "lb-2" {
  name   = "lb-2"
  vpc_id = "${var.svc_vpc_id}"

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "mesos"
    from_port   = 5050
    to_port     = 5050
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5051
    to_port     = 5051
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    lb-2   = true
    Service = "lb-2"
  }
}





resource "aws_security_group" "lb-3" {
  name   = "lb-3"
  vpc_id = "${var.svc_vpc_id}"

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5050
    to_port     = 5050
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5051
    to_port     = 5051
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    lb-3   = true
    Service = "lb-3"
  }
}




resource "aws_security_group" "lb-peer" {
  name   = "lb-peer"
  vpc_id = "${var.peer_vpc_id}"

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "mesos"
    from_port   = 5050
    to_port     = 5050
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5051
    to_port     = 5051
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    lb-peer   = true
    Service = "lb-peer"
  }
}


resource "aws_security_group" "lb-subscriptions" {
  name   = "lb-subscriptions"
  vpc_id = "${var.svc_vpc_id}"

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "squid"
    from_port   = 3128
    to_port     = 3128
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.att_cidr}","${var.internal_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5050
    to_port     = 5050
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5051
    to_port     = 5051
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    lb-subscriptions   = true
    Service = "lb-subscriptions"
  }
}

resource "aws_security_group" "lb-idm" {
  name   = "lb-idm"
  vpc_id = "${var.svc_vpc_id}"

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "mesos"
    from_port   = 5050
    to_port     = 5050
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5051
    to_port     = 5051
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    lb-idm   = true
    Service = "lb-idm"
  }
}


resource "aws_security_group" "cassandra" {
  name   = "cassandra"
  vpc_id = "${var.mgmt_vpc_id}"

  ingress {
    description = "internal traffic"
    from_port = 0
    to_port = 0
    protocol = -1
    self = true
  }

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "cassandra"

    from_port   = 7000
    to_port     = 7000
    self = true
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "cassandra"
    from_port   = 7001
    to_port     = 7001
    self = true
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "cassandra"
    from_port   = 7199
    to_port     = 7199
    self = true
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "cassandra"
    from_port   = 9042
    to_port     = 9042
    self = true
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "cassandra"
    from_port   = 9160
    to_port     = 9160
    self = true
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "cassandra"
    from_port   = 9142
    to_port     = 9142
    self = true

    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name      = "cassandra"
    Cassandra = true
    Service   = "cassandra"
  }
}

resource aws_security_group "consul" {
  name   = "${var.env}-consul"
  vpc_id = "${var.mgmt_vpc_id}"

  ingress {
    description = "ssh"
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "dns"
    from_port = 53
    to_port   = 53
    protocol  = "udp"
    self = true

    cidr_blocks = [
      "${var.internal_cidr}",
    ]
  }

  ingress {
    description = "consul"
    from_port = 8300
    to_port   = 8302
    protocol  = "tcp"
    self = true
    cidr_blocks = [
      "${var.internal_cidr}",
    ]
  }

  ingress {
    description = "consul"
    from_port = 8300
    to_port   = 8302
    protocol  = "udp"
    self = true
    cidr_blocks = [
      "${var.internal_cidr}",
    ]
  }

  ingress {
    description = "consul"
    from_port = 8400
    to_port   = 8400
    protocol  = "tcp"
    self = true
    cidr_blocks = [
      "${var.internal_cidr}",
    ]
  }

  ingress {
    description = "consul"
    from_port = 8500
    to_port   = 8500
    protocol  = "tcp"
    self = true
    cidr_blocks = [
      "${var.internal_cidr}",
    ]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

resource "aws_security_group" "kafka" {
  name   = "kafka"
  vpc_id = "${var.mgmt_vpc_id}"

  ingress {
    description = "internal traffic"
    from_port = 0
    to_port = 0
    protocol = -1
    self = true
  }

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "zookeeper"
    from_port   = 2181
    to_port     = 2181
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "kafka"
    from_port   = 9092
    to_port     = 9092
    protocol    = "tcp"
    self = true
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    self = true
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name    = "kafka"
    Kafka   = true
    Service = "kafka"
  }
}



resource "aws_security_group" "tcp-lb-1" {
  name   = "tcp-lb-1"
  vpc_id = "${var.svc_vpc_id}"

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "helium/tcp"
    from_port   = 5654
    to_port     = 5654
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "helium/udp"
    from_port   = 4305
    to_port     = 4305
    protocol    = "udp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "helium/tls"
    from_port   = 5443
    to_port     = 5443
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "helium/mgmt"
    from_port   = 9003
    to_port     = 9003
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "helium/jmx"
    from_port   = 9004
    to_port     = 9004
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "helium/metrics"
    from_port   = 9145
    to_port     = 9145
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5050
    to_port     = 5050
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5051
    to_port     = 5051
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    tcp-lb-1   = true
    Service = "tcp-lb-1"
  }
}



resource "aws_security_group" "udp-lb-peer" {
  name   = "udp-lb-peer"
  vpc_id = "${var.peer_vpc_id}"

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "dns"
    from_port   = 53
    to_port     = 53
    protocol    = "udp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5050
    to_port     = 5050
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "mesos"
    from_port   = 5051
    to_port     = 5051
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  ingress {
    description = "consul"
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    tcp-lb-1   = true
    Service = "udp-lb-peer"
  }
}

resource "aws_security_group" "es" {
  name   = "elasticsearch"
  vpc_id = "${var.mgmt_vpc_id}"

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.internal_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name    = "elasticsearch"
    Service = "elasticsearch"
  }
}
