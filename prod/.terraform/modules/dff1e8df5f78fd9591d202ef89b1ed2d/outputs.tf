output masters_sg {
  value = "${aws_security_group.masters_sg.id}"
}

output svc_spot_sg {
  value = "${aws_security_group.svc_spot_sg.id}"
}

output mgmt_spot_sg {
  value = "${aws_security_group.mgmt_spot_sg.id}"
}


output cassandra_sg {
  value = "${aws_security_group.cassandra.id}"
}

output consul_sg {
  value = "${aws_security_group.consul.id}"
}

output kafka_sg {
  value = "${aws_security_group.kafka.id}"
}

output lb-1_sg {
  value = "${aws_security_group.lb-1.id}"
}

output lb-2_sg {
  value = "${aws_security_group.lb-2.id}"
}

output lb-3_sg {
  value = "${aws_security_group.lb-3.id}"
}

output lb-subscriptions_sg {
  value = "${aws_security_group.lb-subscriptions.id}"
}


output lb-peer_sg {
  value = "${aws_security_group.lb-peer.id}"
}

output peer_spot_sg {
  value = "${aws_security_group.peer_spot_sg.id}"
}

output lb-idm_sg {
  value = "${aws_security_group.lb-idm.id}"
}


output tcp-lb-1_sg {
    value = "${aws_security_group.tcp-lb-1.id}"
}

output udp-lb-peer_sg {
    value = "${aws_security_group.udp-lb-peer.id}"
}

output es_sg {
    value = "${aws_security_group.es.id}"
}




