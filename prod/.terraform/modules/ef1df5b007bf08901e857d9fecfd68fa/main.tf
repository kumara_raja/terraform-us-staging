
resource "aws_security_group" "alb_proxy" {
  name        = "${var.name}"
  description = "${var.name}"
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    cidr_blocks = "${var.ingress_cidr_blocks}"
  }

  egress {
    from_port       = 443
    to_port         = 443
    protocol        = "TCP"
    cidr_blocks     = "${var.egress_cidr_blocks}"

}
}

resource "aws_lb" "alb_proxy" {
  name               = "${var.name}"
  internal           = true
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.alb_proxy.id}"]
  subnets         = ["${var.subnets}"]
}


resource "aws_lb_listener" "alb_proxy" {
  load_balancer_arn = "${aws_lb.alb_proxy.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "${var.lb_ssl_policy}"
  certificate_arn   = "${var.certificate_arn}"

  default_action {
    target_group_arn = "${aws_lb_target_group.alb_proxy.arn}"
    type             = "forward"
  }
}


resource "aws_lb_listener_rule" "alb_proxy" {
  listener_arn = "${aws_lb_listener.alb_proxy.arn}"
  priority     = 100
  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.alb_proxy.arn}"
  }

  condition {
    field  = "path-pattern"
    values = ["${var.path_pattern}"]
  }
}


resource "aws_lb_target_group" "alb_proxy" {
  name     = "${var.name}"
  port     = 443
  protocol = "HTTPS"
  target_type = "ip"
  vpc_id = "${var.vpc_id}"
}

resource "aws_lb_target_group_attachment" "alb_proxy" {
  target_group_arn = "${aws_lb_target_group.alb_proxy.arn}"
  target_id        = "${var.target_ip[count.index]}"
  count = "${length(var.target_ip)}"
  port             = 443
  availability_zone = "all"
}


resource "aws_route53_record" "alb_proxy" {
  zone_id = "${var.zone_id}"
  name    = "${var.route53_record_name}"
  type    = "A"

  alias {
    name                   = "${aws_lb.alb_proxy.dns_name}"
    zone_id                = "${aws_lb.alb_proxy.zone_id}"
    evaluate_target_health = true
  }
}
