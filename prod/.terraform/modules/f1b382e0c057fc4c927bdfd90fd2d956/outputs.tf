output database_arn {
  value = "${aws_db_instance.tantalum_db.*.arn}"
}
