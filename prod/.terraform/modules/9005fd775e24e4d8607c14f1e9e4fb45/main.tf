resource aws_instance "cassandra" {
  count                       = "${length(var.availability_zones)}"
  ami                         = "${var.ami}"
  associate_public_ip_address = false
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  subnet_id                   = "${var.subnet[count.index]}"
  vpc_security_group_ids      = ["${var.cassandra_sg}"]


  user_data = <<EOF
#cloud-config
package_update: true
package_upgrade: true
packages: 
  - 'python'
manage_etc_hosts: false
manage_resolv_conf: false

runcmd:
 - echo "nameserver 8.8.8.8" > /etc/resolv.conf
 - echo "127.0.0.1 localhost $(hostname)" > /etc/hosts 
 - apt update
 - apt install -y python
  EOF


  tags {
    Name      = "cassandra-${count.index}"
    Service   = "cassandra"
    Cassandra = "true"
  }
}

