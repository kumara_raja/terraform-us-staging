resource aws_instance "mesos-agent" {
  count =                       "${var.size}"
  vpc_security_group_ids =      ["${var.security_groups}"]
  ami =                         "${var.ami}"
  instance_type =               "${var.instance_type}"
  associate_public_ip_address = "${var.associate_public_ip}"
  subnet_id =                   "${var.subnet_groups[count.index]}"
  key_name =                    "${var.key}"
  user_data = "${data.template_file.mesos_agent.rendered}"
  private_ip =                  "${var.private_ips[count.index]}"
  root_block_device {
    delete_on_termination = true
    volume_type =           "${var.disk_type}"
    volume_size =           "${var.disk_size}"
  }

  tags {
    Name =             "${var.cluster}-node-${count.index}"
    Cluster =          "${var.cluster}"
    Service =          "mesos_agent"
    mesos_attributes = "app:${var.app};cluster:${var.cluster};node:${count.index}"
    role =             "${var.role}"
    mesos_role =       "${var.mesos_role}"
    net =              "${var.net}"
    env =              "${var.env}"
    node =             "${count.index}"
    type = "${var.type}"
    ports = "${var.ports}"
  }

  lifecycle {
    create_before_destroy = true
  }

}


data "template_file" "mesos_agent" {
  template = "${file("${var.template}")}"
  vars {
    NET = "${var.net}"
    PORTS = "${var.ports}"
    ENV = "${var.env}"
    ROLE = "${var.mesos_role}"
    TYPE = "${var.type}"
  }
}

