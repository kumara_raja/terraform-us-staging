#!/bin/bash

ansible-galaxy install -r ../requirements.yml --force
ansible-playbook -i ./ec2.py -u ubuntu --extra-vars "variable_host=tag_Service_mesos_agent" --key-file ~/.ssh/prod_us.pem ../playbooks/mesos-agent.yml
