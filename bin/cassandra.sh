#!/bin/bash
#ansible-galaxy install -r ../requirements.yml --force
#terraform state pull > .terraform/ansible.tfstate
#TF_STATE=.terraform/ansible.tfstate ansible-playbook --inventory-file=$(which terraform-inventory) ../playbooks/consul.yml --verbose


ansible -i ./service_inventory -u ubuntu --key-file ~/.ssh/us_prod.pem cassandranodes -m ping

#ansible -i ./service_inventory -u ubuntu --key-file ~/.ssh/us_prod.pem cassandranodes -m setup

#ansible-playbook -i ./service_inventory -u ubuntu --key-file ~/.ssh/us_prod.pem --check ../playbooks/cassandra.yml


ansible-playbook -i ./service_inventory -u ubuntu --key-file ~/.ssh/us_prod.pem ../playbooks/cassandra.yml
