

echo "Please note if the Target Server already exists you must use '--force'"
echo
echo "Please enter APIGEE Login Details.."
echo 
read -p "Email: " EMAIL
echo 
read -sp "Password: " PASSWORD
echo
read -p "Environment (test/dev/prod-us): " ENVIRONMENT
echo 
read -p "Target Server Name (e.g. att-oauth): " TARGETSERVER
echo
read -p "Hostname (e.g. app-oauth-att-idm-prod.spark-telematics.us): " HOST



if [ "$1" == "--force" ]; then
METHOD=PUT
URL=https://api.enterprise.apigee.com/v1/o/tantaluminnovation/environments/${ENVIRONMENT}/targetservers/${TARGETSERVER}
else
METHOD=POST
URL=https://api.enterprise.apigee.com/v1/o/tantaluminnovation/environments/${ENVIRONMENT}/targetservers
fi

curl -H "Content-Type:text/xml" -X ${METHOD} -d \
"<TargetServer name=\"${TARGETSERVER}\">
   <Host>$HOST</Host>
   <Port>443</Port>
   <IsEnabled>true</IsEnabled>

    <SSLInfo> 
        <Ciphers/> 
        <ClientAuthEnabled>true</ClientAuthEnabled> 
        <Enabled>true</Enabled> 
        <IgnoreValidationErrors>false</IgnoreValidationErrors> 
        <KeyAlias>clientCertKey</KeyAlias> 
        <KeyStore>clientCertKeystore</KeyStore> 
        <Protocols/> 
    </SSLInfo> 
 </TargetServer>" \
-u ${EMAIL}:${PASSWORD} \
${URL}



echo
echo "Nb. you must redeploy the related apigee proxy after updating an existing targetserver."
echo